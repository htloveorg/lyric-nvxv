Đã bao Xuân rồi không về thăm me. <br/>
Cùng làng quê xưa biết mấy thân thương <br/>
Đời con giờ đây lạc bước giang hồ <br/>
Tháng ngày miệt mài chân mây <br/>
Đi tìm cuộc đời mai sau <br/>
Cho mẹ quê tháng đợi ngày chờ. <br/>
<br/>
Đón Xuân nơi này tuyết lạnh rơi đầy <br/>
Lòng ngậm ngùi thương chiếc bánh chưng thơm <br/>
Canh Mai nở tươi pháo đỏ hiên nhà <br/>
Thấm nồng manh áo mẹ trao <br/>
Bây giờ lạc loài nơi đây <br/>
Thấy Xuân về nghe lòng buồn thêm. <br/>
<br/>
ĐK: <br/>
<br/>
Mẹ ơi Xuân này là đã bao năm <br/>
Con đi xây dáp cuộc đời <br/>
Để Mẹ hiền hòa tin con <br/>
Đêm đêm bên ngọn đèn khuya <br/>
Mẹ ngồi tóc trắng như vôi <br/>
Thương con thương con xa mấy phương trời <br/>
Biết ngày nào trở về thăm quê <br/>
Có còn trông thấy mẹ không.? <br/>
<br/>
Gửi tâm tư này dâng về bên me. <br/>
Dù đời bể dâu con vẫn không quên <br/>
Chờ mong ngày mai giông gió qua rồi <br/>
Nắng đẹp Xuân thắm tình quê <br/>
Nghĩa tình con đáp đền sau <br/>
Mùa Xuân ấy muôn vạn niềm thương.