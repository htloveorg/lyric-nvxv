(Saigon-1957)<br/>
<br/>
Cho nhau chẳng tiếc gì nhau <br/>
Cho nhau gửi đã từ lâu <br/>
Cho nhau cho lúc sơ sinh ngày đầu <br/>
Cho những hoa niên nhịp cầu <br/>
Đưa tuổi thơ đến về đâu? <br/>
Cho nhau nào có gì đâu! <br/>
Cho nhau dù có là bao <br/>
Cho nhau cho phút yêu đương lần đầu <br/>
Cho rất luôn luôn cuộc sầu <br/>
Cho tình cho cả niềm đau. <br/>
Cho nhau làn tóc làn tơ <br/>
Cho nhau cả mắt trời cho <br/>
Cho nhau tiếng khóc hay câu vui đùa <br/>
Cho chiếc nôi cho nấm mồ <br/>
Cho rồi xin lại tự do. <br/>
<br/>
Cho nhau ngòi bút cùn trơ <br/>
Cho nhau đàn đứt đường tơ <br/>
Cho nhau cho những câu thơ tàn mùa <br/>
Cho nốt đêm mơ về già <br/>
Cho cả nhan sắc Nàng Thơ. <br/>
Cho nhau tình nghĩa đỏ đen <br/>
Cho nhau thù oán hờn ghen <br/>
Cho nhau, xin nhớ cho nhau bạc tiền <br/>
Cho cõi âm ty một miền <br/>
Cho rồi cho cả đời tiên. <br/>
Cho nhau này dãy Trường Sơn <br/>
Cho nhau cả bốn trùng dương <br/>
Quê hương xin vẫn cho nhau như thường <br/>
Cho dứt tay chia đôi đường <br/>
Cho rồi, xin chẳng còn vương !