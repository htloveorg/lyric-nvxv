(Saigon-1975)<br/>
<br/>
Chỉ chừng một năm trôi <br/>
Là quên lời trăn trối <br/>
Ai nuối thương tình đôi <br/>
Chỉ chừng một năm thôi <br/>
Chỉ cần một năm qua <br/>
Là phai mờ hương cũ <br/>
Hoa úa trong lòng ta <br/>
Chỉ cần một năm xa... <br/>
<br/>
Khi xưa em gầy gò <br/>
Đi ngang qua nhà thờ <br/>
Trông như con mèo khờ <br/>
Chờ bàn tay nâng đỡ <br/>
Ta yêu em tình cờ <br/>
Như cơn mưa đầu mùa <br/>
Rơi trên sân cỏ già <br/>
Làm rụng rơi cánh hoa. <br/>
<br/>
Chỉ cần một cơn mưa <br/>
Là vai gầy thêm nữa <br/>
Cho ướt môi, mềm da <br/>
Chỉ cần giọt mưa sa <br/>
Chỉ chờ một cơn mưa <br/>
Để không ngờ chi nữa <br/>
Đi dưới mưa hồng nghe <br/>
Giọt nhẹ vào tim ta. <br/>
<br/>
Ta yêu em mù lòa <br/>
Như A-đam ngù ngờ <br/>
Yêu E-va khù khờ <br/>
Cuộc tình trinh tiết đó <br/>
Nhưng thiên tai còn chờ <br/>
Đôi uyên ương vật vờ <br/>
Chia nhau xong tội đồ <br/>
Đầy đọa lâu mới tha... <br/>
<br/>
Chỉ một chiều lê thê <br/>
Ngồi co mình trên ghế <br/>
Nghe mất đi tuổi thơ <br/>
Chỉ một chiều bơ vơ <br/>
Chỉ là chuyện đong đưa <br/>
Đời luôn là cơn gió <br/>
Thay áo cho tình ta <br/>
Chỉ là chuyện thiên thu. <br/>
<br/>
Tưởng chừng nghìn năm sau <br/>
Chẳng ai còn yêu nhau <br/>
Nào ngỡ đâu tình yêu <br/>
Giăng bẫy nhau còn nhiều <br/>
Nghe lòng còn khô ráo <br/>
Nghe chừng còn khát khao <br/>
Nên gục đầu rất lâu <br/>
Xưng tội cả kiếp sau. <br/>
<br/>
CODA: <br/>
Cả triệu người yêu nhau <br/>
Còn ai là không thấu ? <br/>
Len giữa u tình sâu <br/>
Một vài giọt ơn nhau <br/>
Tia sáng Thiên Đường cao <br/>
Rọi vào ngục tim nhau...