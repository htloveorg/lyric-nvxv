Từng hạt mưa rớt rơi trong khung trời, không gian vắng <br/>
Hạt nào rơi rớt trong trái tim buồn <br/>
Mầu vàng lẻ loi nến thắp soi căn phòng <br/>
“Happy Birthday” trong nhớ mong. <br/>
<br/>
Ngày xưa yêu dấu bên nhau khung trời thân yêu cũ <br/>
Ngày sinh nhật thắm mộng ước bình thường <br/>
Cầu cho đôi ta mãi có nhau trong đời <br/>
“Happy Birthday” vắng anh. <br/>
<br/>
chorus: <br/>
Dù em có khóc cạn bao nước mắt <br/>
Tình xưa muôn kiếp , muôn đời xa rồi <br/>
Đời em như những hạt mưa số kiếp <br/>
Một đời rơi rớt trong lẻ loi <br/>
<br/>
<br/>
Giờ trên con phố không gian muôn trùng đang im lắng <br/>
Đèn khuya đã tắt tự bao giờ <br/>
Gục đầu bên song nến đã bơ vơ tàn <br/>
“Happy Birthday” lẻ loi