Trời thu mưa bay bay, mà lòng ai như say <br/>
Hoa lá rơi đầy, từng giọt mưa rơi rơi <br/>
Hòa nhịp tim chơi vơi, gió cuốn mây trôi <br/>
Ai đi ngoài mưa <br/>
<br/>
Ngậm ngùi nghe mưa rơi, giọng hò ai buông lơi <br/>
Chan chứa u hoài, lệ sầu ai hoen mi <br/>
Lạnh lùng nhịp chân đi, Tiếc nhớ thương chi <br/>
Lướt đi ngoài trời, gió mưa tơi bời <br/>
<br/>
Ai đi ngoài sương gió tả tơi chớ buồn vì mưa <br/>
Ai đi ngoài sương gió chớ nức nở nhạc sầu <br/>
Ai đi ngoài sương gió hỡi ai xóa nổi niềm đau <br/>
Ai ơi mộng tàn xưa không vương lụy thời gian <br/>
<br/>
Ngoài trời mưa rơi rơi, mà hồn ai chơi vơi <br/>
Sao nhớ nhung hoài, từng giọt mưa rơi rơi <br/>
Là lời ca xa xôi, thắm thiết thương ai <br/>
Lướt đi ngoài mưa <br/>
<br/>
Dù trời còn mưa rơi, đừng buồn lòng ai ơi <br/>
Ta vẫn tươi cười, rồi ngày trời thôi mưa <br/>
Đời lại đẹp như thơ, quyến luyến trong mơ <br/>
Ấm êm tâm hồn, hát câu yêu đời