(Nhạc: Đức Trí   -  Lời: Bằng Kiều )<br/>
<br/>
Chỉ một ánh mắt đắm đuối em trao<br/>
Anh ngỡ như ánh sao trên trời<br/>
Rơi vào tim này muôn ngàn thương yêu<br/>
Anh mong ánh sao trời kia ngừng mãi nơi đây<br/>
Anh sẽ không quên giây phút này<br/>
Và ánh mắt em mãi không rời xa<br/>
Một làn môi ấm đã lướt qua đây<br/>
Cho thế gian bỗng như ngưng lại<br/>
Những giây phút này mãi còn trong anh<br/>
Như sao sáng khi màn đêm dần xuống quanh đây<br/>
Em đã cho anh ấm lại dù bao giá băng<br/>
Trong tim vẫn còn<br/>
Người ơi hãy luôn gần anh<br/>
Và anh sẽ nhớ mãi những phút mặn nồng<br/>
Khi anh ôm thương yêu vào lòng<br/>
Ôm tia nắng của ngày giá rét<br/>
Cho lòng này bao ấm áp<br/>
Em như muôn ngàn giọt mưa xuân<br/>
Cho chồi non luôn xanh thắm<br/>
Hãy đến đây cùng anh để anh mãi ấp ôm<br/>
Và mãi có em trong đời.