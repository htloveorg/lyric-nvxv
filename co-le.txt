Thương ai về ngõ vắng tiếng chân nhẹ rơi đều <br/>
Tiếng ve giờ đã vắng cuối đường một tiếng tiêu <br/>
Thương ai chờ ai mãi bao năm đã trôi qua <br/>
Lũ trẻ con đã lớn người yêu giờ đã già <br/>
Có lẽ còn đâu đây chút tình vương trong đêm <br/>
Ai ngồi chờ ánh nắng mong niềm vui lớn thêm <br/>
<br/>
Thương ai dài năm tháng không thấy bóng ai qua <br/>
Thương ai ngồi đêm trắng mưa rồi mưa qua đi <br/>
Thương ai ừ thương quá <br/>
Thương ai còn ai thương