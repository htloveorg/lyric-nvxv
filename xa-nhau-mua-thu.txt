Ngày nào gặp nhau ta cùng bước đi bên nhau cuộc đời <br/>
Mùa thu hỡi người đã xa em, nay thật rồi <br/>
Đừng vội, còn lại đây em vẫn nhớ, anh đừng quên <br/>
Ngày mai, tình yêu nồng rực cháy trong tim rồi hoang vu <br/>
Mùa thu đừng xa vắng cho em buồn cô đơn <br/>
Lạnh lùng, cay đắng muộn phiền <br/>
<br/>
Một lời, tình yêu em gởi đến cho anh yêu một chiều <br/>
Mùa thu anh còn nhớ không đôi ta nồng nàn <br/>
Lối mòn, lối mòn bên anh bóng mát, em nào quên <br/>
Này anh, tình yêu còn rực cháy trong tâm hồn đôi ta <br/>
Và anh còn hơi ấm hay bao lời dối gian <br/>
Ngất ngây ngọt ngào.. người mãi xin đừng quên