Nghe Xuân sang thấy trong lòng mình chứa chan <br/>
Tiếng pháo vui vang đó đây ôi rộn ràng <br/>
Kìa mùa Xuân đang đến trước thềm <br/>
Gần xa nhịp nhàng Xuân đến <br/>
Nghe bước chân tô đẹp thêm. <br/>
<br/>
Xuân ơi Xuân vẫn muôn đời yêu mến Xuân <br/>
Nhấp chén vui ta chúc nhau ly rượu mừng <br/>
Ngày đầu năm hạnh phúc phát tài <br/>
Người người gặp nhiều duyên may <br/>
Xuân thắm tươi Xuân nồng say. <br/>
<br/>
Ai xuôi ngược trên khắp nẻo quê hương <br/>
Nhớ quay về vui đón mùa Xuân yêu thương <br/>
Lòng dạt dào hồn Xuân nao nao <br/>
Thật tuyệt vời mùa Xuân thanh cao <br/>
Ta chúc nhau những gì đẹp nhất lòng nhau. <br/>
<br/>
Đôi uyên ương sánh vai nhịp nhàng thắm duyên <br/>
Dưới nắng Xuân trông bướm hoa đang tỏ tình <br/>
Còn mùa Xuân đem vui đất trời <br/>
Còn nụ cười nở trên môi <br/>
Nhân thế ơi mong đợi Xuân