Khi vừa lớn lên, tôi đã vội yêu một người <br/>
Nên chưa hiểu đời, cuộc đời bạc trắng như vôi <br/>
Tình yêu thứ nhất đã đổi ngôi <br/>
Người yêu thứ nhất đã phụ tôi <br/>
Giấc mơ chung đời <br/>
Thường êm đẹp bên nhau cho đến ngàn sau <br/>
<br/>
Khi vừa biết yêu, tôi ngỡ tình yêu là đẹp <br/>
Nên chẳng ngại ngùng, tình đầu trao hết cho nhau <br/>
Nụ hôn thứ nhất cho người yêu <br/>
Để người đem đến bao sầu đau <br/>
Trót thương nhau rồi <br/>
Đành ôm hận thương đau cho đến muôn đời <br/>
<br/>
ĐK: <br/>
Ai ơi tình cũ lỡ làng rồi <br/>
Người về nơi ấy có vui không <br/>
Để lòng tôi mãi nhớ thương mong <br/>
Lạnh lùng kiếp cô đơn mùa đông <br/>
<br/>
Tương lai đừng nhắc đến làm gì <br/>
Thiệp hồng thôi nhé hết chung tên <br/>
Cuộc tình hai đứa đã vô duyên <br/>
Đường trần có riêng tôi tìm quên <br/>
<br/>
Không còn có nhau, thôi nhé gặp nhau càng buồn <br/>
Chôn vùi kỷ niệm, kỷ niệm đường vắng không tên <br/>
Hàng cây cao vút như lặng im <br/>
Biển tình tôi chết sau một đêm <br/>
Phút giây ban đầu <br/>
Rồi đi vào thiên thu muôn kiếp bẽ bàng