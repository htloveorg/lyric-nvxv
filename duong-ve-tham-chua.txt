Anh đi chiến dịch xa vời, lòng súng nhân đạo cứu người lầm than. <br/>
Thương dân nghèo ruộng hoang cỏ cháy. <br/>
Thấy nỗi xót xa của kiếp đoạ đầy anh đi. <br/>
<br/>
Không quên lời xưa đã ước thề, dâng cả đời trai với sa trường. <br/>
Nam nhi cổ lai chinh chiến hề, nào ai ngại gì vì gió sương. <br/>
Hôm nay ruộng đồng trong chiến dịch, kìa sáu chốn miền Đồng Nai lên niềm tin. <br/>
Nghe như lúa reo đời sống lành, nghe như đất vui nhịp quân hành. <br/>
<br/>
Anh đi chắc hẳn anh còn nhớ, đôi mắt u uẩn chiều tiễn đưa. <br/>
Của người em nhỏ thơ ngây quá, chưa biết cười lên hẹn đợi chờ. <br/>
<br/>
Có những chiều mưa phơn phớt lạnh, đem cả hồn Thu tới lòng người. <br/>
Bao nhiêu chàng trai tay xiết mạnh, thầm hẹn ngày về quê Bắc ơi. <br/>
Im nghe từ đồng hoang phố phường, còn mênh mang một niềm thương như trùng dương. <br/>
Hôm nay có anh miền chiến dịch, ôm súng mơ ngày về quang vinh .