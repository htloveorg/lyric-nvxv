Rồi mai tôi sẽ xa Đà Lạt <br/>
Thành phố này xin trả lại cho em <br/>
Ngàn thông buồn chiều nay im tiếng <br/>
Ngôi giáo đường lặng đứng suy tư <br/>
<br/>
Rồi mai tôi sẽ xa Đà Lạt <br/>
Đồi núi buồn xin gửi lại cho em <br/>
Và con đường mù sương giăng mắc <br/>
Hai đứa hôm nào lạnh buốt trong tình yêu <br/>
<br/>
Rồi mai tôi sẽ xa <br/>
Tình yêu như bóng mây <br/>
Tình yêu cơn gió bay hương thời gian phai <br/>
Rồi mai em có quên <br/>
Người đi như bóng chim <br/>
Người đi đâu dễ quên kỷ niệm đau thêm <br/>
<br/>
Rồi mai tôi sẽ xa Đà Lạt <br/>
Thành phố chiều sương khói buồn riêng em <br/>
Còn bao điều sao em  không nói <br/>
Tôi cúi đầu từ giã Đà Lạt ơi...