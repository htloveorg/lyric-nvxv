Chúng bạn thường hay bảo em rằng phước đầy nhà <br/>
Có được người yêu nổi danh là đấng hào hoa <br/>
Đời vui biết chừng nào <br/>
Tình say đắm ngọt ngào <br/>
Hỏi bạn rằng "Có người nào mà không mơ ước?" <br/>
<br/>
Thế mà lòng em vẫn chưa được mấy hài lòng <br/>
Dẫu rằng người yêu của em cũng khá thủy chung <br/>
<br/>
Đời đâu biết mà ngờ <br/>
Hào hoa số nặng thề <br/>
Bởi vì hào hoa cũng là, là đào hoa vậy thôi <br/>
<br/>
Chorus: <br/>
Hào hoa là gì? <br/>
Nếu không đào hoa là gì? <br/>
Hào hoa là gì? <br/>
Nếu không đào hoa là gì? <br/>
Là một chàng đẹp trai <br/>
Phong lưu và duyên dáng <br/>
Đôi mắt đa tình nhiều cô si mê <br/>
<br/>
Có chồng hào hoa chúng ta thường phải đề phòng <br/>
Bởi người hào hoa ít khi được tiếng thủy chung <br/>
Người mang tiếng đèo bồng <br/>
Tình bay bướm lụy phiền <br/>
Khổ nhiều người, khổ cuộc đời <br/>
Vì người yêu đào hoa