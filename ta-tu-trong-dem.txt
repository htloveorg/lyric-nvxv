Thăm thẳm chiều trôi, khuya anh đi rồi sao trời đưa lối <br/>
Khi thương mến nhau hai người hai ngả tránh sao tủi hờn <br/>
Hẹn gặp nhau đây đêm thâu lá đổ, sương giăng kín mờ nhạt nhòa đường mờ <br/>
Đã gặp nhau, sao em không nói, sao em cúi mặt, em giận hờn anh chăng <br/>
<br/>
Anh hiểu rồi đây khuya nay em về trăng gầy soi bóng <br/>
Nên em cúi mặt ngăn dòng nước mắt phút giây tạ từ <br/>
Đừng buồn nghe em tuy anh biết rằng xa xôi vẫn làm tâm tư héo mòn <br/>
Nếu em đã thường thường anh xa vắng <br/>
Xin em chớ buồn cho nặnh lòng chinh nhân <br/>
<br/>
Nếu em biết rằng có những người đi đấu tránh chưa về <br/>
Mang lời thề lên miền sơn khê từng đêm địa đầu hun hút gió sâu <br/>
Nếu em đã gặp mẹ già thường cầu khấn nguyện đêm rằm <br/>
Vợ yêu chồng đan áo lạnh từng đông <br/>
Thì duyên tình mình có nghĩa gì đâu <br/>
<br/>
Anh hỏi một câu khi trong đêm dài vọng về tiếng súng <br/>
Sao em, sao em cúi mặt không nhìn đôi mắt anh hứa thương em trọn đời <br/>
Đầu đường chia phôi anh không nói gì <br/>
Nên phong kín lời hẹn tình lứa đôi <br/>
Nếu anh có về khi tàn chinh chiến <br/>
Xin em cúi mặt dấu lệ mừng nghe em