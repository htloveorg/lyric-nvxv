[Nhạc: Trọng Đài - Lời: Trọng Đài - Chu Lai]<br/>
<br/>
<br/>
<br/>
Hà nội ơi tươi xanh màu áo học trò <br/>
Những con đường thân quen còn đó <br/>
Tiếng rao vang đâu đây nghe động trời đêm <br/>
Hồng Hà ơi buồm ai khe khẽ thuyền về <br/>
Cành me thì thầm gục đầu vào dĩ vãng <br/>
Tiếng ve kêu râm ran suốt đêm hè <br/>
Giọng dân ca sao gợi nhắc hồ Gươm <br/>
<br/>
Hà nội ơi xanh xanh liễu rủ mặt hồ Gươm <br/>
Cô đơn sấu rụng ngoài ngõ vắng <br/>
Con sóng nào vẫn vỗ về vào đam mê <br/>
Hà nội ơi ... Hà nội ơi <br/>
<br/>
Ta nhớ không quên những tháng năm qua <br/>
Một nét riêng tư gợi nhắc cho ai <br/>
là nhắc đến những kỷ niệm đã qua <br/>
Hà nội ơi nhớ về mùa thu tháng mười <br/>
Áo học trò xanh những hàng me <br/>
Hà nội ơi ta nhớ không quên <br/>
Hà nội ơi trong trái tim ta <br/>
Chiều mùa thu gió về rộng trên phố phường <br/>
Nắng vàng hồng tươi những nụ cười <br/>
Hà nội ơi ta nhớ không quên <br/>
Hà nội ơi trong trái tim ta