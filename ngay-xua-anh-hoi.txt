VERSE 1 :<br/>
Ngày xưa anh hỡi....<br/>
Hai ta cùng chung một lối,<br/>
Ngày xưa anh hỡi....<br/>
đôi ta thề sánh duyên đời.<br/>
<br/>
Hòa nhịp nhau trên lối tình,<br/>
Ta xây ước mộng đẹp cho mình,<br/>
Và anh thường nói<br/>
"Sẽ mãi yêu em suốt đời"<br/>
Rồi nhiều đêm ân ái đê mê nồng say<br/>
mà em trao anh, thầm mong anh sẽ vui, sẽ yêu em trọn đời<br/>
Rồi anh ra đi không hề nói một lời...<br/>
<br/>
Anh có biết chăng? Người hỡi có thấu chăng?<br/>
Em sẽ mang anh trong tim em suốt cả một cõi đời!<br/>
Vì cớ sao muốn xa nhau?<br/>
Dù biết em lỡ yêu rồi!<br/>
Sao nỡ quên anh ơi, anh quên đi câu thề xưa.<br/>
<br/>
Dù biết em đang đớn đau,<br/>
đành bỏ đi không ngoái đầu,<br/>
Còn chút yêu thương ngày nào<br/>
xin người ơi, hãy trở về mau!<br/>
<br/>
VERSE 2 :<br/>
Vì anh em đã lắm lúc buồn đau ray rứt...<br/>
Vì anh em đã thôi không còn chữ "chung tình" !<br/>
Giờ con tim đã giá lạnh<br/>
thôi... không dám còn mộng hay mơ<br/>
Mặc màn đêm xuống, <br/>
Em lang thang đi trong thẫn thờ<br/>
Dù cho mưa rớt lên đôi bờ mi hòa dòng lệ rơi<br/>
Hòa tan trong tấm thân em nỗi cô đơn hiu quạnh<br/>
Giọt mưa sao không lạnh át tim mình.<br/>
<br/>
Chorus:<br/>
Anh có biết chăng? Người hỡi có thấu chăng?<br/>
Em sẽ mang anh trong tim em suốt cả một cõi đời!<br/>
Vì cớ sao muốn xa nhau?<br/>
Dù biết em lỡ yêu rồi!<br/>
Sao nỡ quên anh ơi, anh quên đi câu thề xưa.<br/>
<br/>
Coda:<br/>
Ngày xưa anh hỡi....<br/>
Hai ta cùng chung một lối,<br/>
Ngày nay anh hỡi....<br/>
Duyên ta đành phải xa rời.