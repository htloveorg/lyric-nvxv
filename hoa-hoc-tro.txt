<i>(nhạc: Anh Bằng - thơ: Nhất Tuấn)</i><br/>
<br/>
<br/>
Bây giờ còn nhớ hay không? <br/>
Ngày xưa hè đến phượng hồng nở hoa <br/>
Ngây thơ anh rủ em ra <br/>
Bảo nhặt hoa phượng về nhà chơi chung <br/>
<br/>
Bây giờ còn nhớ hay không? <br/>
Bây giờ còn nhớ hay không? <br/>
<br/>
Bây giờ còn nhớ hay không? <br/>
Anh đem cánh phượng tô hồng má em <br/>
Để cho em đẹp như tiên <br/>
Nhưng em không chịu <br/>
Sợ phải lên trên trời <br/>
<br/>
Sợ phải lên, sợ phải lên trên trời <br/>
Sợ phải lên, sợ phải lên trên trời <br/>
<br/>
Lên trời hai đứa hai nơi <br/>
Thôi em chỉ muốn làm người trần gian <br/>
Hôm nay phượng nở huy hoàng <br/>
Nhưng từ hai đứa lỡ làng duyên nhau <br/>
<br/>
Rưng rưng phượng đỏ trên đầu <br/>
Tìm anh em biết tìm đâu bây giờ <br/>
Bây giờ tìm kiếm em đâu? <br/>
Bây giờ thì mãi xa nhau...