1. <br/>
Bao nhiêu năm rồi còn mãi ra đi <br/>
Đi đâu loanh quanh cho đời mỏi mệt <br/>
Trên hai vai ta đôi vầng nhật nguyệt <br/>
Rọi suốt trăm năm một cõi đi về <br/>
<br/>
Lời nào của cây lời nào cỏ lạ <br/>
Một chiều ngồi say, một đời thật nhẹ ngày qua <br/>
Vừa tàn mùa xuân rồi tàn mùa hạ <br/>
Một ngày đầu thu nghe chân ngựa về chốn xa <br/>
<br/>
Mây che trên đầu và nắng trên vai <br/>
Đôi chân ta đi sông còn ở lại <br/>
Con tinh yêu thương vô tình chợt gọi <br/>
Lại thấy trong ta hiện bóng con người <br/>
<br/>
2. <br/>
Nghe mưa nơi nầy lại nhớ mưa xa <br/>
Mưa bay trong ta bay từng hạt nhỏ <br/>
Trăm năm vô biên chưa từng hội ngộ <br/>
Chẳng biết nơi nao là chốn quê nhà <br/>
<br/>
Đường chạy vòng quanh một vòng tiều tụy <br/>
Một bờ cỏ non một bờ mộng mị ngày xưa <br/>
Từng lời tà dương là lời một địa <br/>
Từng lời bể sông nghe ra từ độ suối khe <br/>
<br/>
Trong khi ta về lại nhớ ta đi <br/>
Đi lên non cao đi về biển rộng <br/>
Đôi tay nhân gian chưa từng độ lượng <br/>
Ngọn gió hoang vu thổi suốt xuân thì...