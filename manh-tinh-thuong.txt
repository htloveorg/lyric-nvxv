Vừa mới yêu nhau, mà đã xa rồi. <br/>
Bao nhiêu mộng đẹp của em (anh) và tôi <br/>
Trôi theo giòng đời quên đi tình người, <br/>
Vẫn còn lưu-luyến chưa nguôi nỗi-niềm thương yêu ngày ấy. <br/>
<br/>
Ngồi dưới trăng thơ cùng ước mơ rằng: <br/>
"Yêu nhau trọn đời, thề không lạt phai". <br/>
Nhưng duyên tình mình đã lỡ-làng rồi, <br/>
Đâu còn câu hát say sưa giữa mùa trăng ấy, em (anh) ơi. <br/>
<br/>
Anh (em) đâu có ngờ giờ đây hai đứa chia tay, <br/>
Mộng tàn như áng mây bay. <br/>
Thì những ngày qua ta đừng trao thiết-tha, <br/>
Để lòng không vấn-vương. <br/>
<br/>
Tình đã chia-ly còn ước mong gì ? <br/>
Thư em (anh) hẹn-hò gửi anh (em) còn đây. <br/>
Đêm nay ngồi nhìn sương rơi lạnh-lùng, <br/>
Trăng về soi bóng riêng anh (em) âm-thầm ôm ấp đơn côi