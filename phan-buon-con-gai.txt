Xin trả cho anh lời nói yêu đầu <br/>
Bằng lời thương yêu anh viết cho em. <br/>
Duyên kiếp ngăn chia hai đứa mình rồi, <br/>
Còn giữ chi cho thêm buồn. <br/>
Lòng gợi thêm bao nhớ thương <br/>
Em chúc cho anh đẹp mối duyên tình. <br/>
Cùng người anh yêu quyền quý cao sang. <br/>
Em biết thân em phận gái nghèo hèn, <br/>
Mà lỡ yêu thương ai rồi, <br/>
Tình đành như áng mây trôi <br/>
<br/>
<br/>
Anh hay chăng anh <br/>
Khi buồn em cố mong quên nhưng càng thương nhớ thêm. <br/>
Hay chăng anh ơi, <br/>
Duyên tình em chỉ một người nên đời em lẻ loi. <br/>
Em chỉ xin anh đừng mỉa mai gì. <br/>
Dù một lời thôi khi nhắc tên em. <br/>
Cho phút chia tay không tủi phận người, <br/>
còn ở trên môi nụ cười, dù rằng trong giấc mơ thôi.