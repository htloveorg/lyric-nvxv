Xao xuyến bến sông xuôi con nước ngược dòng <br/>
Mấy mùa trổ bông thương cây khế đợi trông <br/>
Có người ra đi bỏ quên lời hẹn ước <br/>
Để ai một mình, thui thủi ánh trăng non. <br/>
<br/>
Khô héo mắt ai theo năm tháng miệt mài <br/>
Dáng người mảnh mai nghe rưng rức bờ vai <br/>
Nhớ người ra đi để thêm buồn cây đắng <br/>
Ai hát thì thầm, câu vọng cổ tương tự <br/>
<br/>
Ơi chàng ơi, nỡ phụ tình em <br/>
Chàng nở sao đành <br/>
Sáo bay lạc đàn thở than sớm tối <br/>
Ngựa ô kệu đàng, lạc lối về dinh <br/>
Câu hát não nề <br/>
Gió thổi năm canh, nghe đàn ai <br/>
Đàn ai xuông xệ tình dở dang <br/>
Dở dang hẹn thề. <br/>
Ơi chàng ơi, nỡ phụ tình em <br/>
Chàng nỡ sao đành <br/>
Nắng mưa tội tình nỉ non mái lá <br/>
Quạ kêu lục bình đợi cá về sông <br/>
Than vắng thở dài <br/>
Xóm nhỏ mưa giông <br/>
Em ngồi đây chờ mong bóng hình <br/>
Đời hẩm hiu, hẩm hiu lý một mình.