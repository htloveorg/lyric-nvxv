Em khóc tơ duyên bẽ bàng <br/>
nằm ôm sầu nhớ mênh mang <br/>
Anh xót xa cho thân người <br/>
lữ khách nơi phương trời <br/>
buồn hơn lá thu rơi. <br/>
<br/>
Em ngóng quê hương xa mờ <br/>
gọi ân tình quá bơ vơ <br/>
Anh ngóng chim bay thẫn thờ <br/>
thương nhớ hơn bao giờ <br/>
Tình thiên thu đứng chờ. <br/>
<br/>
Bài Tango hôm nào <br/>
bước đi Tango người yêu <br/>
Thành kỷ niệm ngàn sau, <br/>
Thành vết thương tình yêu, <br/>
thành những cơn cuồng bão. <br/>
Bài Tango xa rồi bước đi Tango lẻ loi <br/>
Ðường năm xưa hoang tàn <br/>
lòng cô đơn muôn phần <br/>
Thành phố vắng đi đôi tình nhân. <br/>
<br/>
Như lá xanh bỗng xa cành <br/>
đời em vàng võ mong manh <br/>
Như cánh chim kia xa bầy. <br/>
Em sống cho qua ngày <br/>
và cho qua kiếp này