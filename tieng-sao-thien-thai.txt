(nhạc: Phạm Duy - thơ: Thế Lữ)<br/>
<br/>
Xuân tươi<br/>
Êm êm ánh xuân nồng<br/>
Nâng niu sáo bên rừng<br/>
Dăm ba chú Kim Ðồng<br/>
Hò xang xê tiếng sáo<br/>
Nhẹ nhàng lướt cỏ nắng<br/>
Nhạc lòng đưa hiu hắt<br/>
Và buồn xa, buồn vắng<br/>
Mênh mông là buồn...<br/>
Tiên Nga<br/>
Buông lơi tóc bên nguồn <br/>
Hiu hiu lũ cây tùng<br/>
Ru ru tiếng trên cồn<br/>
Hò ơi, làn mây ơi<br/>
Ngập ngừng sau đèo vắng<br/>
Nhìn mình cây nhuộm nắng <br/>
Và chiều như chìm lắng <br/>
Bóng chiều không đi...<br/>
Trời cao xanh ngắt, xanh ngắt<br/>
Ô ô ô kià<br/>
Hai con hạc trắng<br/>
Bay về nơi nao ?<br/>
Trời cao xanh ngắt, ô ô ô kià <br/>
Ô ô ô kià <br/>
Hai con hạc trắng<br/>
Bay về về nơi nao ?<br/>
<br/>
Ðôi chim ơi <br/>
Lên khơi sáo theo vời<br/>
Hay theo đến bên người<br/>
Tiên Nga tắm sau đồi<br/>
Tình tang ôi tiếng sáo<br/>
Khi cao cao mờ vút<br/>
Cùng làn mây lửng lơ<br/>
Rồi về bên bờ suối<br/>
Cây xanh mờ mờ...<br/>
Êm êm, ôi tiếng sáo tơ tình<br/>
Xinh như bóng xiêm đình<br/>
Trên không uốn thân mình<br/>
Ðường lên lên Thiên Thai<br/>
Lọt vài cung nhạc gió<br/>
Thoảng về mơ mòng quá<br/>
Nàng Ngọc Chân tưỏng nhớ<br/>
Tiếng lòng bay xa...