Cuộc đời là một giấc mơ <br/>
mà lòng người thường nhiều mơ ước <br/>
Vì đời đẹp muôn đóa hoa <br/>
Êm đềm theo tiếng em ca ḥoa với cung đàn <br/>
Dù mau phai, dù chóng tàn <br/>
Tôi vẫn yêu thương suốt đời <br/>
<br/>
Tôi yêu em, tôi yêu em như hình yêu bóng <br/>
Em nhìn tôi, em nh́ìn tôi đắm đuối say mê <br/>
Tôi yêu em, tôi yêu em quên bóng tối sương mù <br/>
Chỉ vì yêu em, chỉ vì yêu em... quên cả lối về!