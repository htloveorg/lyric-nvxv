Vườn rau, vườn rau xanh ngắt một mầu <br/>
Có đàn, có đàn gà con nương náu <br/>
Mẹ quê, mẹ quê vất vất vả trăm chiều <br/>
Nuôi đàn, nuôi một đàn con chắt chiu <br/>
Bà bà mẹ quê ! <br/>
Gà gáy trên đầu ngọn tre <br/>
Bà bà mẹ quê ! <br/>
Chợ sớm đi chưa thấy về <br/>
Chờ nụ cười con, và đồng quà ngon. <br/>
<br/>
Trời mưa, trời mưa ướt áo mẹ già <br/>
Mưa nhiều, mưa nhiều càng tươi bông lúa <br/>
Trời soi, trời soi bốc khói sân nhà <br/>
Nắng nhiều nắng nhiều thì phơi lúa ra <br/>
Bà bà mẹ quê ! Đêm sớm không nề hà chi <br/>
Bà bà mẹ quê ! Ngày tháng không ao ước gì <br/>
Nhỏ giọt mồ hôi, vì đời trẻ vui. <br/>
<br/>
Miệng khô, miệng khô nhớ bát nước đầy <br/>
Nhớ bà, nhớ bà mẹ quê xưa ấy <br/>
Mùa đông, mùa đông manh chiếu thân gầy <br/>
Cháu bà, cháu bà ngủ ngon giấc say. <br/>
Bà bà mẹ quê ! <br/>
Chân bước ra đời cõi xa <br/>
Bà bà mẹ quê ! <br/>
Từ lúc quê hương xóa nhòa <br/>
Nhớ về miền quê, mà giọt lệ sa