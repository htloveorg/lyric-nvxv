Từ xa tôi về phép hai mươi bốn giờ <br/>
Tìm người thương trong người thương <br/>
Chân nghe quen từng viên sỏi đường nhà <br/>
Chiều nghiêng nghiêng nắng đổ <br/>
Và người yêu đứng chờ ngoài đầu ngõ bao giờ <br/>
<br/>
Cửa tâm tư là mắt nên khi đối mặt <br/>
Chuyện buồn dương gian lẩn khuất <br/>
Ðưa ta đi về nguyên thủy loài người <br/>
Mùa yêu khi muốn ngỏ vụng về <br/>
Ngôn ngữ tình làm bằng dấu đôi tay <br/>
<br/>
Bốn giờ đi dài thêm bốn giờ về <br/>
Thời gian còn lại anh cho em tất cả em ơi <br/>
Ta đưa ta đến đỉnh tuyệt vời <br/>
Ðêm lạc loài giấc ngủ mồ côi <br/>
<br/>
Người đi chưa đợi sáng <br/>
Ðưa nhau cuối đường sợ làm đêm vui rủ xuống <br/>
Thương quê hương và bé nhỏ tình này <br/>
Ngừng trong đôi mắt đỏ <br/>
Vì mình mười sáu giờ bỏ trời đất bơ vơ