Giọt lệ vu qui em khóc duyên bẽ bàng <br/>
Ngày mai em đã sang ngang <br/>
Một người trong theo hồn rét buốt môi hôn <br/>
Em bước theo chồng còn nhớ anh không? <br/>
Trả lại cho nhau những cánh thư ân tình <br/>
Vì cuộc đời tình lỡ chia xa <br/>
Một người quay lưng một người mắt rưng rưng <br/>
Ta mất nhau rồi xin chớ buồn nghe anh. <br/>
<br/>
ĐK: <br/>
Tình lỡ đã một lần khóc hận <br/>
Cho một người đi mang theo những cơn mê chợt khóc <br/>
Đời hẩm hiu sớm chiều <br/>
Và nếu nếu mình còn chút vấn vương <br/>
Cho dù tình vỡ chua cạy <br/>
Một mai em đi mang nỗi đau sang cầu <br/>
Đừng buồn cho duyên số long đong <br/>
Một người sang ngang một người khóc lỡ làng <br/>
Nước mắt phũ phàng xin “giã biệt anh ơi”...