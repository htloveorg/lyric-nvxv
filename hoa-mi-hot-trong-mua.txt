Tiếng mưa rơi ngoài hiên, gió mưa như lạnh thêm <br/>
Có con chim họa mi hót trong mưa buồn lắm <br/>
Nỗi nhớ anh ngày mưa, nỗi nhớ anh thật vô ngần <br/>
<br/>
Muốn xa anh thật xa, muốn quên đi những ngày qua <br/>
Muốn sao cho đừng yêu muốn sao cho đừng nhớ <br/>
Cố quên đi mà em vẫn thấy hơi thở anh kề bên em <br/>
<br/>
Mưa, trong mưa họa mi vẫn hót thật dịu dàng dịu dàng <br/>
Trên môi em tình yêu đã mất còn nồng nàn nồng nàn <br/>
Vì sao lại chia tay, vì sao chẳng trở về <br/>
Vì sao ngừng mê say, vì sao chẳng mãi mãi <br/>
<br/>
Hỡi chim họa mi dịu dàng <br/>
Hót trong ngày mưa thật buồn <br/>
Hay trong tình yêu của em.