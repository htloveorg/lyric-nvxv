Gió nhớ gì ngẩn ngơ ngoài hiên <br/>
Mưa nhớ gì thì thầm ngoài hiên <br/>
Bao đêm tôi đã một mình nhớ em <br/>
Đêm nay tôi lại một mình <br/>
<br/>
Nhớ em vội vàng trong nắng trưa <br/>
Áo phơi trời đổ cơn mưa <br/>
Bâng khuâng khi con đang còn nhỏ <br/>
Tan ca bố có đón đưa <br/>
<br/>
Nhớ em giọt mồ hôi tóc mai <br/>
Gió sương mòn cả hai vai <br/>
Đôi chân chênh vênh con đường nhỏ <br/>
Nghiêng nghiêng bóng em gầy <br/>
<br/>
Vắng em còn lại tôi với tôi <br/>
Lá khô mùa này lại rơi <br/>
Thương em mênh mông chân trời lạ <br/>
Bơ vơ chốn xa xôi <br/>
<br/>
Vắng em đời còn ai với ai <br/>
Ngất ngây men rượu say <br/>
Đêm đêm liêu xiêu con đường nhỏ <br/>
Cô đơn, cùng với tôi về.....