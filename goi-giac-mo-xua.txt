Ngày mai lênh đênh trên sông Hương, <br/>
Theo gió mơ hồ hồn về đâu? <br/>
Sóng sầu dâng theo bao năm tháng, <br/>
ngóng về đường lối cũ tìm em! <br/>
<br/>
Thương em thì thương rất nhiều mà duyên kiếp lỡ làng rồi <br/>
Xa em ! lòng anh muốn nói bao lời gió buông lả lơi. <br/>
Hình bóng đã quá xa mờ dần theo thời gian. <br/>
Kiếp sau xin chắp lời thề cùng sống bước lang thang. <br/>
<br/>
Em ơi ! tình duyên lỡ làng rồi còn đâu nữa mà chờ, <br/>
Anh đi lòng vương vấn lời thề nhớ kiếp sau chờ nhau. <br/>
Tha hương lòng thương nhớ ngày nào cùng tắm nắng vườn đao. <br/>
Gió xuân sang anh buồn vì vắng bóng người yêu. <br/>
<br/>
Rồi mai khi anh xa kinh đô. <br/>
Em khóc cho tàn một mùa thơ. <br/>
Nhớ người em nương theo cơn gió. <br/>
Ru hồn về dĩ vãng mộng mơ. <br/>
<br/>
Thương anh thì thương rất nhiều mà ván đã đóng thuyền rồi. <br/>
Đa đoan trời xanh cắt cánh lìa cành khiến chim lìa đôi <br/>
Chiều xuống mưa gió tiêu điều reo trên dòng Hương. <br/>
Tháng năm chưa xóa niềm sầu vì đứt khúc tơ vương. <br/>
<br/>
Anh ơi, đời đã lỡ hẹn thề thì đâu có ngày về. <br/>
Xa anh đời em tắt nụ cười héo hắt đôi làn môi. <br/>
Đêm đêm đèn le lói một mình ngồi ôm giấc mộng tình. <br/>
Kiếp sau đôi tim hòa chào đón ánh bình minh.