Đêm nay thu sang cùng heo may <br/>
Đêm nay sương lam mờ chân mây <br/>
Thuyền ai lờ lững trôi xuôi dòng <br/>
Như nhớ thương ai chùng tơ lòng <br/>
<br/>
Trong cây hơi thu cùng heo may <br/>
Vi vu qua muôn cành mơ say <br/>
Miền xa lời gió vang thông ngàn <br/>
Ai oán thương ai tàn mơ màng <br/>
<br/>
Lướt theo chiều gió <br/>
Một con thuyền, theo trăng trong <br/>
Trôi trên sông thương, <br/>
nước chảy đôi dòng <br/>
Biết đâu bờ bến <br/>
<br/>
Thuyền ơi thuyền trôi nơi đâu <br/>
Trên con sông thương, <br/>
nào ai biết nông sâu? <br/>
Nhớ khi chiều sương, <br/>
cùng ai trắc ẩn tấm lòng. <br/>
Biết bao buồn thương, <br/>
thuyền mơ buồn trôi xuôi dòng <br/>
<br/>
Bến mơ dù thiết tha, <br/>
thuyền ơi đừng chờ mong <br/>
ánh trăng mờ chiếu, <br/>
một con thuyền trong đêm thâu <br/>
Trên sông bao la, <br/>
thuyền mơ bến nơi đâu.