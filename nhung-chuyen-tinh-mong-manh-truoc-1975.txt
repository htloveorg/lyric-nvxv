Ân tình cho anh từ muôn kiếp nào <br/>
Ân tình cho anh từng cơn sóng gào. <br/>
Cho dù xa nhau tình không xóa mờ <br/>
Những đêm buồn, ngồi mênh mang tiếc nhớ. <br/>
Ân tình cho anh tuổi xuân trắng ngần. <br/>
Ngày nào đôi lứa, sống trong mơ mộng, <br/>
Ân tình cho anh sầu dâng ngút ngàn <br/>
Chan chứa nỗi vui đớn đau rã rời. <br/>
<br/>
Anh ở nơi nào đắm say với ai <br/>
Ngày nào hạnh phúc nay đã phôi pha <br/>
Những đêm giá lạnh những đêm nhớ anh, <br/>
Hồn em chỉ thấy mưa gió ngậm ngùi. <br/>
Dù thời gian dấu vết xưa phai tàn <br/>
Đã trót yêu anh rồi, yêu mãi mãi. <br/>
Anh ở nơi nào có nghe lá rơi, <br/>
Vòng tay ân ái tiếng hát lả lơi <br/>
Những đêm giá lạnh những đêm nhớ anh, <br/>
Hồn em chỉ thấy mưa gió ngậm ngùi. <br/>
<br/>
Ân tình cho anh từ muôn kiếp nào <br/>
Ân tình cho anh từng cơn sóng gào. <br/>
Ngày nào đôi lứa sống trong mơ mộng, <br/>
chan chứa nỗi vui đớn đau rã rời. <br/>
Anh ở nơi nào hạnh phúc nay đã phôi pha <br/>
Những đêm giá lạnh những đêm nhớ anh, <br/>
Hồn em chỉ thấy mưa gió ngậm ngùi. <br/>
Dù thời gian dấu vết xưa phai tàn <br/>
Đã trót yêu anh rồi, yêu mãi mãi. <br/>
Anh ở nơi nào có nghe lá rơi <br/>
Vòng tay ân ái tiếng hát lả lơi <br/>
Những đêm giá lạnh những đêm nhớ anh, <br/>
Hồn em chỉ thấy... mưa gió ngậm ngùi...