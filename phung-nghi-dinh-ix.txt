<em><b>Lời: Việt Tiên</em></b><br/>
<br/>
<br/>
Toàn dân! Nghe chăng? Sơn hà nguy biến! <br/>
Hận thù đằng đằng! Biên thùy rung chuyển <br/>
Tuôn giày non sông rền vang tiếng vó câu <br/>
Gây oán nghìn thu <br/>
Toàn dân Tiên Long! Sơn hà nguy biến! <br/>
Người nào hào hùng! Nên hòa hay chiến? <br/>
Diên Hồng tâu lên cùng minh đế báo ân <br/>
Hỡi đâu tứ dân! <br/>
<br/>
Kìa vừng hồng tràn lan trên đỉnh núi <br/>
Ôi Thăng Long! Khói kinh kỳ phơi phới <br/>
Loa vang vang, chiếu ban truyền bốn hương <br/>
Theo gió bay khắp miền sông núi réo đời. <br/>
<br/>
Lòng dân Lạc Hồng nhìn non nước yêu quê hương <br/>
Giống anh hùng nâng cao chí lớn <br/>
Giống anh hùng đua sức tráng cường. <br/>
Ta lên đường lòng mong tâu đến long nhan <br/>
Dòng Lạc Hồng xin thề liều thân liều thân! <br/>
<br/>
Đường còn dài <br/>
Hồn vương trên quan tái <br/>
Xa xa trông áng mây đầu non đoài <br/>
<br/>
Trông quân Nguyên tàn phá non sông nhà <br/>
Đoạt thành trì toan xéo giày lăng miếu <br/>
Nhìn bao quân Thoát lấn xâm tràn nước ta <br/>
Ôi sông núi nhà rền tiếng muôn dân kêu la <br/>
<br/>
<em>Hỏi</em>: Trước nhục nước nên hòa hay nên chiến? <br/>
<em>Đáp</em>:  Quyết chiến! <br/>
<br/>
<em>Hỏi</em>: Trước nhục nước nên hòa hay nên chiến? <br/>
<em>Đáp</em>:  Quyết chiến! <br/>
<br/>
Quyết chiến luôn <br/>
Cứu nước nhà <br/>
Nối chí dân hùng anh <br/>
<br/>
<em>Hỏi</em>: Thế nước yếu lấy gì lo chiến chinh? <br/>
<em>Đáp</em>: Hy sinh! <br/>
<br/>
<em>Hỏi</em>:  Thế nước yếu lấy gì lo chiến chinh? <br/>
<em>Đáp</em>:  Hy sinh! <br/>
<br/>
Thề liều thân cho sông núi <br/>
<br/>
Muôn năm lừng uy!!