Xin trả lại những kỷ niệm buồn vui <br/>
Ngày xanh đã theo thời gian qua mất rồi <br/>
Ngồi viết tâm sự nhớ ngược về quá khứ <br/>
Chợt lên nét suy tư <br/>
Bao năm thầm kín trót vương tà áo tím <br/>
Những đêm sương lạnh nghe trái sầu rớt vào tim... <br/>
 <br/>
Thương rất nhiều mái tóc xõa bờ vai <br/>
Tình khôn lớn như chờ mong ôi quá dài <br/>
Lòng vẫn u hoài nghĩ chuyện tình đổi thay <br/>
Đời ai biết được ai <br/>
Chia ly là thế, xót xa nhiều cũng thế <br/>
Nếu mai sau gặp xin cúi mặt làm ngơ ! <br/>
<br/>
Thời gian có ngừng đây bao giờ ! <br/>
Thương tiếc rồi sẽ làm buồn vu vơ ! <br/>
<br/>
Nhiều lúc muốn quên để xóa mờ <br/>
Nhưng mỗi lần nhìn xuân về thương nhớ <br/>
Người đó ta đây, tình vẫn chia phôi <br/>
Biết cuộc đời mình ra sao <br/>
<br/>
Ôm kỷ niệm chẳng nửa lời thở than <br/>
Một tâm khúc cho người thương, cho tiếng đàn <br/>
<br/>
Đời đã không màng những gì mơ ước, <br/>
mà sao khó tìm quên ?! <br/>
<br/>
Xa nhau thì nhớ lúc đêm gần xao xuyến <br/>
Nhớ thương bây giờ xin trả lại thời gian <br/>
<br/>
Nhớ thương bây giờ xin trả lại thời gian <br/>
Nhớ thương bây giờ xin trả lại thời gian.....