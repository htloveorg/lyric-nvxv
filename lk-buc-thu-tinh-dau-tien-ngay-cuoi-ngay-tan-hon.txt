Khi anh đưa mắt nhìn em qua tấm gương <br/>
Ta đã gặp nhau, bối rối thật lâu <br/>
Đêm nay dường như những ánh mắt muốn đi kiếm tìm nhau. <br/>
Anh muốn nói với em những điều thật lớn lao <br/>
Sẽ luôn ở đây, nơi tim anh, tình yêu bất tận <br/>
Phút giây anh nghẹn lời, vì biết em yêu anh <br/>
Và anh sẽ là người đàn ông của đời em <br/>
Anh đã mơ về ngôi nhà và những đứa trẻ <br/>
Vì yêu em, ngày mai anh thêm vững bước trên con đường dài <br/>
Em có nghe mùa đông, những ngọn đèn vàng <br/>
Anh nhớ em <br/>
Anh nhớ em miên man bên trang thư tình gửi đến em <br/>
Bạn đời ơi, <br/>
Anh mơ mỗi sớm tỉnh giấc, <br/>
Mắt anh kiếm tìm, <br/>
Tai anh lắng nghe, <br/>
Môi anh cất tiếng gọi, <br/>
Và vòng tay anh rộng mở đón em vào lòng.