Nghe trong biển sóng có mùa thu trở về hôm qua<br/>
Lang thang một bóng mỗi mình em cùng giọt mưa sa<br/>
Anh bên trời vắng làm sao biết đời như lá gầy<br/>
Anh vui ngày nắng làm sao hay tình như khói mây<br/>
<br/>
Nghe trong biển sóng có lời ca tự tình bao la<br/>
Cho mưa và nắng cuốn mình đi để tình trôi xa<br/>
Tay em còn ấm bờ môi thắm tình chưa xóa nhòa<br/>
Người yêu hỡi em cố quên thôi<br/>
Mà hỏi lòng sao mãi chơi vơi<br/>
<br/>
Bao nhiêu mộng ước có tan theo cùng ngày tháng<br/>
Bao nhiêu tình ái có chết trong cơn mê dài<br/>
Em vẫn mong chờ vẫn nhớ thiết tha đầy vơi<br/>
Anh có bao giờ tiếc nuối xót xa một thời<br/>
<br/>
Nghe trong biển sóng có niềm vui nụ cười hôm qua<br/>
Sao mưa lạnh giá buốt bờ vai tạ từ phôi pha<br/>
Xin cho lần cuối nụ hôn dỗi vòng tay rã rời<br/>
Người yêu hỡi Anh đã xa xôi<br/>
Để một đời Em nhớ khôn nguôi.