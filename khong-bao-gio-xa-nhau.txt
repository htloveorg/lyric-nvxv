Mưa vẫn rơi trên đường chiều, thầm nhớ những ngày mình bên nhau. Em, cô bé thơ ngây học trò, hờn dỗi khi anh nói lời vu vơ.<br/>
Thương phút bên nhau lần đầu, cùng hát trong chiều buồn mưa ngâu. Thương đôi mắt thơ ngây ngày nào, ngày em đã trao anh tình yêu ban đầu.<br/>
Có những phút bên nhau em hỡi, ngỡ không bao giờ xa nhau, rồi giọt nước mắt rơi giữa đêm tối, em đã đi như cánh chim chiều mang nỗi đau.<br/>
Ngừơi yêu hỡi sao cách xa mãi, nhớ em trời làm mưa ngâu. Và con tim ấy nay đã thay đổi. Em đã quên, đã quên tình yêu ban đầu.