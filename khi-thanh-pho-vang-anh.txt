Yêu cho biết sao đêm dài <br/>
Cho quen với nồng cay <br/>
Yêu cho thấy bao lâu đài <br/>
Chỉ còn vài trang giấy <br/>
<br/>
Giòng mực xanh còn đấy <br/>
Hứa cho nhiều dù bao lời nói <br/>
Đã phai tàn thành mây thành khói <br/>
Cũng xem như không mà thôi <br/>
<br/>
Những ân tình em đong bằng nước mắt <br/>
Khóc cho đầy hai chữ tình yêu <br/>
Phấn hương nồng anh xem tựa tấm áo <br/>
Đã phai màu ân ái từ lâu... <br/>
<br/>
Những neo thuyền yêu đương thường dễ đứt <br/>
Khiến bao chiều trên bến tịch liêu <br/>
Vắng con tàu sân ga thường héo hắt <br/>
Thiếu anh lòng em thấy quạnh hiu... <br/>
<br/>
Xưa đêm vắng đưa nhau về <br/>
Nay đơn bóng đường khuya <br/>
Khi vui thấy trăng không mờ <br/>
Lòng buồn nên trăng úa <br/>
<br/>
Kìa phồn hoa còn đó <br/>
Những con đường buồn vui lộng gió <br/>
Những ân tình chìm trong lòng phố <br/>
Cũng theo hư không mà đi