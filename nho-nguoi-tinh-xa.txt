<i>(nhạc: Võ Tá Hân - thơ: Phạm Thiên Thư)</i><br/>
<br/>
<br/>
Đêm nghe mưa nhỏ <br/>
Động mái lều thơ <br/>
Dưng nhớ, <br/>
Nhớ người ngày xưa <br/>
<br/>
Áo vàng thuở nọ <br/>
Người tình nho nhỏ <br/>
Nhỏ mãi trong ta <br/>
Như chùm hạ hoa <br/>
<br/>
Buồn ơi đốt thuốc <br/>
Lần trang sách nhòa <br/>
Này những đóa hoa, <br/>
ép từ hạ cũ <br/>
<br/>
Làn hương tóc biếc <br/>
Còn riêng giấc mơ <br/>
Vùi trong sương mù <br/>
Vương vấn xót xa lòng ai <br/>
<br/>
Tưởng em tóc xõa <br/>
Trong dòng mưa sa <br/>
Vội hái <br/>
Mấy chùm hạ hoa <br/>
<br/>
Kết thành vương miện <br/>
Mừng người em nhỏ <br/>
Người tình xa xưa <br/>
Một lần thoáng qua <br/>
Một lần thoáng qua