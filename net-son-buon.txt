Vẫn biết người không đến mà sao em vẫn mong chờ hoài. Lá úa còn rơi mãi, mình em qua phố đông ngậm ngùi. Giờ thì lòng chỉ còn lại những tiếc nuối, đành lòng nhìn người tình mờ khuất bóng khói, còn đây mình em, nét son nhạt phai.<br/>
<br/>
Vẫn biết tình như thế, mà em không thể quên được người. Xuống phố, lòng nhung nhớ, đường thênh thang có em lạc loài. Ngày nào mình hẹn hò từng phút đắm đuối. Đành lòng chờ người tình, chờ đến phút cuối. Dù cho thời gian, nét son nhạt phai.<br/>
<br/>
Em cô đơn trong những đêm dài người có hay, mưa rơi bên hiên vắng nghe lòng nhiều đắng cay. Bao yêu thương năm tháng đã tan thành khói mây, dù em không đổi thay.<br/>
<br/>
Anh ra đi nơi ấy nghe đời còn có vui, ai ru em yên giấc bên dòng đời đổi thay. Bao đêm em thao thức nghe đời buồn lá rơi. Anh đã xa em thật rồi.