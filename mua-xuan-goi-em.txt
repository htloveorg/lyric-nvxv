Anh ở đâu thì em đó <br/>
Lúc núi biếc khi sông hồ <br/>
biên thùy kia nào khác chi nhà <br/>
vì tình yêu thì có đâu xa <br/>
kìa mùa xuân chim hót <br/>
mùa hè ve ca hát <br/>
và mình chia ánh trăng thu <br/>
<br/>
Chỉ còn mùa đông lạnh lùng <br/>
thì em đan áo cho chàng <br/>
ba ngày một lá thư tình <br/>
bảy ngày em gởi tấm hình <br/>
dặm trường muôn lối có em bên mình <br/>
<br/>
Anh ở đâu thì em đó <br/>
Lúc núi biếc khi sông hồ <br/>
biên thùy kia nào khác chi nhà <br/>
vì tình yêu thì có đâu xa <br/>
dù biển Đông sóng gió <br/>
thì lòng luôn ghi nhớ <br/>
thì tình yêu vẫn trong ta