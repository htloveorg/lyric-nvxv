Tôi xin kể ra nơi đây <br/>
Chuyện một người thiếu nữ đẹp <br/>
Đẹp như đóa hoa lan rừng <br/>
Hương sắc ôi mặn nồng <br/>
Nàng mang tên là La Lan <br/>
La Lan người xứ cô mang <br/>
Chưa yêu đương chưa đau thương <br/>
Nên nàng chưa biết buồn <br/>
Bao trai tráng đang si mê <br/>
Chờ lọt vào đôi mắt nàng <br/>
Thường nhảy múa ca tụng nàng <br/>
Trong những đêm hội làng <br/>
Nàng La Lan vẫn ngây thơ <br/>
Vui say điệu múa côtô <br/>
Tay trong tay, cô quay quay <br/>
Chưa biết yêu là gì <br/>
Rồi thời gian trôi qua <br/>
Con người xứ Ngự bỗng vắng tiếng cười <br/>
Thường đưa đôi mắt trông xa vời <br/>
Thì ra nàng đã biết yêu rồi <br/>
Nàng yêu anh hiền tính <br/>
Nàng thương anh thật tình <br/>
Nhưng chàng trai nay đã đi rồi <br/>
La Lan buồn nát cánh hoa tươi <br/>
Qua bao tháng không tin xa <br/>
Mà người tình không trở lại <br/>
Buồn tim tím cả tâm hồn <br/>
La Lan khóc nhiều hơn <br/>
Nàng lang thang từng đêm trăng <br/>
Qua bao ghềnh đá cheo leo <br/>
Ôi La Lan <br/>
Ôi La Lan <br/>
Lan chết trên sườn đèo