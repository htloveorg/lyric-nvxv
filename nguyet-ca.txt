Từ khi trăng là nguyệt đèn thắp sáng trong tôi <br/>
Từ khi trăng là nguyệt em mang tim bối rối <br/>
Từ khi trăng là nguyệt tôi như từng cánh diều vui <br/>
Từ khi em là nguyệt trong tôi có những mặt trời <br/>
<br/>
Từ đêm khuya khi nắng sớm hay trong những cơn mưa <br/>
Từ bao la em đã đến xua tan những nghi ngờ <br/>
Từ trăng xưa là nguyệt lòng tôi có đôi khi <br/>
Tựa bông hoa vừa mọc hân hoan giây xuống thế <br/>
<br/>
Từ khi trăng là nguyệt tôi nghe đời gõ nhịp ca <br/>
Từ khi em là nguyệt cho tôi bóng mát thật là <br/>
<br/>
oOo <br/>
<br/>
Từ khi trăng là nguyệt vườn xưa lá xanh tươi <br/>
Đàn chim non lần hạt cho câu kinh bước tới <br/>
Từ khi trăng là nguyệt tôi nghe đời vỗ về tôi <br/>
Từ khi em là nguyệt câu kinh đã bước vào đời <br/>
<br/>
Từ bao la em đã đến hay em sẽ ra đi <br/>
Vườn năm xưa còn tiếng nói tôi nghe những đêm về <br/>
Từ trăng thôi là nguyệt một hôm bỗng nghe ra <br/>
Buồn vui kia là một như quên trong nỗi nhớ <br/>
<br/>
Từ trăng thôi là nguyệt tôi như giọt nắng ngoài kia <br/>
Từ em thôi là nguyệt coi như phút đó tình cờ <br/>
<br/>
 <br/>
<br/>
Từ trăng thôi là nguyệt là trăng với bao la <br/>
Từ trăng kia vừa mọc trong tôi không trí nhớ <br/>
Từ trăng thôi là nguyệt hôm nao chợt có lời thưa <br/>
Rằng em thôi là nguyệt tôi như đứa bé dại khờ <br/>
<br/>
Vườn năm xưa em đã đến nay trăng quá vô vi. <br/>
Giọt sương khuya rụng xuống lá như chân ai lần về <br/>
Từ trăng thôi là nguyệt mỏi mê đá thôi lăn <br/>
Vườn năm xưa vừa mệt cây đam mê hết nhánh <br/>
<br/>
Từ trăng thôi là nguyệt tôi như đường phố nhiều tên <br/>
Từ em thôi là nguyệt tôi xin đứng đó một mình