Đời là vạn ngày sầu biết tìm nơi chốn nào <br/>
Ta quen nhau bao lâu nhưng tình đã có gì đâu <br/>
Nhiều khi anh cũng muốn biết bao giờ sẽ có tình yêu <br/>
Cho lòng không thấy quạnh hiu khi đêm rừng buông xuống tịch liêu. <br/>
<br/>
Dù đời mình còn dài nhưng ngày vui chóng tàn <br/>
Ta yêu nhau đi thôi cho mộng không vỡ thành đôi <br/>
Từ khi anh là lính chiến không về thăm ghé nhà em <br/>
Không còn nghe tiếng cười thâu đêm buồn ơi sao là buồn. <br/>
<br/>
<br/>
      Ôi ước mơ nhiều cũng thế thôi <br/>
      Đời chỉ làm bạn cùng sương gió <br/>
      Nghe gió đêm từng cơn ru cô đơn <br/>
      Biết cho trăng đêm nay <br/>
      Chiến tranh đem thân trai đi ngàn phương <br/>
      Đời chỉ ân ái với cánh thư hồng ấp yêu. <br/>
<br/>
Rừng lá rừng chập chùng, giá lạnh trai chiến trường <br/>
Đêm nay xa quê hương xa lìa tiếng nói người thương <br/>
Ngày anh lên đường chiến đấu hoa lòng đã chớm tình yêu <br/>
Nhưng chờ đâu thấy người anh yêu chờ đến xuân về chiều