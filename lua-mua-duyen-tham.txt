Chiều dần rơi sau mái đồi ánh trăng buông lả lơi <br/>
Nhịp chày rơi như tiếng ca thiết tha xây cuộc đời <br/>
Làng thôn tôi mừng hát vui mùa lúa lên màu tươi <br/>
Hát lúa thơm vị thấm bao mồ hôi <br/>
và sớm hôm ra sức ta cày xới. <br/>
<br/>
Mừng đêm nay bông lúa vàng thắm tươi vui miền Nam <br/>
Nhịp hò quan cô gái ngoan hát vang lên dịu dàng <br/>
Hò hò quan rằng chúng em là gái sông Cửu Long <br/>
Cày cấy mong mùa tốt cho nhiều bông <br/>
Gạo trắng trong mà nên duyên mặn nồng <br/>
<br/>
Đây giòng Cửu Long lúa về mát lòng nhà nông <br/>
Vui đời no ấm lúa mùa mến người lập công. <br/>
<br/>
Lúa thắm vàng đầy đồng <br/>
Người sống với tình mặn nồng <br/>
Như cùng nhau xây tình yêu sông núi <br/>
tô màu cho nước Việt ngày thêm tươi! <br/>
<br/>
Kìa trăng soi bông lúa vàng gió reo vui miền Nam <br/>
Đồng mênh mang cô gái ngoan hát vang lên dịu dàng <br/>
Này ai ơi miền nước tôi ruộng lúa bờ đê <br/>
Người sống vui tình thắm vươn hồn quê <br/>
Gạo trắng trong mà nên duyên hẹn thề!