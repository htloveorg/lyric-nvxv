1. Chỉ thiếu một mình em chiều nay bên trời Âu xa vời <br/>
Mây kéo lê thê trời nước bao la kìa dòng sông Seine lờ lửng xuôi ngày tháng <br/>
Mùa xuân đến khắp trời, khắp nơi người ta đi lướt thướt <br/>
Riêng tôi đứng nhìn, nhớ lại ngày qua cùng em sánh vai bên dòng đời âm thầm ! <br/>
<br/>
Tôi nhớ sông Hương với giọng hò cô lái <br/>
Khi thấy sông Seine nước đục lờ không tình yêu ! <br/>
Vườn hoa đâu đây lại gợi nhớ Văn Lâu <br/>
Sao lúc xuân về mà nước non âu sầu ? <br/>
Xa cách muôn trùng bởi vì đâu còn lưu luyến ? <br/>
Ngàn năm tiếng đàn còn thiết tha tình duyên ! <br/>
<br/>
2. Ngày xuân một mình ta đàn ca xuân Việt Nam muôn đời ! <br/>
Ôi phút chia ly từ lúc xa em dòng đời ngưng trôi bên cảnh mây trời nước <br/>
Đời phồn hoa đô hi Ba Lê cùng bao nhiêu gái mới <br/>
Dáng dấp yêu kiều không làm cho ta quên khúc ái ân quê nhà đầy huy hoàng ! <br/>
<br/>
Tôi nhớ sông Hương với giọng hò cô lái <br/>
Khi thấy sông Seine nước đục lờ không tình yêu ! <br/>
Vườn hoa đâu đây lại gợi nhớ Văn Lâu <br/>
Sao lúc xuân về mà nước non âu sầu ? <br/>
Xa cách muôn trùng bởi vì đâu còn lưu luyến <br/>
Ngàn năm tiếng đàn còn thiết tha tình duyên