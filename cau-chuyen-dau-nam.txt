Trên đường đi lễ Xuân đầu năm<br/>
Qua một năm ruột rối tơ tằm<br/>
Năm mới nhiều ước vọng chờ mong<br/>
May nhiều rủi ít ngóng trông<br/>
Vui cùng pháo nổ rượu hồng<br/>
<br/>
Ta cùng nhau đón thêm mùa Xuân<br/>
Xuân dù thay đổi biết bao lần<br/>
Xin khấn nguyện kết chặt tình thân<br/>
Vin cành lộc những bâng khuâng<br/>
Năm này chắc gặp tình quân!<br/>
<br/>
Xuân mang niềm vui tới<br/>
Bao la nguồn yêu mới <br/>
Như hoa mai nở phơi phới<br/>
Thế gian thay nụ cười<br/>
Đón cho nhau cuộc đời<br/>
Trên đất mẹ tràn vui khắp nơi<br/>
<br/>
Xuân reo lộc khắp chốn<br/>
Xuân đi rồi Xuân đến<br/>
Cho nhân gian đầy lưu luyến<br/>
Đón thư nơi trận tiền<br/>
Viết thư thăm bạn hiền<br/>
Một lời nguyền xin chớ quên<br/>
<br/>
Mong đầu năm cuối năm gặp may<br/>
Gia đình luôn hạnh phúc vơi đầy<br/>
Trên bước đường danh lợi rồng mây<br/>
Duyên vừa đẹp ý đắp xây<br/>
Ôm nàng Xuân đẹp vào tay!