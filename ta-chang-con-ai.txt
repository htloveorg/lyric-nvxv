Ngày anh ra đi mang theo trái tim ai bao buồn đau <br/>
Bỏ đi rất xa nơi quê nhà có người hờn ghen đôi ta <br/>
Để em nơi đây cô đơn với nỗi đau riêng mình em <br/>
Chẳng còn ai xót thương duyên tình của đôi mình ngày sau anh ơi <br/>
<br/>
Ai đã từng khóc vì yêu <br/>
Xin hãy yêu nhau thật nhiều <br/>
(Xin hãy yêu nhau thật nhiều) <br/>
Những ai được chết vì yêu <br/>
Là đang sống trong tình yêu <br/>
(Đang sống trong tình yêu nhưng đã mất đi người yêu <br/>
<br/>
Khi con tim ai trót trao ai tiếng yêu đầu <br/>
Xin đừng đem nỗi xót xa gieo sầu trên yêu thương <br/>
(Chớ đem xót xa gieo sầu trên yêu thương) <br/>
Cô đơn anh đi để nơi đây em vẫn ngồi <br/>
Vẫn chời anh mang dấu yêu xóa dừng bao chua cay <br/>
(Anh mang dấu yêu xóa dừng bao chua cay)