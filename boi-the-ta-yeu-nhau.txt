Anh nhớ gì không anh? <br/>
Những trai hùng đi giúp non sông <br/>
Trên bốn ngàn năm qua, <br/>
Dải sơn hà đôi phen thạch mã <br/>
Gỗ đá còn gian lao, <br/>
Tiếng anh hào muôn thuở hơn nhau <br/>
Giữa cao trào thế giới dâng mau <br/>
Tựa ngàn sóng tuôn bờ đại dương. <br/>
<br/>
Anh nhớ gì không anh? <br/>
Những anh tài phiêu lãng phương xa <br/>
Vui sướng gì đâu anh, <br/>
Chốn quê người vui riêng hạnh phúc <br/>
Chim Bắc cành phương Nam, <br/>
Há chi người ơn nghĩa thâm sâu <br/>
Lúc cơ cầu ta có bên nhau <br/>
Cùng chung một tiếng nói yêu Việt Nam. <br/>
<br/>
Ôi nước mắt dân Hời <br/>
Thành quách một thời <br/>
Tan tành hết bởi đâu? <br/>
Sao ta nỡ xa ta <br/>
Rẽ đôi sơn hà <br/>
Cho Bắc - Nam mình xa cách nhau. <br/>
<br/>
Anh nhớ gì không anh? <br/>
Giữa thanh bình hay lúc gian nguy <br/>
Xin hết lòng chung lo <br/>
Bản dư đồ ông cha nhọc khó <br/>
Trên bước đường tương lai, <br/>
Kết tâm đồng một dải non sông <br/>
Bắc - Nam cùng dòng giống tinh anh <br/>
Trời Đông một cõi núi sông Việt Nam. <br/>
<br/>
Trời Nam một cõi minh châu trời Đông.