Từ một ngày xa trước anh đưa em về bóng ngã đam mê <br/>
Em dấu son gót mềm nhủ lòng lãng quên mà nhớ đêm đen <br/>
Chuyện một lần yêu ai như chuyện một đời con gái <br/>
Cho anh một lần anh được gì không, em còn gì không? <br/>
<br/>
Ôi những câu chuyện lòng làm thơm ngát thêm tuổi hồng <br/>
Anh ơi yêu đi yêu đi trên đỉnh yêu đương gió tỏa thêm hương <br/>
Ôi những câu chuyện lòng từ lâu vẫn như mùa đông <br/>
Anh ơi yêu đi yêu đi nếm thử thương đau <br/>
Khi hạnh phúc qua mau <br/>
<br/>
Kể từ sau đêm đó sân vui đại học <br/>
Mất tiếng chim ca ho dẫu không xóa nhòa <br/>
Thì rồi cũng qua tình cũng bay xa <br/>
Ngàn ngày trôi xa vắng chưa cạn một lần cay đắng <br/>
Xa nhau một đời, em còn gì đâu anh còn gì đâu <br/>
<br/>
Ôi những câu chuyện lòng <br/>
Làm thơm ngát thêm tuổi hồng <br/>
Anh ơi yêu đi yêu đi <br/>
Trên đỉnh yêu đương gió tỏa thêm hương <br/>
<br/>
Ôi những câu chuyện lòng từ lâu vẫn như mùa đông <br/>
Anh ơi yêu đi yêu đi nếm thử thương đau <br/>
<br/>
Khi hạnh phúc qua mau kể từ sau đêm đó <br/>
Sân vui đại học mất tiếng chim ca <br/>
Cho dẫu không xóa nhòa <br/>
Thì rồi cũng qua tình cũng bay xa <br/>
Anh anh còn gì đâu và em em còn gì đâu <br/>
<br/>
Xin cho em thêm một ngày nữa thôi <br/>
Xin cho em thêm một đời thương nhớ