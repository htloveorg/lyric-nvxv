Nhớ quá màu mắt em hiền hòa xa xăm. <br/>
Lang thang chiều nắng buông dần tàn héo hắt. <br/>
Bóng tối về mau, ai ngóng ai buồn sâu. <br/>
Cô đơn từng đêm gió mưa ôi lòng đau. <br/>
<br/>
Nước mắt chiều ấy sao ngàn đời chia ly. <br/>
Em đi về cuối phương trời nào nhung nhớ. <br/>
Có biết tình ta ôm bóng đêm hoài mong. <br/>
Tơ duyên về đâu trách ai quên câu thề. <br/>
<br/>
Giờ xa cách xa sầu riêng người ơi. <br/>
Dáng em khôn tìm thôi tàn phai. <br/>
Chiều xưa ta trót (...còn thiếu...) <br/>
Trót đi tìm lấy câu ca buồn. <br/>
<br/>
Khuất bóng kìa cánh chim chiều tà đơn côi. <br/>
Bay đi về cuối phương trời nào xa xăm. <br/>
Nhớ nhớ hoài tơ vương dáng hoa ngày nào. <br/>
Sao quên chiều xưa ấy trăng sao ơ thờ.