Đi về đâu hỡi em? <br/>
Khi trong lòng không chút nắng <br/>
Giấc mơ đời xa vắng <br/>
Bước chân không chờ ai đón <br/>
Một đời em mãi lang thang <br/>
Lòng lạnh băng giữa đau thương <br/>
<br/>
Em về đâu hỡi em <br/>
Hãy lau khô dòng nước mắt <br/>
Đời gọi em biết bao lần <br/>
Đời gọi em về giữa yêu thương <br/>
Để trả em ngày tháng êm đềm <br/>
Trả lại nắng trong tim <br/>
Trả lại thoáng hương thơm <br/>
<br/>
Em về đâu hỡi em <br/>
Có nghe tình yêu lên tiếng <br/>
Hãy chôn vào quên lãng <br/>
Nỗi đau hay niềm cay đắng <br/>
Đời nhẹ nâng bước chân em <br/>
Về lại trong phố thênh thang <br/>
Bao buồn xưa sẽ quên <br/>
Hãy yêu khi đời mang đến <br/>
Một cành hoa giữa tâm hồn.