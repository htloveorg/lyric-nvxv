Mẹ thường bảo con thân gái đục trong <br/>
Mai kia mốt nọ con đi lấy chồng <br/>
Giờ thì ba má chìu cưng <br/>
Chẳng thèm liệu lo lấy thân <br/>
Lỡ mai về bên khó lòng <br/>
<br/>
Nhà người ta xem chẳng dễ gì đâu <br/>
Ba bên bốn họ nhìn ra ngó vào <br/>
Họ hàng cô bác thì đông <br/>
Mẹ chồng em dâu biết không? <br/>
Đứng ngồi nói năng lựa lời <br/>
<br/>
Dù người ta thương yêu <br/>
Con nhé đừng xem là thường <br/>
Xuất giá rồi thì tòng phu con nhé <br/>
Công ngôn dung đức hạnh <br/>
Trọn đời con thờ chồng <br/>
Giữ gìn tiếng sạch trong <br/>
Danh giá ta thủy chung <br/>
<br/>
Học hành đi nghe con gái mẹ ngoan <br/>
Mai kia mốt nọ còn đi lấy chồng <br/>
Dặm ngàn thân gái đục trong <br/>
Một lần xe hoa pháo hồng <br/>
Suốt đời khắc ghi trong lòng.