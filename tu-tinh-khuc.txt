Tôi như trẻ nhỏ ngôi bên hiên nhà <br/>
chờ nghe thế kỷ tàn phai <br/>
Tôi như trẻ nhỏ tìm nơi nương tựa <br/>
mà sao vẫn cứ lạc loài. <br/>
<br/>
Tôi như là người lạc trong đô thị <br/>
một hôm đi về biển khơi <br/>
Tôi như là người một hôm quay lại <br/>
vì nghe sa mạc nối dài <br/>
Đừng nghe tôi nói lời tăm tối <br/>
Đừng tin tôi nhé vì tiếng cười <br/>
<br/>
Đôi khi một người dường như chờ đợi <br/>
thật ra đang ngồi thảnh thơi <br/>
Tôi như là người ngồi trong đêm dài <br/>
nhìn tôi đang quá ngậm ngùi <br/>
<br/>
Một hôm buồn ra ngắm dòng sông <br/>
Một hôm buồn lên núi nằm xuống <br/>
<br/>
Tôi đi tìm ngày tìm đêm lâu dài <br/>
một hôm thấy được đời tôi <br/>
Tôi yêu mọi người cỏ cây <br/>
muôn loài làm sao yêu hết cuộc đời <br/>
<br/>
Tôi như đường về mở ra đô thị <br/>
chờ chân thiên hạ về vui <br/>
Tôi như nụ cười nở trên môi người <br/>
phòng khi nhân loại biếng lười <br/>
Tìm tôi đi nhé đừng bối rối <br/>
Đừng mang gươm giáo vào với đời <br/>
<br/>
Tôi như ngọn đèn từng đêm vơi cạn <br/>
lửa lên thắp một niềm riêng <br/>
Tôi như nụ hồng nhiều khi ưu phiền <br/>
chờ tôi rã cánh một lần.