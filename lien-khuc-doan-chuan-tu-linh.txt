Dìu nhau vào cơn mê bên lề cuộc sống. <br/>
Vòng tay đầy hơi thở nồng ấm. <br/>
Dìu nhau vào tiếng hát rong rêu suởi cho nhau tình yêu cho tuổi mộng không hắt hiu. <br/>
<br/>
Kề vai dìu nhau đi dù đường lạc lối. <br/>
Trong sương mù vay hết kiếp u mê. <br/>
Một mai đời ta có phiêu bồng. <br/>
Ghép nụ sầu ngàn sau dõi tìm nhau <br/>
Dù biết lối đi này cuộc tình gãy cánh đêm trăng nào lẻ loi. <br/>
Và những chiếc hôn này người tình tôi dấu yêu muôn đời sầu nào sẽ ghé trên môi người như thoáng rơi trong giấc mơ tả tơi. <br/>
<br/>
Dù mai cuộc tình ta vội vàng rụng xuống. <br/>
Trên tay người hoa khép kín đôi mi. <br/>
Thì xin người hãy ghé môi sầu <br/>
Cho lệ tình ngàn sau ấm lòng nhau.