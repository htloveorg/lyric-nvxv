Đêm công viên buồn đơn côi <br/>
Ta xa nhau lâu lắm rồi <br/>
Đêm trăng soi buồn ghế đá <br/>
Nhạt nhòa trên hoa lá <br/>
Riêng có em mà thôi. <br/>
Xin cho tôi một niềm tin <br/>
Anh ra đi anh sẽ về <br/>
Xin cho màu trăng nguyên thủy <br/>
Của ngày xưa hai đứa <br/>
Soi bước chân chàng đi. <br/>
<br/>
ĐK: <br/>
Từng bước chân buồn gieo âm thầm trên hè phố đêm kinh đô <br/>
Sương khuya ướt vai gầy <br/>
Hồn em nhỏ bé nghe bơ vơ giữa kinh thành mong tin anh <br/>
Sắt se gợi nỗi niềm. <br/>
Đêm rưng rưng màu trời xanh <br/>
Xin Ơn Trên ban phước lành <br/>
Xin ân tình không thay đổi <br/>
Đường đời không hai lối <br/>
Duyên kiếp anh và tôi...!!