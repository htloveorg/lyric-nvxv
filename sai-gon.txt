Dừng chân trên bến khi chiều nắng chưa phai, <br/>
Từ xa thấp thoáng muôn tà áo tung bay <br/>
Nếp sống vui tươi nối chân nhau đến nơi này <br/>
Saigon đẹp lắm, Saigon ơi ! Saigon ơi ! <br/>
<br/>
Ngựa xe như nước trên đường vẫn qua mau <br/>
Người ra thăm bến câu chào nói lao xao <br/>
Phố xa thênh thang đón chân tôi đến chung vui <br/>
Saigon đẹp lắm, Saigon ơi ! Saigon ơi ! <br/>
<br/>
Lá la la lá la <br/>
Lá la la lá la <br/>
Tiếng cười cùng gió chan hòa niềm vui say sưa. <br/>
Lá la la lá la <br/>
Lá la la lá la <br/>
Ôi đời đẹp quá, tràn bao ý thơ. <br/>
<br/>
Một tình yêu mến ghi lời hát câu ca <br/>
Để lòng thương nhớ bao ngày vắng nơi xa. <br/>
Sống mãi trong tôi bóng hôm nay sẽ không phai <br/>
Saigon đẹp lắm, Saigon ơi ! Saigon ơi !