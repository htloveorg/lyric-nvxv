Năm năm mỗi lần nghe hè đến <br/>
Lòng đắm đuối những giờ phút hè xưa <br/>
Hồn lâng lâng như buồn nhớ bâng khuâng <br/>
Và thương tiếc vô cùng ... <br/>
<br/>
Tôi đâu quên người em hè ấy <br/>
Tà áo trắng trong màu nắng mộng mơ <br/>
Chiều chia ly nên lòng thấy bơ vơ <br/>
Tìm tôi giữa sân trường <br/>
<br/>
Nhớ người em xưa , <br/>
màu mắt xanh, Tình thơ ngây, <br/>
Thường men lối ven sông <br/>
Tìm nhặt cánh hoa tàn ... <br/>
<br/>
Đem về giấu ép, <br/>
Rồi cắt cánh thành con tim <br/>
Tìm đem đến cho tôi <br/>
Để làm chút tin duyên <br/>
<br/>
Năm năm mỗi lần nghe hè đến <br/>
Thường tới chốn ấy tìm bóng người em <br/>
Thường tới chốn ấy tìm cánh hoa tim <br/>
Mà chẳng biết đâu tìm ... <br/>
<br/>
Nên năm năm nhìn hoa phượng thắm <br/>
Nhìn mái ngói nét trường cũ điều hiu <br/>
Nhìn ánh nắng trên giòng nước cô liêu <br/>
Lòng chợt thấy tiêu điều