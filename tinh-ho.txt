(Saigon 1975)<br/>
<br/>
Tôi đang lừa dối em<br/>
Mà sao em không biết<br/>
Những lời nói tình duyên<br/>
Với tôi không cần thiết<br/>
Chớ nên thề thốt chi<br/>
Ðùa vui thôi đấy nhé<br/>
Say đắm và si mê<br/>
Sẵn sàng đang nhạt nhoè.<br/>
Tội nghiệp quá, xây những lâu đài cát mơ<br/>
Biển vắng trong chiều sắp mưa<br/>
Tình cũng như là đám mây mịt mù<br/>
Tình là nhớ, xin nhớ không lừa dối ai<br/>
Ðừng nói câu chuyện lứa đôi<br/>
Tình cũng như dòng nước trôi.<br/>
Khi tôi tìm đến em<br/>
Là tìm vui trong chốc lát<br/>
Ðến một lúc rồi quên<br/>
Nhớ nhung không cần thiết<br/>
Khi em hiểu rõ tôi<br/>
Yêu nghĩa là phai phôi<br/>
Nghĩa là mang hận hoài.