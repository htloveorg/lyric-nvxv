Một mai xa nhau xin nhớ cho nhau nụ cười <br/>
Cho cuộc tình người hẹn hò nhau đến kiếp mai <br/>
Đừng hận nhau nữa lệ nào em khóc cho đầy <br/>
Tình đã ngủ trong sương khói, theo cơn gió lùa tả tơi <br/>
<br/>
Một mai em đi ngày tháng bơ vơ giận hờn <br/>
Nhớ về tình người buồn như con nước đã vơi <br/>
Lời nào gian dối để người đã lỡ một giờ <br/>
Một đêm nào em bỡ ngỡ buông tay ngậm ngùi xót xa <br/>
<br/>
Cho nhau bao nhiêu yêu dấu bên cuộc đời này <br/>
Nên đôi tay không cầm như nước đời lạnh lùng <br/>
Ý còn trong lòng tình nào mơ dấu chim bay <br/>
Còn nhau giữa cơn mê đầy <br/>
Khiến hao gầy buồn nét xuân xanh <br/>
<br/>
Một mai em đi gọi gió thả mây về ngàn <br/>
Xin tạ lòng người tình ta hư không thế thôi <br/>
Đời vui không mấy niềm đau đã chín kiếp người <br/>
Lòng đâu phụ nhau thêm nữa khi mai không còn có nhau