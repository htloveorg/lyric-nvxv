Chuyện tình đôi mươi chan chứa không bao giờ vơi<br/>
Như dòng suối tình êm ái<br/>
Có anh và em còn ai còn ai nữa<br/>
Đã yêu nhau trong cuộc đời<br/>
Chuyện mình từ một chiều dừng chân trú mưa<br/>
Ta bên nhau nhìn công viên lá đổ<br/>
Tuy chưa quen mà sao tình như đã,<br/>
dẫu ngoài còn e.<br/>
<br/>
Trời làm mưa tuôn nên khiến xui anh gặp em<br/>
Cho mình kết lời hẹn ước<br/>
Cớ sao trời cho tình yêu rồi ngăn cách<br/>
Mấy ai không rơi lệ sầu<br/>
Tình vừa nồng thì vừa được tin xót thương<br/>
Em ra đi về bên kia cõi đời<br/>
Xe tang lăn buồn trong lòng phố vắng,<br/>
khóc em âm thầm...<br/>
<br/>
Ôi! Em về đâu?<br/>
Vùi lấp mối duyên đầu<br/>
Anh ơi còn đâu?<br/>
Tuổi xuân mình đang chớm<br/>
Nay em về đâu?<br/>
Về thế giới xa nào?<br/>
Cho đời hiu hắt như nghĩa trang<br/>
<br/>
Một mình lê bước anh đến công viên ngày xưa<br/>
Nghe làn gió buồn xao xác<br/>
Ngỡ linh hồn em tựa theo ngàn cơn gió<br/>
oán than khóc duyên ban đầu<br/>
Nầy là vườn kỷ niệm ngày ta mới quen,<br/>
Đây công viên chiều xưa thêu mối tình,<br/>
Nay không em vườn hoang buồn xơ xác,<br/>
nghĩa trang lạnh lùng...