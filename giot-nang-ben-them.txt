Hoa vẫn hồng trước sân nhà tôi <br/>
Chim vẫn hót sau vườn nhà tôi <br/>
Giọt nắng bâng khuâng <br/>
Giọt nắng rơi rơi bên thềm <br/>
Bài hát bâng khuâng <br/>
Bài hát mang bao kỷ niệm <br/>
Những ngày đã qua <br/>
<br/>
Lâu lắm rồi anh không đến chơi <br/>
Cây sen đã lá bạc như vôi <br/>
Sỏi đá rêu phong <br/>
sỏi đá chưa quên chân người <br/>
Bài hát rêu phong <br/>
bài hát viết không nên lời <br/>
đã vội ... lãng quên <br/>
<br/>
Bài hát tìm trong nỗi nhớ từng ngày bình yên <br/>
Bài hát tìm trong ký ức cuộc tình đầu tiên <br/>
Trả lại cho tôi, trả lại cho anh <br/>
Trả về hư không giọt nắng bên thềm <br/>
<br/>
Hoa vẫn hồng trước sân nhà tôi <br/>
Chim vẫn hót sau vườn nhà tôi <br/>
Một sớm mai kia <br/>
Chợt thấy hư vô trong đời <br/>
Người vẫn đâu đây, người cũng đã như xa rồi <br/>
Chỉ là ... thế thôi <br/>
<br/>
Khi thấy buồn anh cứ đến chơi <br/>
Chim vẫn hót trong vườn đấy thôi <br/>
Chỉ có trong tôi ngày đã sang đêm lâu rồi <br/>
Bài hát cho anh giờ đã hát cho mọi người <br/>
Để rồi lãng quên <br/>
<br/>
Bài hát tìm trong khói thuốc từng giờ bình yên <br/>
Bài hát tìm trong lá biếc từng chiều hoàng hôn <br/>
Còn lại trong tôi, còn lại trong anh <br/>
Chỉ là lung linh giọt nắng bên thềm <br/>
Trả lại cho tôi, trả lại cho anh <br/>
Trả về hư không giọt nắng bên thềm