Hồn lỡ sa vào đôi mắt em <br/>
Chiều nao xõa tóc ngồi bên rèm <br/>
Thầm ước nhưng nào đâu dám nói <br/>
Khép tâm tư lại thôi <br/>
Đường hoa vẫn chưa mở lối <br/>
<br/>
Đời lắm phong trần tay trắng tay <br/>
Trời đông ngại gió lùa vai gầy <br/>
Lầu kín trăng về không lối chiếu <br/>
Gác cao ngăn niềm yêu <br/>
Thì thôi mơ ước chi nhiều <br/>
<br/>
Bên nhau sao tình xa vạn lý cách biệt mấy sơn khê <br/>
Ngày đi mắt em xanh biển sâu, mắt tôi rưng rưng sầu <br/>
Lặng nghe tiếng pháo tiễn ai qua cầu <br/>
<br/>
Đường phố muôn màu sao thiếu em <br/>
Về đâu làn tóc xõa bên rèm <br/>
Lầu vắng không người song khép kín <br/>
Nhớ em tôi gọi tên, chỉ nghe lá rơi bên thềm