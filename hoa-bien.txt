Ngày xưa em anh hay hờn dỗi <br/>
Giận anh khi anh chưa kịp tới <br/>
Cho anh nhiều lời, cho anh bồi hồi <br/>
Em cúi mặt làm ngơ <br/>
Không nghe kể chuyện <br/>
Bao nhiêu chuyện tình đẹp nhất trên trần đời <br/>
<br/>
Tại em khi xưa yêu màu trắng <br/>
Tại em suy tư bên bờ vắng <br/>
Nên đêm vượt trùng <br/>
Anh mong tìm gặp hoa trắng về tặng em <br/>
Cho anh thì thầm <br/>
Em ơi tình mình trắng như hoa đại dương <br/>
<br/>
Trùng khơi nổi gió lênh đênh triền sóng <br/>
thấy lung linh rừng hoa <br/>
Màu hoa thật trắng, ôi hoa nở thắm <br/>
ngất ngây lòng thêm <br/>
<br/>
Vượt bao hải lý chưa nghe vừa ý <br/>
lắc lư con tàu đi <br/>
Chỉ thấy bọt nước tan theo ngọn sóng <br/>
dáng hoa kia mịt mùng <br/>
<br/>
Biển khơi không mang hoa màu trắng <br/>
Tàu anh xa xôi chưa tìm bến <br/>
Nên em còn buồn, nên em còn hờn <br/>
Sao chưa thấy anh sang <br/>
Em ơi giận hờn <br/>
Xin như hoa sóng tan trong đại dương.