Theo năm tháng hoài mong <br/>
thư gởi đi mấy lần <br/>
đợi hồi âm chưa thấy <br/>
em ơi nhớ rằng đây <br/>
còn có anh đêm ngày <br/>
hằng thương nhớ vơi đầy <br/>
<br/>
Ngày đi mình đã hứa <br/>
toàn những lời chan chứa <br/>
còn hơn gió hơn mây <br/>
mỗi tuần một lần thư <br/>
kể nghe chuyện sương gió <br/>
kể nghe niềm ước mơ <br/>
<br/>
Nhưng em vắng hồi thư <br/>
thế là em hững hờ <br/>
hoặc là em không nhớ <br/>
anh đâu khác người xưa <br/>
ngày lẫn đêm mong chờ <br/>
tình yêu nói sao vừa <br/>
<br/>
Từ lâu đành xa vắng <br/>
đời trăm ngàn cay đắng <br/>
hỏi em có hay không <br/>
chỉ cần một hồi âm <br/>
là anh mừng vui lắm <br/>
cớ sao em phụ lòng... <br/>
<br/>
Ngày xưa em còn nhớ <br/>
nàng Tô Thị bồng con ngóng trông <br/>
thời gian đã hoài công <br/>
nàng thành đá ngàn kiếp yêu thương chồng <br/>
<br/>
Ngày nay anh nào thấy <br/>
lòng chân thành của thế nhân <br/>
tìm đâu trong tình yêu <br/>
được bền lâu để kiếp sau không sầu <br/>
<br/>
Anh mơ ước làm sao <br/>
cho trọn mối duyên đầu <br/>
đẹp lòng em yêu dấu <br/>
xưa Chức Nữ chàng Ngưu <br/>
từng đắng cay dãi dầu <br/>
chờ Ô Thước bắt cầu <br/>
<br/>
Còn em từ xa cách <br/>
làm anh hờn anh trách <br/>
hỏi em có hay không <br/>
mỏi mòn đợi hồi âm <br/>
thềm xưa giờ vắng bóng <br/>
nhớ thương ai ngập lòng...