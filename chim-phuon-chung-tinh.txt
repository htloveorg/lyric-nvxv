Anh là người lính chung tình <br/>
Gió sương trên vạn nẻo đường <br/>
Ngày đêm anh nhớ em <br/>
Nhớ mái tóc em buông lơi <br/>
Nhớ đôi môi em cười <br/>
<br/>
Dù cho đời lính xa nhà <br/>
Mỗi đêm anh mơ thấy nàng <br/>
Nụ cười như đóa hoa <br/>
Với ánh mắt em mơ màng <br/>
Lối đi không còn lẻ loi <br/>
<br/>
Nhớ em thì nhớ thương nhiều <br/>
Nước non còn đó em ơi <br/>
Ðời trai chinh chiến phiêu linh <br/>
Và trọn đời anh thề sẽ chung tình <br/>
Sẽ không quên ngày chúng mình <br/>
Cùng nhau chung hướng đi <br/>
Lúc đất nước thôi chia ly <br/>
Chúng ta vui đẹp đôi