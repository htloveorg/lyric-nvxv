Hãy yêu nhau đi khi rừng thay lá <br/>
Hãy yêu nhau đi, giòng nước đã trôi xa <br/>
Nước trôi qua tim rong đầy trí nhớ <br/>
Ngày mãi mong chờ, ngày sẽ thiên thu <br/>
<br/>
Hãy ru nhau trên những lời gió mới <br/>
Hãy yêu nhau cho gạch đá có tin vui <br/>
Hãy kêu tên nhau trên ghềnh dưới bãi <br/>
Dù mai nơi này người có xa người <br/>
<br/>
Hãy yêu nhau đi quên ngày u tối <br/>
Dù vẫn biết mai đây xa lìa thế giới <br/>
Mặt đất đã cho ta những ngày vui với <br/>
Hãy nhìn vào mặt người lần cuối trong đời <br/>
<br/>
Hãy yêu nhau đi bên đời nguy khốn <br/>
Hãy yêu nhau đi bù đắp cho trăm năm <br/>
Hãy yêu nhau đi cho ngày quên tháng <br/>
Dù đêm súng đạn, dù sáng mưa bom <br/>
<br/>
Hãy trao cho nhau muôn ngàn yêu dấu <br/>
Hãy trao cho nhau hạnh phúc lẫn thương đau <br/>
Trái tim cho ta nơi về nương náu <br/>
Được quên rất nhiều ngày tháng tiêu điều