Một mùa thu tàn úa, lá vàng rơi khắp sân<br/>
Mình anh nơi đây cô đơn lặng lẽ <br/>
Từ khi em ra đi, từng hàng cây trước sân <br/>
Dường như cũng đã xác xơ đi nhiều<br/>
<br/>
Rồi mùa thu đi qua, khi mùa đông đã về<br/>
Chờ mong tin em nhưng sao chẳng thấy <br/>
Người yêu ơi, em có còn yêu anh nữa không<br/>
Mà sao không thấy một lời cho nhau<br/>
<br/>
Người yêu ơi có biết anh nhớ em nhiều lắm<br/>
Những đêm trong giấc mơ tay nắm tay nghẹn ngào <br/>
Lòng hạnh phúc biết bao ngỡ rằng em còn đây <br/>
Nụ hôn trao ngất ngây, ôi tình yêu tuyệt vời<br/>
<br/>
Người yêu ơi có biết anh nhớ em nhiều lắm<br/>
Đã bao năm tháng qua anh vẫn mong vẫn chờ <br/>
Giờ em đang ở đâu hãy về đây bên anh <br/>
Tình yêu ta thắp lên cho mùa xuân xanh ngời