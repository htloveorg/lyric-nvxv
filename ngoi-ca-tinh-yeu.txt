(nhạc: Phạm Đình Chương - lời: Hoàng Thanh Tâm)<br/>
<br/>
Tôi chờ đợi lớn lên cùng giông bão, <br/>
Hôm nay tuổi nhỏ khóc trên vai , <br/>
Hôm nay tuổi nhỏ khóc trên vai . <br/>
Tìm cánh tay nước biển, <br/>
Con ngựa buồn lửa trốn con ngươi (hm ..hm...) <br/>
Đất nước có một lần tôi ghì đau thương trong thân thể, <br/>
Những giòng sông, những đường cày, núi nhọn. <br/>
Những biệt ly, những biệt ly rạn nứt lòng đường. <br/>
Hút chặt mười ngón tay, ngón chân da thịt <br/>
Như người yêu, như người yêu từ chối vùng vằng. <br/>
Những giòng sông, những đường cày, núi nhọn. <br/>
<br/>
Những biệt ly, những biệt ly rạn nứt lòng đường. <br/>
Tôi chờ đợi cười lên sặc sỡ, la qua mái ngói thành phố ruộng đồng, <br/>
Bấu lấy tim tôi thành nhịp thở <br/>
Ngõ cụt đường làng cỏ hoa cống rãnh <br/>
Cây già đá sỏi bùn nước mặn nồng chảy máu tiếng kêu.