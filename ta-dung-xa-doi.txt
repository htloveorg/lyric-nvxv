Hãy bay đi những bụi mưa trên đường về chuyện xưa <br/>
Hãy bay đi, bay đi những giọt vươn trên hồn người hằn nhớ <br/>
Một dĩ vãng hắt hiu hơi buồn, về lãng đãng giữa cơn mưa phùn <br/>
Một dĩ vãng có mưa là lệ ta khóc nhau <br/>
<br/>
Hãy bay đi những bụi mưa trên hồn người lẻ loi <br/>
Hãy bay đi kỷ niêm xưa trong đời tình mòn mỏi <br/>
Người dẫu khuất cuối chân mây rồi <br/>
Tình dẫu chết lúc xuân xanh đời <br/>
Dòng nước mắt vẫn không ngừng lại trong trái tim <br/>
<br/>
Bay đi những cơn mưa phùn <br/>
Bay đi những cơn đau buồn <br/>
Những cơn mưa phùn là lệ khôn nguôi <br/>
Phất phơ trong trời kỷ niệm xa xôi <br/>
Hãy bay đi nhé những giọt buồn ơi <br/>
Bay đi ...Bay đi ... <br/>
<br/>
Hãy bay đi những bụi mưa trên nghìn trùng đời nhau <br/>
Hãy bay đi kỷ niêm xưa bên bờ thành phượng cũ <br/>
Dòng tiếc nuối xót xa hoa thề <br/>
Chiều phố cũ em về ... <br/>
Và tiếng hát bỗng dưng thành lời ru lãng quên <br/>
<br/>
Hãy bay đi những bụi mưa chia nghìn đời tình đôi <br/>
Hãy bay đi kỷ niêm xưa xanh ngời lệ hờn dỗi <br/>
Tình đắm đuối đã chia nhau rồi <br/>
Người vĩnh viễn đã chia xa người <br/>
Và nước mắt đã bay thành lệ mưa giữa trời <br/>
<br/>
Bay đi những cơn mưa phùn <br/>
Bay đi những cơn đau buồn <br/>
Những cơn mưa phùn là lệ không nguôi <br/>
Phất phơ trong trời kỷ niệm xa xôi <br/>
Hãy bay đi nhé những giọt buồn ơi <br/>
Bay đi ...Bay đi ...