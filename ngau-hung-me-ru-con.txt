Hãy ngủ đi con giấc ngủ no tròn <br/>
Êm ả say nồng con ngủ cho ngon <br/>
Giấc ngủ ngây thơ để mẹ ước mơ <br/>
Ấp ủ trên tay những đường chỉ may <br/>
<br/>
Múi chỉ yêu thương nhắc nhở luôn rằng <br/>
Bên những cánh đồng hay khắp nẽo quê hương <br/>
Áo bà ba đẹp sao mà thon thả <br/>
Dáng thiệt thà, vẫn mượt mà <br/>
Như lũy tre quê nhà khoe dáng bao ngày qua <br/>
<br/>
Vất vả trên nương áo vẫn bên mình <br/>
Mến cả ân tình che nắng che sương <br/>
Áo dẫu đơn sơ cũng đầy ý thơ <br/>
Áo ở trong mưa để lòng ngẩn ngơ <br/>
<br/>
Áo chẳng kiêu sa áo cũng khoe tà <br/>
Như những câu hò chân chất của quê ta <br/>
Áo bà ba mẹ may chờ con lớn <br/>
Sẽ mặn môi, nở miệng cười <br/>
Xinh xắn thêm chan hòa trong chiếc áo bà ba <br/>
<br/>
Ơi ..à á ơi ...ơi à á ơi <br/>
Ơi ..ớ ớ ơi à ơi .. ơi ớ ớ ơi à ơi