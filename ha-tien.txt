Tôi nhớ hoài một chiều dừng chân ghé qua thăm miền ước mơ <br/>
Hà Tiên mến yêu đẹp như xứ thơ xa cách tôi còn nhớ <br/>
Nhớ ghi muôn đời nước trời biển mơ <br/>
xanh xanh màu ánh mắt em gái chiều năm xưa <br/>
như vấn vương ai trên bến chiều xa vắng năm tháng còn ngẩn ngơ <br/>
<br/>
Hà Tiên ơi, đây miền xinh tươi như hoa gấm trong đời <br/>
Hà Tiên ơi, đây những bóng dừa xanh mát biển khơi <br/>
<br/>
Tôi qua lăng Mạc Cửu, nằm trên con voi cùn <br/>
Tôi vô thăm Thạch Động, trời bát ngát mênh mông <br/>
Nghe chuông ngân chiều vắng như tiếng nói cô miên <br/>
xao xuyến tâm tư người ghé thăm Hà Tiên <br/>
<br/>
Giây phút đẹp còn lại kỷ niệm khó phai trên bờ mắt ai <br/>
Hà Tiên đã ghi vào tâm trí tôi ôi luyến lưu làng mây <br/>
Nhớ thương với đầy hướng về Hà Tiên <br/>
Quê hương hùng vĩ hiên ngang ngắm mặt trùng dương <br/>
Đây bến Tô Châu khôn sánh niềm lưu luyến tôi hướng về Hà Tiên <br/>
<br/>
ĐK: <br/>
<br/>
Hà Tiên ơi, đây miền xinh tươi như hoa gấm trong đời <br/>
Hà Tiên ơi, đây những bóng dừa xanh mát biển khơi <br/>
<br/>
<br/>
Tôi qua lăng Mạc Cửu, nằm trên con voi cùn <br/>
Tôi vô thăm Thạch Động, trời bát ngát mênh mông <br/>
Nghe chuông nghiêng chiều vắng, như tiếng nói cô miên <br/>
xao xuyến tâm tư người ghé thăm Hà Tiên <br/>
<br/>
Giây phút đẹp còn lại kỷ niệm khó phai trên bờ mắt ai <br/>
Hà Tiên đã ghi vào tâm trí tôi ơi luyến lưu làn mây <br/>
Nhớ thương với đầy hướng về Hà Tiên <br/>
Quê hương hùng vĩ hiên ngang ngắm mặt trùng dương <br/>
Đây bến Tô Châu khôn sánh niềm lưu luyến tôi hướng về Hà Tiên