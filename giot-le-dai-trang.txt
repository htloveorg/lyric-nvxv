Ngày xưa ai lá ngọc cành vàng <br/>
Ngày xưa ai quyền quý cao sang <br/>
Em chính em ngày xưa đó <br/>
Ước xây đời lên tột đỉnh nhân gian <br/>
<br/>
Ngày xưa ai tiếng nhạc cung đàn <br/>
Ngày xưa anh nghệ sĩ lang thang <br/>
Tôi chính tôi ngày xưa đó <br/>
Cũng đèo bòng mơ người đẹp lầu hoa <br/>
<br/>
Rồi một hôm tôi gặp nàng <br/>
Ðem tiếng hát cung đàn <br/>
Với niềm yêu lai láng <br/>
Nhưng than ôi quá bẽ bàng <br/>
Bao tiếng hát cung đàn <br/>
Người chẳng màng còn chê chán <br/>
<br/>
Nhìn đời thấy lắm phũ phàng <br/>
Mượn tiếng hát cung đàn <br/>
Với niềm đau dĩ vãng <br/>
Nhưng bao giông tố lan tràn <br/>
Lên gác tía huy hoàng <br/>
Xiêu đổ theo nước mắt nàng <br/>
<br/>
Còn đâu đâu lá ngọc cành vàng <br/>
Còn đâu đâu quyền quý cao sang <br/>
Em chính em ngày xưa đó <br/>
Ðến bây giờ phiêu bạt giữa trần gian <br/>
<br/>
Ðời tôi vẫn tiếng nhạc cung đàn <br/>
Ðời tôi vẫn nghệ sĩ thênh thang <br/>
Em, em nhớ xưa rồi em khóc <br/>
Tôi thoáng buồn thương giòng lệ đài trang