Đời xa tôi! Tình xa tôi, đành bỏ tôi rồi .... <br/>
Đời bôn ba, tình xa hoa người mới xa người ... <br/>
Nay ta còn lại trong dĩ vãng, <br/>
Với những ngày vàng son êm ái... <br/>
Phút chốc thành bờ mây phiêu lãng...về xa xôi ... <br/>
Đường em đi, hồng đôi chân người đón em về! <br/>
Đường tôi đi mờ hơi sương vực đá rêu mòn ..... <br/>
<br/>
Tôi xin đời một hai tấc đất! <br/>
Cho tôi đặt bàn chân trong đó .... <br/>
Nhưng sao đời đành xô tôi ngã khỏi vùng yêu thương ..... <br/>
Đến với nhau! rồi đi, đắng môi khi biệt ly! <br/>
Tấm aó thay thật nhanh, tình hồng tung đôi cánh .... <br/>
Vì tình đời thường lầm lỡ, nên tình người thường đổ vỡ... <br/>
<br/>
Giấc mơ đầu qua mất rồi giờ mình nghe bơ vơ ... <br/>
Bạn thân ơi ! Người yêu ơi giờ bỏ tôi rồi ... <br/>
Để tôi đi về đơn côi lạc lõng trên đời .... <br/>
Tôi mong đợi một tình thân mến ... <br/>
Đến xoa dịu hồn tôi khô héo... <br/>
Đếm tháng ngày nhiều đêm tôi khóc bởi người xa tôi