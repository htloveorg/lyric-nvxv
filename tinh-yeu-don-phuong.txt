Ta đã yêu rồi người ấy đâu hay <br/>
Để năm tháng vô tình lặng lẽ mây bay <br/>
Một ngày vừa quen nhau nay trót đã thương yêu <br/>
Để chuốc lấy cô liêu, men đắng cay tình yêu <br/>
<br/>
Ta đã yêu rồi, người ấy cao sang <br/>
Còn ta kiếp phong trần mờ bóng tương lai <br/>
Giọng cười người vô tư chôn lấp những say mơ <br/>
Để tiếng hát bơ vơ, ta trách ai hững hờ <br/>
<br/>
Đêm nay đốt thư xanh mấy lần <br/>
Chuyện tình đơn phương lòng ta muốn ngó lại thôi <br/>
Chỉ muốn cho tình yêu còn mãi theo thời gian <br/>
Tình ấy thêm đẹp dù dở dang <br/>
Mong cho giấc mơ kia chẳng thành <br/>
<br/>
Một ngày bên nhau<br/>
Ngày vui hoa pháo rền vang <br/>
Lặng lẽ riêng mình ta <br/>
Ngày tháng êm đềm qua <br/>
Ai đó đâu hiểu được lòng ta <br/>
<br/>
Ai bước đi rồi <br/>
Người đứng trông theo <br/>
Tình yêu đó xa dần <br/>
Thuyền đã sang sông <br/>
Một người rồi mau quên ai nuối tiếc không khuây <br/>
Rượu chưa uống nhưng say câu hát ru lòng ai <br/>
<br/>
Thôi lỡ xa rồi tình vẫn đơn phương <br/>
Để cay đắng thêm dài, người đó ta đây, <br/>
Chuyện tình đầu tiên xin cho giữ mãi trong tim <br/>
Ngày tháng dẫu xa xăm <br/>
Đêm nhớ thương gọi thầm