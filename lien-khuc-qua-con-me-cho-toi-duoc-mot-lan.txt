Cho tôi được một lần nhìn hoa giăng đầu ngõ <br/>
Một lần cài hoa đỏ lên tim <br/>
Một lần dìu em sang nhà mới <br/>
Tình yêu trong tầm với <br/>
Ngọt tiếng nói thơm môi <br/>
<br/>
Cho tôi được một lần nhìn trăng soi trọn tối <br/>
Một lần nhìn mây ngủ quên trôi <br/>
Một lần trót thương yêu ngàn lối <br/>
Niềm tin xa về tới <br/>
Ngời sáng trên lưng đồi <br/>
<br/>
Xin cho tôi được một lần <br/>
Nhìn đàn chim trắng bay <br/>
Dập dìu qua đó đây <br/>
Ngày đó được nghe súng im hơi <br/>
Ðời không oán thôi hờn <br/>
Mến thương cùng kiếp người <br/>
<br/>
Cho tôi được một lần <br/>
Nhìn quê hương đợi sáng <br/>
Một lần nhân nghĩa sống lên ngôi <br/>
Người người cùng chung vui một lối <br/>
Ðời thôi không lừa dối <br/>
vì đã yêu thương rồi