Ngày ngày trôi nhanh ngóng tin người ơi tôi yêu từ lâu <br/>
Từng giờ đi qua héo hon thời gian dại khờ thương nhớ <br/>
Mong manh đời vẫn trôi như bao bọt sóng chìm vào đại đương mờ khuất <br/>
Từng lời yêu thương viết cho người ơi tôi mơ ngày đêm <br/>
Nhạt nhòa trang thư xót xa tình yêu đau buồn nhớ mong <br/>
Những trang thư về em ấp ủ <br/>
Ngày dài ta yêu em trong vạn nỗi sầu <br/>
Ngàn mong nhớ yêu thương giờ ta xin ghi vào đây <br/>
Dù ngàn năm có phai mờ đi những câu hẹn ước bên nhau <br/>
Em yêu ơi anh mãi mãi khắc ghi <br/>
Từng câu nói ta trao nhau ngày nào <br/>
Ngàn câu nói yêu thương giờ ta xin ghi vào đây <br/>
Dù thời gian có phai tàn theo bóng mây về núi xa xăm <br/>
Em yêu ơi anh luôn chờ mong em mãi <br/>
Dù ta xa nhau anh vẫn thư về em