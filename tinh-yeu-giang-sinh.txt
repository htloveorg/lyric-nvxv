(Saigon-1972)<br/>
<br/>
Xin tình yêu giáng sinh <br/>
Trên một quê hương cằn cỗi<br/>
Xin tình yêu giáng sinh <br/>
Trên địa cầu tăm tối<br/>
Xin tình yêu giáng sinh <br/>
Trong lòng người hấp hối<br/>
Xin tình yêu giáng sinh <br/>
Trên cuộc đời lầy lội.<br/>
Xin tình yêu giáng sinh<br/>
Trên quê hương ngục tối<br/>
Xin tình yêu giáng sinh<br/>
Trên địa cầu gian dối<br/>
Xin tình yêu giáng sinh<br/>
Trong lòng người tội lỗi <br/>
Xin tình yêu giáng sinh<br/>
Trên cuộc đời nổi trôi. <br/>
Mười ngàn đêm đau thương<br/>
Ôi trường thiên ác mộng <br/>
Mười ngàn đêm của hờn<br/>
Mười ngàn đêm của giận<br/>
Trên vũng lầy vô tận <br/>
Chỉ thấy máu và xương<br/>
Trên vũng lầy vô tận<br/>
Chỉ thấy khóc và than<br/>
Mười ngàn đêm đau thương <br/>
Mười ngàn đêm đoạn trường<br/>
Mười ngàn đêm oan khiên<br/>
Mười ngàn đêm đau thương.<br/>
ÐIỆP KHÚC<br/>
Xin tình yêu giáng sinh<br/>
Cho một lần hoa nở<br/>
Xin tình yêu giáng sinh<br/>
Cho một lần ngực thở<br/>
Xin tình yêu giáng sinh<br/>
Cho một lần cửa mở<br/>
Xin tình yêu giáng sinh <br/>
Tình yêu của chúng mình