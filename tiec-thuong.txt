Tình em trao anh bao đắm đuối <br/>
Khát khao nụ hôn đêm dài <br/>
Mà anh yêu đan tâm quay bước bỏ đi thật xa sao đành <br/>
Người yêu anh có biết những đêm trăng không đèn <br/>
Mình em nằm ôm gối hoen bờ mi <br/>
<br/>
Tại sao anh không cho em biết giấc mơ của anh lâu dài <br/>
Mà giờ đây sao anh bối rối đau thương vì sao anh buồn <br/>
Người yêu đừng oan trách nữa chi duyên đôi mình <br/>
Em, em muốn đi thật xa <br/>
Em cố quên tình ta <br/>
<br/>
Đừng để lòng oan trái <br/>
Cho con tim đau buồn <br/>
Đừng buồn phiền chi nữa cho đôi mình <br/>
Anh, anh ước mơ gì anh hỡi <br/>
Và xin anh đừng có phụ tình <br/>
<br/>
Vì cuộc sống lắm lúc dối gian nhiều <br/>
Vì anh ít có nhớ đến em nhiều <br/>
Người dấu yêu anh ơi <br/>
Tiếc thương, tiếc thương làm gì <br/>
<br/>
Vì cuộc sống lắm lúc dối gian nhiều <br/>
Vì anh ít có nhớ đến em nhiều <br/>
Người dấu yêu anh ơi, anh ơi <br/>
Tiếc thương, tiếc thương làm chi