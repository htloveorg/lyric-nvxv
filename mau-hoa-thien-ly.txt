Đời đời còn nhớ hoài <br/>
Đời đời còn nhớ <br/>
Bao cánh hoa nghèo <br/>
thứ như đem mộng mơ vào lòng <br/>
<br/>
Chiều xưa dưới giàn hoa này <br/>
Mẹ tôi thường hay lần đến <br/>
vuốt tóc tôi những khi tôi buồn <br/>
Chiều xưa dưới giàn hoa này <br/>
Người em thường hay tìm đến <br/>
nói với tôi những câu êm đềm <br/>
<br/>
Kỷ niệm nào êm êm <br/>
bằng những chiều bên giàn thiên lý <br/>
Một chiều biệt ly <br/>
phấn ghi mái nghèo ôm đàn ra đi <br/>
Đẫu rằng xa xôi quê nhà <br/>
Nhớ hoài không sao phai nhòa <br/>
Chiều trắng mùi nắng màu hoa <br/>
<br/>
Chiều nay tôi về chốn này <br/>
Bèn ra giàn hoa ngày ấy <br/>
Vắng bóng ai cánh hoa quên cười <br/>
Lòng tôi nhớ người tơi bòi <br/>
Ngẩn ngơ hỏi thăm đàn bướm <br/>
Xót thương tôi bướm không trả lơì <br/>
<br/>
Kỷ niệm nào êm êm <br/>
bằng những chiều bên giàn thiên lý <br/>
Một chiều biệt ly <br/>
phấn ghi mái nghèo ôm đàn ra đi <br/>
Đẫu rằng xa xôi quê nhà <br/>
Nhớ hoài không sao phai nhòa <br/>
Chiều trắng mùi nắng màu hoa <br/>
<br/>
Chiều nay tôi về chốn này <br/>
Bèn ra giàn hoa ngày ấy <br/>
Vắng bóng ai cánh hoa quên cười <br/>
Lòng tôi nhớ người tơi bòi <br/>
Ngẩn ngơ hỏi thăm đàn bướm <br/>
Xót thương tôi bướm không trả lời <br/>
Nước mắt rơi, nước mắt rơi <br/>
vì vắng màu hoa xưa