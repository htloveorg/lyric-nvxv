Anh biết chăng anh, em khổ vì ai, em khóc vì ai. <br/>
Ngày vui đã tan, nhân tình thế thái <br/>
chỉ còn lại đống tro tàn. <br/>
Em muốn kêu lên cho thấu tận trời cao xanh <br/>
Rằng tình yêu em sao giống đời đóa phù dung <br/>
Sớm nở tối tàn, xót thương vô vàn <br/>
chưa thắm vội lìa tan. <br/>
<br/>
<br/>
Thuở xưa ngày đầu của nhau, hai đứa vang câu tình ca. <br/>
Ngày đầu của nhau, anh đón đưa em về nhà <br/>
Trăng nước hiền hòa, ngày đầu của nhau <br/>
hương sắc tình yêu đậm đà. <br/>
Ngày nay mình đành bỏ nhau, canh vắng bơ vơ sầu đau. <br/>
Mình đành bỏ nhau, quên phút ta yêu lần đầu. <br/>
Trăng nước bạc màu, người đành bỏ người <br/>
như sương khói sau chuyến tàu. <br/>
<br/>
Em biết chăng em, anh ngủ nào yên anh thức nào yên. <br/>
Nhiều khi cố quên nhưng chỉ thêm chuốc vào lòng <br/>
những ưu phiền. <br/>
Âu yếm hôm qua không xóa được buồn hôm nay. <br/>
Người đời phụ nhau khi đã cạn chén tình say. <br/>
Để lại thương sầu, trót yêu nhau rồi <br/>
sao nỡ đành làm khổ nhau.