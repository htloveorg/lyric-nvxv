Thôi xin đừng thương hại dùm tôi <br/>
Không cần thương hại người ơi <br/>
Vì tôi chán phấn son cuộc đời <br/>
Thôi xin đừng đưa đón vui cười <br/>
Xin đừng đưa đã gạn mời <br/>
Vì tôi khinh chót lưỡi đầu môi. <br/>
<br/>
Khi ân tình đang thời nở hoa <br/>
Sao người ta vui thật là vui <br/>
Mình như thấy chữ yêu tuyệt vời <br/>
Nhưng khi người ta quên lời <br/>
Đem tình san sẻ đi rồi <br/>
Mình không hơn chi cánh bèo trôi. <br/>
<br/>
ĐK: <br/>
<br/>
Tình yêu ơi tình yêu ??? <br/>
Thuở xưa biết nếu duyên tình đầu <br/>
Gặp ngang trái xót xa nghẹn ngào <br/>
Bây giờ ta ôm trái sầu <br/>
Thà làm thân gổ cây tận rừng cao <br/>
Thà làm viên đá âm thầm đáy biển thật sâu. <br/>
<br/>
Thôi cam phận ôm tình lẻ loi <br/>
Không thèm ghen giận gì ai <br/>
Làm thân gái dĩ nhiên thiệt thòi <br/>
Xin Ông trời cho sống qua ngày <br/>
Tôi tự an ủi lâu rồi <br/>
Rằng chua cay cũng thế mà thôi...!!!