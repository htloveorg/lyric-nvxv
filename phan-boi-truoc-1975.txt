Có một loài hoa màu trắng bay trên tóc thầy bâng khuâng. <br/>
Tôi gọi tên là hoa phấn những ngày đi học xa xăm. <br/>
Hoa nhẹ mềm như hạt gió bay trên tóc thầy lung linh. <br/>
Ôi màu hoa ngày xưa ấy, vấn vương mãi trong lòng tôi <br/>
Chiều nay gặp loài hoa ấy bay trên bục giảng bồi hồi. <br/>
Còn đâu thầy tôi đứng đó những ngày hoa bướn xa xôi. <br/>
Có một loài hoa màu trắng cho tôi năm tháng cuộc đời. <br/>
Bồi hồi là màu hoa phấn một đời người ai nỡ quên