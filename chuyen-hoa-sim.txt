<i>(nhạc: Anh Bằng - thơ: Hữu Loan)</i><br/>
<br/>
<br/>
Rừng hoang đẹp nhất hoa màu tím <br/>
Chuyện tình thương nhất chuyện hoa sim <br/>
Có người con gái xuân vời vợi <br/>
Tóc còn ngăn ngắn chưa đầy búi <br/>
<br/>
Ngày xưa nàng vẫn yêu màu tím <br/>
Chiều chiều lên những đồi hoa sim <br/>
Đứng nhìn sim tím hoang biền biệt <br/>
Nhớ chồng chinh chiến miền xa xăm <br/>
<br/>
Ôi lấy chồng chiến binh <br/>
Lấy chồng thời chiến chinh, mấy người đi trở lại <br/>
Sợ khi mình đi mãi, sợ khi mình không về <br/>
Thì thương người vợ bé bỏng chiều quê <br/>
Nhưng không chết người trai khói lửa <br/>
Mà chết người em nhỏ hậu phương <br/>
Mà chết người em gái tôi thương <br/>
<br/>
Đời tôi là chiến binh rừng núi <br/>
Thường ngày qua những đồi hoa sim <br/>
Thấy cành sim chín thương vô bờ <br/>
Tiếc người em gái không còn nữa <br/>
<br/>
Tại sao nàng vẫn yêu màu tím <br/>
Màu buồn tan tác phải không em <br/>
Để chiều sim tím hoang biền biệt <br/>
Để mình tôi khóc chuyện hoa sim.