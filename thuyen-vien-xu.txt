<i>(nhạc: Phạm Duy - thơ: Huyền Chi)<br/>
<br/>
1952</i><br/>
<br/>
<br/>
Chiều nay sương khói lên khơi<br/>
Thùy dương rũ bến tơi bời<br/>
Làn mây hồng xa ráng trời<br/>
Bến Ðà Giang, thuyền qua xứ người <br/>
Thuyền ơi viễn xứ xa xôi<br/>
Một lần qua giạt bến lau thưa<br/>
Hò ơi giọng hát thiên thu<br/>
Suối nguồn xa vắng, chiều mưa ngân về<br/>
Nhìn về đường cố lý, cố lý xa xôi<br/>
Ðời nhịp sầu lỡ bước, bước hoang mang rồi<br/>
Quay lại hướng làng<br/>
Ðà Giang lệ ướt nồng<br/>
Mẹ già ngồi im bóng<br/>
Mái tóc sương mong con bạc lòng <br/>
Chiều nay gửi tới quê xưa<br/>
Biết là bao thương nhớ cho vừa<br/>
Trời cao chìm rơi xuống đời<br/>
Biết là bao sầu trên xứ người <br/>
Mịt mù sương khói lên hương<br/>
Lũ thùy dương rũ bóng ven sông<br/>
Chiều nay trên bến muôn phương<br/>
Có thuyền viễn xứ nhổ neo lên đường...