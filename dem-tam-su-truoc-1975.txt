Mình gặp nhau như lúc mới quen ban đầu cớ sao anh ngập ngùng <br/>
Nhà tôi đơn côi mời anh ở lại <br/>
Kể chuyện tha hương chưa lần phai nhớ thương. <br/>
<br/>
Mang tâm sự từ thuở thiếu đôi tay mềm biết nơi nao mà tìm <br/>
Nhiều khi ưu tư tựa song cửa nhỏ <br/>
Nhìn ngoài mưa tuôn sao nghe lạnh vào hồn. <br/>
<br/>
Mấy năm cách biệt chỉ vui đêm này <br/>
Chưa vơi tâm tình của hai chúng mình <br/>
Một lần trong đời anh nói thương tôi <br/>
Tiếng ngọt trên bờ môi<br/>
Này bạn đêm nay hỡi nếu mai đi rồi nhớ mang theo nụ cười <br/>
Còn tôi đêm mơ còn tôi đợi chờ <br/>
Thì dù xa xôi tôi vẫn là của người...<br/>
<br/>
Thời gian trôi nhanh quá <br/>
Nói chưa hết lời nắng mai lên cửa ngoài<br/>
Tàn đêm tâm tư <br/>
Tàn đêm hẹn hò<br/>
Và tàn một đêm cho tình yêu chúng ta<br/>
Tôi không buồn vì rằng tiễn nhau khi đời gió mưa đã nhiều rồi<br/>
Tình yêu riêng tôi, tình yêu của người<br/>
Nhường tình quê hương hai mươi tuổi gửi buồn<br/>
<br/>
Những đêm sương đổ<br/>
Đạn bay khói phủ<br/>
Những khi xua giặc bỏ quên giấc ngủ<br/>
Dù nhiều gian khổ câu nói thương ai vẫn ngọt trên đầu môi<br/>
<br/>
Này người đi xa hỡi<br/>
Trót thương nhau rồi, chỉ xin anh một điều<br/>
Tìm trong tương lai bàn tay diệt thù<br/>
Tìm về đêm xưa trong giấc ngủ đợi chờ