Tình ta giờ đây tựa như áng mây bay về cuối trời <br/>
Còn lại gì hỡi là nước mắt đắng ngắt trên bờ môi khô <br/>
Vẫn biết sẽ có lúc mình lìa xa <br/>
Vẫn biết sẽ vỡ nát cuộc tình ta <br/>
Người yêu ơi còn biết làm sao làm sao nguôi <br/>
<br/>
Một ngày nào đó rồi em cũng ra đi về với người <br/>
Còn lại mình anh chiều hoang vắng với trái tim đầyhéo khô <br/>
Vẫn nhớ mãi nhớ mãi lời hẹn ước <br/>
Vẫn sẽ vẫn nhớ những ngày cùng bước <br/>
Người yêu ơi rằng chốn đây anh luôn luôn chờ đợi <br/>
<br/>
Rồi ngày xa nhau em chôn giấu đi niềm đau <br/>
Biết ra sao ngày sao khi con tim khát khao <br/>
ta tìm nhau như khi đôi ta còn nhau <br/>
Anh sẽ đau <br/>
<br/>
Rồi mai sao em quên đi chuyện tình duyên <br/>
Em về đâu lanh hoanh ôi biết đâu bờ bến <br/>
Anh gọi em khi mưa bay bay trong màn đêm <br/>
Chắc khó quên <br/>
<br/>
Dù mai xa nhau anh sẽ không bao giờ quên <br/>
Em về đâu lênh đênh có biết đâu là bến <br/>
Anhcầu mong sao cho em luôn được bình yên <br/>
Ôi ưu phiền <br/>
<br/>
Quên anh đi nghe em rồi ngày tháng qua <br/>
Quên sao quên sao em chuyện tình chúng ta <br/>
Em hay chăng em ơi mùa đông rồi sẽ tàn <br/>
<br/>
Quên anh đi nghe em rồi ngày tháng qua <br/>
Quên sao quên sao em chuyện tình chúng ta <br/>
Em hay chăng ngày mai mùa xuân lại sẽ quay về