<i>(nhạc: Hữu Xuân - lời: Phạm Hà)</i><br/>
<br/>
Mùa thu đến rồi em <br/>
Cúc vàng khoe sắc thắm <br/>
Lá chao nghiêng trong nắng <br/>
Như là dáng em qua <br/>
<br/>
Mùa thu giữa lòng ta <br/>
Riêng em thì đi mãi <br/>
Để nỗi niềm khắc khoải <br/>
Lạnh vào trong tim anh <br/>
<br/>
Mối tình đầu màu xanh <br/>
Anh dành cho em đó <br/>
Trái tim giờ bỏ ngõ <br/>
Gõ vào đêm không em? <br/>
<br/>
Thu đến rồi lại đi <br/>
Riêng anh còn nhớ mãi <br/>
Tiếng em như hơi thở <br/>
Trăn trở cả đời anh