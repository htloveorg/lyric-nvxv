Gió chiều thầm vương bao nhớ nhung <br/>
Người yêu thoáng qua trong giấc mộng <br/>
Vui nguồn sống mơ <br/>
Những ngày mong chờ <br/>
Trách ai đành tâm hững hờ <br/>
<br/>
Mối tình đầu xuân ai thấu chăng <br/>
Lòng tha thiết buông theo tiếng đàn <br/>
Mơ đời ái ân những ngày mong chờ<br/>
Sống trong mộng đẹp ngày xuân <br/>
<br/>
Ngây thơ dáng huyền đến trong mơ <br/>
Lòng anh bớt sầu <br/>
Mộng vàng phút tan theo gió chiều <br/>
Biết em về đâu <br/>
<br/>
Hãy trả lời lòng anh mấy câu <br/>
Tình duyên với em trong kiếp nào <br/>
Xuân còn thắm tươi <br/>
Anh còn mong chờ <br/>
Ái ân kẻo tàn ngày mơ!