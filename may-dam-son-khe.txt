Anh đến thăm, áo anh mùi thuốc súng <br/>
Ngoài mưa khuya lê thê, qua ngàn chốn sơn khê <br/>
Non nước ơi, hồn thiêng của núi sông<br/>
Kết trong lòng thế hệ nghìn sau nối nghìn xưa <br/>
<br/>
Bao ước mơ giữa khung trời phiêu lãng, <br/>
Chờ mùa Xuân tươi sang, nhưng mùa thắm chưa sang <br/>
Anh đến đây, rồi anh như bóng mây, <br/>
Chốn phương trời ấm lạnh hòa chung mái nhà tranh <br/>
<br/>
Anh như ngàn gió, ham ngược xuôi, theo đường mây, <br/>
Tóc tơi bời lộng gió bốn phương, <br/>
Nước non còn đó một tấc lòng, <br/>
Không mờ xóa cùng năm tháng, <br/>
Nhớ ai ra đi hẹn về dệt nốt tơ duyên, <br/>
<br/>
Khoác lên vòng hoa trắng, <br/>
Cầm tay nhau đi anh <br/>
Tơ trời quá mong manh <br/>
Anh hỡi anh, đường xa vui đấu tranh, <br/>
Giữa khung trời gió lộng, nghìn sau tiếp nghìn xưa