Anh đi rồi, một mình em đứng bơ vơ <br/>
Cây thông gầy rủ buồn trăng gió xác xơ <br/>
Con chim nào gọi bầy cất tiếng thiết tha <br/>
Chim ơi muộn màng tình đã bay xa <br/>
Anh đi rồi, một mình em với cô đơn <br/>
Công viên buốn lạnh lùng tiếng dế nỉ non <br/>
Dế có còn lặng nghe khúc hát yêu thương <br/>
những buổi chiều về nồng ấm yêu đương <br/>
<br/>
Còn gì đâu tình như sương khói tan rồi <br/>
Lời hẹn hò chỉ như cơn gió thoảng thôi <br/>
Một mình em và nước khóc chia phôi <br/>
Yêu thương như con tàu lìa khỏi ra cuộc đời <br/>
<br/>
Anh đi rồi, đường dài em bước đơn côi <br/>
Chiếc lá vàng muộn màng lướt trên môi <br/>
Nhớ hôm nào, nụ hôn ngây ngất ta trao <br/>
Cho nhau tình nồng ngỡ đến mai sau <br/>
<br/>
Anh đi rồi, tình ta có lúc chia phôi <br/>
Như cơn mưa rào, như nắng qua thôi <br/>
Phút cuối cùng gởi người yêu dấu xa xôi <br/>
Xin anh mang theo hạnh phúc em một đời