Ngày xưa yêu em anh thường nói là tình yêu thứ nhất <br/>
Ngày xưa yêu em anh thường nói là tình yêu ban đầu <br/>
Gặp anh lên ngôi ngôi báu tôn thờ <br/>
Ngôi báu tôn thờ niềm tin vô bờ <br/>
Tình mong duyên chờ mình dệt bao ước mơ <br/>
<br/>
Ngày xưa yêu anh em nào nghĩ mình người yêu thứ mấy <br/>
Ngày xưa yêu anh em nào biết người phụ em sau này <br/>
Tình như cơn say như phút điên cuồng <br/>
Như phút điên cuồng <br/>
Tình yêu non dại lòng không e ngại <br/>
Ngày mình gặp đắng cay <br/>
<br/>
ĐK: <br/>
<br/>
Nhưng bây giờ còn lại lời thề gian dối <br/>
Những ân tình mặn nồng ngoài tầm tay với <br/>
Bước chân đi anh có buồn chi <br/>
Phút chia ly anh ngoảnh mặt đi <br/>
Đêm hôm nay em còn những gì ngoài tình buồn ướt mi <br/>
<br/>
Ngày mai yêu ai anh đừng nói là tình yêu thứ nhất <br/>
Ngày mai xa ai anh đừng nói là lần yêu sau cùng <br/>
Cầu cho dương gian không biết thay lòng <br/>
Không có con người thường hay thay lòng <br/>
Để những đêm dài chuyện tình thôi đắng cay