Đường em đi <br/>
Đường xa vắng buồn <br/>
Đôi mắt nhìn nhau <br/>
Không nói một câu <br/>
Chiều nay lá rơi <br/>
Buồn xa vời vợi <br/>
Sầu đến chơi vơi! <br/>
<br/>
Khi tay trong tay run rẩy <br/>
Xót xa vơi đầy <br/>
Lòng bỗng dâng sầu <br/>
Ngập trong gió mưa <br/>
Mùa thu tiễn đưa <br/>
Hồn mãi bơ vơ  <br/>
<br/>
Yêu anh, để tình ngang trái <br/>
Ray rứt mãi không thôi <br/>
Lệ sầu vương trên môi <br/>
Tâm hồn hoang vắng lạnh <br/>
Nhìn nhau trong giây lát <br/>
Rồi thẫn thờ tê tái <br/>
Ảo ảnh trôi trôi mau! <br/>
<br/>
Còn đâu nữa<br/>
Đường xa vắng buồn <br/>
Trong giấc mộng về <br/>
Chỉ có mình em! <br/>
Nhìn anh thoáng buồn <br/>
Lòng thấy đơn côi <br/>
Sầu mãi trên môi <br/>
<br/>
Yêu anh, yêu trong dang dở <br/>
Nhưng không ơ thờ! <br/>
Mà ai gieo sầu ? <br/>
Tình bao đắm say, <br/>
Lòng không đổi thay <br/>
Mà vẫn chua cay