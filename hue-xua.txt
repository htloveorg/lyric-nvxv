Tôi có người em Sông Hương Núi Ngự, <br/>
của lũy tre Thôn Vỹ hiền từ, <br/>
của kinh thành cổ xưa thật xưa... <br/>
<br/>
Buổi trưa em che nón lá, <br/>
Cá sông Hương liếc nhìn ngẩn ngơ <br/>
lũ chim quyên ngất ngây từ xa... <br/>
<br/>
Tôi sống độc thân trong căn phố nghèo <br/>
bởi trót thương nên nhớ thật nhiều <br/>
bởi em là hạnh phúc tình yêu... <br/>
<br/>
Ở bên ni qua bên nớ... <br/>
cách con sông chuyến đò chẳng xa <br/>
nhỏ sang thăm có tôi đợi chờ... <br/>
<br/>
Huế... ơi!... có biết bây chừ <br/>
tiếng ca nào vương bên mạn thuyền <br/>
có ai chờ ai qua Tràng Tiền... <br/>
không biết bây chừ... <br/>
nữ sinh mang nón bài thơ <br/>
để trai xứ Huế mộng mơ... <br/>
<br/>
Huế... ơi!... ta nhớ muôn đời <br/>
bóng trăng hồ sen trong Hoàng Thành <br/>
tiếng chuông từng đêm Thiên Mụ buồn <br/>
Ta nhớ muôn đời... <br/>
Người con gái Huế quá... xinh <br/>
Tóc mây ngang lưng trữ tình <br/>
<br/>
Non nước Thần Kinh quê hương đất lạnh <br/>
cả trái tim sông núi của mình <br/>
cả linh hồn của dân hùng anh.. <br/>
<br/>
Bởi đâu gây nên nông nỗi <br/>
cánh chim bay giữa trời lẻ loi <br/>
nhỏ tôi yêu khóc bao giờ nguôi??? <br/>
<br/>
Tôi đã lạc em trong cơn biến động <br/>
để tháng năm hai đứa lạnh lùng <br/>
để đêm ngày kẻ nhớ người mong... <br/>
Khổ đau cao như mây tím <br/>
cố năm xưa đã buồn buồn thêm <br/>
nhỏ yêu ơi... biết đâu mà tìm...