Vẫn biết trên cõi đời thường yêu thường mơ lứa đôi <br/>
Nếu biết sống giữa trời tình yêu là con nước trôi <br/>
Trôi lang thang qua từng miền <br/>
Lúc êm ái xuôi đồng bằng. <br/>
Cũng có lúc thác gập ghềnh chia từng con nước xuôi <br/>
Mời bạn nghe chuyện thê lương <br/>
Khóc cho người lỡ yêu đương <br/>
Trời già nhưng còn ghen tương <br/>
Cách chia người trót thương <br/>
<br/>
Em xinh em tên Mộng Thường Mẹ gọi em bé ngoan <br/>
Em xinh em tên Mộng Thường cha gọi em bé xinh <br/>
Đến lúc biết mơ mộng như những cô gái xuân nồng <br/>
Nàng yêu anh quân nhân Biệt Động trong một ngày cuối đông <br/>
Chuyện tình trong thời giao tranh vẫn như làn khói mong manh <br/>
Chàng về đơn vị xa xăm nàng nghe nặng nhớ mong <br/>
<br/>
Yêu nhau lúc triền miên khói lửa <br/>
Chuyện vui buồn ai biết ra sao <br/>
Nhìn quanh mình sao lắm thương đau <br/>
Khi không thấy người yêu trở lại <br/>
Tình không tìm ra dấu ban mai <br/>
Người không tìm ra dấu tương lai <br/>
Nhưng không chết người trai khói lửa <br/>
Mà chết người em nhỏ phương xa <br/>
Một đêm buồn có gió đông qua <br/>
<br/>
<br/>
Xin cho yêu trong Mộng Thường nhưng mộng thường cũng tan <br/>
Xin cho đi chung một đường sao định mệnh chắn ngang <br/>
Xin ghi tên chung thiệp hồng bỗng giây phút nghe ngỡ ngàng <br/>
Cô dâu chưa về nhà chồng <br/>
Ôi lạnh lùng nghĩa trang <br/>
Chàng thề không còn yêu ai dẫu cho ngày tháng phôi phai <br/>
Nhiều lần chàng mộng liêu trai <br/>
Chàng hẹn nàng kiếp mai...