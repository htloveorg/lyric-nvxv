1. <br/>
Trưa nay qua đường phố quen gặp những tiếng ve đầu tiên <br/>
Chợt nghe tâm hồn xao xuyến <br/>
Điệp khúc tiếng ve triền miên <br/>
Tiếng ve đu cành sấu <br/>
Tiếng ve náu cành me <br/>
Tiếng ve vẫy tuổi thơ <br/>
Tiếng ve chào mùa hè <br/>
Và gọi cơn gió mát <br/>
Những đêm đầy trăng thanh <br/>
Tiếng ve như lời hát đan giữa vòn cây xanh <br/>
<br/>
2. <br/>
Nơi đây con đường vẫn qua <br/>
Chợt thoáng tiếng ve gần xa <br/>
Giọng chim im lìm trưa vắng <br/>
Lại ngỡ tiếng ve gọi ta <br/>
Tiếng ve trên đường vắng <br/>
Hát theo bước hành quân <br/>
Mãi xa vẫn còn ngân <br/>
Tiễn tôi ra mặt trận <br/>
Đường hành quân gấp gấp <br/>
Tiếng ve chào say sưa <br/>
Thấy thêm yêu thành phố trong sáng tuổi ngây thơ