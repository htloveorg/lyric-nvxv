Khi đất nước tôi thanh bình, tôi sẽ đi thăm, <br/>
tôi sẽ đi thăm, một phố đầy hầm, <br/>
đi thăm một con đường nhiều hố <br/>
Khi đất nước tôi không còn chiến tranh <br/>
bạn bè mấy đứa vừa xanh nấm mồ <br/>
<br/>
Khi đất nước tôi thanh bình, tôi sẽ đi thăm, <br/>
tôi sẽ đi thăm, cầu gẫy vì mìn, <br/>
đi thăm hầm chông và mã tấu. <br/>
Khi đất nước tôi không còn giết nhau <br/>
Trẻ con đi hát đồng dao ngoài đường. <br/>
<br/>
Khi đất nước tôi thanh bình, tôi sẽ đi không ngừng <br/>
Sài gòn ra Trung, Hà Nội vô Nam, <br/>
tôi đi chung cuộc mừng <br/>
và mong sẽ quên chuyện non nước mình. <br/>
<br/>
Khi đất nước tôi thanh bình, tôi sẽ đi thăm, <br/>
tôi sẽ đi thăm, <br/>
đi thăm nhiều nghĩa địa buồn <br/>
đi xem mộ bia đều như nấm <br/>
Khi đất nước tôi không còn chiến tranh <br/>
Mẹ già lên núi tìm xương con mình <br/>
<br/>
Khi đất nước tôi thanh bình, tôi sẽ đi thăm, <br/>
tôi sẽ đi thăm, làng xóm thành đồng <br/>
đi thăm từng khu rừng cháy nám <br/>
Khi đất nước tôi không còn giết nhau <br/>
Mọi người ra phố mời rao nụ cười <br/>
<br/>
Khi đất nước tôi thanh bình, tôi sẽ đi không ngừng <br/>
Sài gòn ra Trung, Hà Nội vô Nam, <br/>
tôi đi chung cuộc mừng <br/>
và mong sẽ quên chuyện non nước mình.