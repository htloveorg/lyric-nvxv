Bài thánh ca đó còn nhớ không em <br/>
Noel năm nào chúng mình có nhau <br/>
Long lanh sao trời thêm đẹp môi mắt <br/>
Áo trắng em bay như cánh thiên thần <br/>
Giọt môi hôn dưới tháp chuông ngân <br/>
<br/>
Cùng nhau quỳ dưới chân Chúa cao sang <br/>
Xin cho đôi mình suốt đời có nhau <br/>
Vang trong đêm lạnh bài ca Thiên Chúa <br/>
Khẽ hát theo câu đêm thánh vô cùng <br/>
Ôi giọng hát em mênh mông buồn... <br/>
<br/>
Rồi mùa giá buốt cũng qua mau <br/>
Lời hẹn đầu ai nhớ dài lâu <br/>
Rồi một chiều áo trắng thay màu <br/>
Em qua cầu xác pháo bay sau <br/>
<br/>
Lời nguyện mình Chúa có nghe không <br/>
Sao bây giờ mình hoài xa vắng <br/>
Bao nhiêu đêm Chúa xuống dương gian <br/>
Bấy nhiêu lần anh nhớ người yêu <br/>
<br/>
Rồi những đêm thánh đường đón Noel <br/>
Lang thang qua miền giáo đường dấu yêu <br/>
Tiếng thánh ca ngày xưa vang đêm tối <br/>
Nhớ quá đi thôi giọng hát ai buồn <br/>
Đêm thánh vô cùng lạnh giá hồn tôi