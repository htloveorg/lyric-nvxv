Không biết tại sao tôi lại yêu <br/>
Lòng tôi tê tái hận cô liêu <br/>
Tôi yêu màu sắc hương huyền bí <br/>
Tôi yêu, yêu bóng hoa xoan tẩm nắng chiều. <br/>
Không biết tại sao tôi lại mơ <br/>
Đời tôi tha thiết mãi tiếng tơ <br/>
Tôi mơ câu hát vương mộng thắm <br/>
Tôi mơ, mơ bóng giai nhân với bóng mờ. <br/>
<br/>
Không biết tại sao tôi lại sao tôi lại thương <br/>
Hồn tôi ngây ngất suốt đêm trường. <br/>
Tôi thương điệu nhạc mơ ngày tháng, <br/>
Thương kiếp phong sương chốn biên cương <br/>
<br/>
Tôi biết hồn tôi vương niềm thương <br/>
Vì tin yêu đến khắp phố phường, <br/>
Vì cung đàn sống gieo ngàn hướng <br/>
Khắc ghi lòng tôi nguồn mến thương