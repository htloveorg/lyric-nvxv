Nhịp 2/4 Điệu Tango Hợp âm La thứ <br/>
<br/>
(sáng tác khoảng cuối thập niên 60) <br/>
<br/>
<br/>
<br/>
Em ơi . . . nếu đừng dang dở <br/>
Nếu đừng dang dở <br/>
Thì tình ta như bài thơ <br/>
Đẹp như giấc mơ <br/>
Em ơi . . . lệ ướt hoen mi <br/>
Còn ước mong chi <br/>
Kiếp sau chờ nhau em nhé <br/>
Thôi sầu biệt ly <br/>
<br/>
Ước cũ . . . thề xưa . . . nhắc bao niềm nhớ <br/>
Nhiều khi trong đêm mơ hồn dật dờ <br/>
Mộng thấy bóng hình ai . . . xa mờ <br/>
Em ơi . . . chốn nao . . . biết em về đâu ? <br/>
Xót xa cho lòng nhau thư nhạt màu <br/>
Nát tan duyên tình đầu <br/>
Đành phụ lòng nhau <br/>
<br/>
Đêm nay . . . tiếng lòng nức nở <br/>
Gió lùa gác nhỏ <br/>
Từng giọt mưa rơi buồn tênh <br/>
Hồn em vắng lạnh <br/>
Mênh mang . . . vẳng tiếng chuông ngân <br/>
Vọng đến bâng khuâng <br/>
Sắt se buồn trong đêm tối <br/>
Dâng ngập hồn tôi.