Tôi xin người cứ gian dối <br/>
Cho tôi tưởng người cũng yêu tôi <br/>
Cho tôi còn được thấy đời vui <br/>
Khi cơn mưa mùa đông đang đến <br/>
Xin giã từ ngày tháng rong chơi <br/>
<br/>
Đôi tay này vẫn chờ mong <br/>
Con tim này dù lắm long đong <br/>
Tôi yêu người bằng nỗi nghiệt oan <br/>
Không than van và không trách oán <br/>
Cho tôi trọn một kiếp đam mê <br/>
<br/>
Ôi tôi ước mơ anh bỏ cuộc vui <br/>
Trở về căn phòng này đơn côi <br/>
Môi anh ru nỗi đau tuyệt vời <br/>
Khi màn đêm phủ lứa đôi <br/>
Là thời gian cũng như ngừng trôi <br/>
Thương yêu này người hãy nhận lấy <br/>
ôm tôi đi môi hôn tràn đầy <br/>
Trong tay người hồn sẽ mù say bao khốn khó vụt bay <br/>
<br/>
Tôi không cần và nghi ngại khi <br/>
Ai chê bai thân tôi khờ dại <br/>
Tôi yêu người hồn chẳng tình trong <br/>
Tôi vẫn cứ đợi mong <br/>
<br/>
Tôi xin người cứ gian dối <br/>
Cho tôi tưởng người cũng yêu tôi <br/>
Cho tôi còn được thấy đời vui <br/>
Khi cơn mưa mùa đông đang đến <br/>
Tôi xin người cứ gian dối <br/>
Nhưng xin người đừng lìa xa tôi