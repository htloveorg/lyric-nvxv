Một ngày vui dường như về đây <br/>
Mà chợt nghe hàng cây nở hoa <br/>
Bên ngoài sân bầy chim tìm về <br/>
Lũ trẻ nhỏ đang hát ca. <br/>
Và gió quấn quanh bên em hát chào, <br/>
Làm em nhớ tuổi xanh hôm nào <br/>
<br/>
Ðời thật vui và cũng buồn tênh <br/>
Từ khi trao nụ hôn đầu tiên <br/>
Em làm quen niềm đau làm người <br/>
Biết yêu đời biết chán chê <br/>
Ngày tháng đã mang trong em nỗi buồn <br/>
Vì mang lỡ một con tim yêu thương <br/>
<br/>
Hãy khóc lên đi hỡi em <br/>
Cất cao tiếng ca với đời <br/>
Hãy cùng tôi vượt qua những ưu phiền <br/>
Ðể tim hồng còn mang nhiều say đắm <br/>
Với ngày xanh bao thiết tha <br/>
<br/>
Hãy khóc lên đi hỡi em <br/>
Cất cao tiếng ca với đời <br/>
Cho Tình yêu của em sẽ thêm nhiều <br/>
Những nụ hồng còn xanh, còn xanh mãi <br/>
Trong biển đời hay đổi thay <br/>
<br/>
Em hãy cười trong phù du cuộc đời <br/>
Lắm đắng cay và tuyệt vời