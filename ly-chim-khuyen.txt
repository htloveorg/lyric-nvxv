Chim khuyên quen trái nhãn lồng<br/>
Thia lia quen chậu,<br/>
Vợ chồng quen (hay quyện) hơi. <br/>
<br/>
Chim khuyên nút mật bông qùi <br/>
Ba năm đợi được huống gì một năm. <br/>
<br/>
Chim khuyên xuống núi ăn mồi<br/>
Thấy anh lao khổ đứng ngồi không yên.