Nghe tiếng đàn ai rao sáu câu <br/>
Như sống lại hồn Cao Văn Lầu <br/>
Về Bạc Liêu danh tiếng ôn lại giấc ngủ vàng son<br/>
Một thời để nhớ ngày đó xa rồi. <br/>
<br/>
Bên nước mặn biển cho muối nhiều <br/>
Bên nước ngọt phù sa vun bồi <br/>
Bạc Liêu đưa ta tới cánh đồng lúa trải ngàn khơi<br/>
Cò bay thẳng cánh nhìn mỏi mắt người <br/>
<br/>
Bạc Liêu giấc mơ tình yêu <br/>
Dân gian ca rằng: "Bạc Liêu là xứ cơ cầu <br/>
Dưới sông cá chốt, trên bờ Triều Châu <br/>
Nghe danh Công Tử Bạc Liêu <br/>
Ðốt tiền nấu trứng tỏ ra mình giàu!" <br/>
<br/>
Cho nhắn gởi Bạc Liêu mấy lời <br/>
Sông có cạn tình không đổi dời <br/>
Dù đi xa trăm hướng <br/>
Ai người thấu nỗi hoài hương <br/>
Bạc Liêu thương quá hình bóng quê nhà