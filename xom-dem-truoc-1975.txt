Đường về canh thâu. <br/>
Đêm khuya ngõ sâu như không mầu, <br/>
qua phên vênh có bao mái đầu <br/>
hắt hiu vàng ánh điện câu. <br/>
<br/>
Đường dài không bóng. <br/>
Xa nghe tiếng ai ru mơ màng. <br/>
Mưa rơi rơi xóa lối đi mòn. <br/>
Có đôi lòng vững chờ mong. <br/>
<br/>
Ai chia tay ai đầu xóm vắng im lìm. <br/>
Ai rung lên tia mắt ngàn câu êm đềm. <br/>
Mong sao cho duyên nghèo mai nắng gieo thêm. <br/>
Đẹp kiếp sống thêm. <br/>
<br/>
Màn đêm tịch liêu. <br/>
Nghe ai thoáng ru câu mến trìu. <br/>
Nghe không gian tiếng yêu thương nhiềụ <br/>
Hứa cho đời thôi đìu hiu. <br/>
<br/>
Đêm tha hương ai vọng trông. <br/>
Đêm cô liêu chinh phụ mong. <br/>
Đêm bao canh mưa âm thầm, <br/>
Theo gió về khua cơn mộng, <br/>
hẹn mai ánh xuân nồng. <br/>
<br/>
Cho nên đêm còn dậy hương, <br/>
để dìu bước chân ai trên đường, <br/>
để nhìn xóm khuya không buồn <br/>
vì người biết mang tình thương