Về đây bên nhau, ta nối lại tình xưa <br/>
Chuyện tình mà bao năm qua, <br/>
em gói ghém thành kỷ niệm <br/>
Vai nắng con đường xưa, <br/>
những chiều hẹn cơn mưa đổ <br/>
Mưa ướt lạnh vai em, <br/>
anh thấy lòng mình xót xa <br/>
<br/>
Mùa thu năm nao, anh với em gặp nhau <br/>
Tưởng rằng mình quen nhau thôi <br/>
Khi đã biết thì yêu rồi <br/>
Nuôi trái tim chờ nhau, <br/>
hứa hẹn mùa đông muôn thuở <br/>
Sương gió lạnh môi em, <br/>
anh thấy lòng mình giá băng <br/>
<br/>
Nhưng không ngờ, định mệnh chia rẽ xa nhau, <br/>
cuộc đời đôi ta hai lối, em rét mướt giữa trời đơn côi <br/>
Còn anh chơi vơi, ngày tháng vơ vơ nơi miền xa vời vợi <br/>
Chuyện dĩ vãng buồn lưu luyến chưa hề nguôi <br/>
Đêm đông dài, sợ rằng nhung nhớ phôi pha <br/>
Tình vào thiên thu mãi mãi <br/>
Em sẽ khóc suốt đời anh ơi <br/>
Thì em ơi em vì nghĩa tương lai ta về xây mộng lại <br/>
chuyện cũ sẽ vào dĩ vãng sẽ nhạt phai <br/>
<br/>
Rồi sau cơn mưa, giông tố sẽ vượt qua <br/>
Trời đẹp và xanh bao la <br/>
Soi sáng lối đường anh về <br/>
Chim én mang mùa xuân, xóa mờ niềm đau năm cũ <br/>
Anh sẽ về bên em <br/>
Ta ấm lại tình cố nhân <br/>
<br/>
Đêm đông dài, sợ rằng nhung nhớ phôi pha <br/>
Tình vào thiên thu mãi mãi <br/>
Em sẽ khóc suốt đời anh ơi <br/>
Thì em ơi em vì nghiã tương lai ta về xây mộng lại <br/>
chuyện cũ sẽ vào dĩ vãng sẽ nhạt phai <br/>
<br/>
Một đêm trăng sao, tha thiết thương người xưa <br/>
Mình dệt mùa thương năm qua ân ái cũ chẳng phai nhoà <br/>
Ta nối lại tình xưa, sống trọn mùa đông muôn thuở <br/>
Chim én về xôn xao, ta thấy đời còn có nhau <br/>
Anh sẽ về bên em, ta ấm lại tình cố nhân