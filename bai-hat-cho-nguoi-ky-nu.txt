Ta tiếc cho em trong cuộc đời làm người<br/>
Ta xót xa thay em là một cánh hoa rơi<br/>
Người đời vô tình giẫm nát thân em<br/>
Người đời vô tình giày xéo lên em<br/>
Người đời vô tình giết chết đời em<br/>
<br/>
Em hỡi em ơi trong đèn màu mờ mờ<br/>
Sau những cơn say thấy hồn mình có bơ vơ<br/>
Và từng đêm về lòng có cô đơn<br/>
Và từng đêm về hồn có đau thương<br/>
Và từng đêm về lệ có đầy vơi<br/>
<br/>
Ta tiếc cho em, ta tiếc cho em<br/>
Nhìn em trong lòng mọi người<br/>
Quay cuồng theo tiếng nhạc đưa<br/>
Nhìn em tay nâng ly rượu<br/>
Môi cười mà lệ như rơi<br/>
Môi cười mà lệ như rơi<br/>
<br/>
Trong xót xa đưa theo từng ngày lạnh lùng<br/>
Em hỡi em ơi mai này đời sẽ ra sao<br/>
Từng mùa xuân buồn lòng vẫn đơn côi<br/>
Từng mùa đông lạnh tình vẫn xa xôi<br/>
Lòng người vô tình em sẽ về đâu