Đừng tuyệt vọng, tôi ơi đừng tuyệt vọng <br/>
Lá mùa thu rơi rụng giữa mùa đông <br/>
Đừng tuyệt vọng, em ơi đừng tuyệt vọng <br/>
Em là tôi và tôi cũng là em. <br/>
Con diều bay mà linh hồn lạnh lẽo <br/>
Con diều rơi cho vực thẳm buồn thêm <br/>
Tôi là ai mà còn ghi dấu lệ <br/>
Tôi là ai mà còn trần gian thế <br/>
Tôi là ai, là ai, là ai? <br/>
Mà yêu quá đời này. <br/>
<br/>
Đừng tuyệt vọng, tôi ơi đừng tuyệt vọng <br/>
Nắng vàng phai như một nỗi đời riêng <br/>
Đừng tuyệt vọng, em ơi đừng tuyệt vọng <br/>
Em hồn nhiên rồi em sẽ bình minh <br/>
Có đường xa và nắng chiều quạnh quẽ <br/>
Có hồn ai đang nhè nhẹ sầu đêm.