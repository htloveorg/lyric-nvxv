Rồi chiều nào anh qua xóm vắng <br/>
Đã mấy mùa trăng anh cách xa <br/>
Mấy lần nghe tim thiết tha <br/>
Cớ sao anh tưởng là hôm qua <br/>
Đôi ta hẹn nhau giữa lòng mây rặng tre già <br/>
Mặn mà làm sao <br/>
Nhìn trăng lên xa xa <br/>
Hai đứa thì thầm chuyện cũ qua <br/>
Ước ngày mai nở hoa <br/>
Mười năm vai mang phong sương, <br/>
bước đến lối thương mộng biết sầu vương <br/>
<br/>
Rồi từ mùa trăng xa xôi <br/>
Đã nhủ lòng thôi nhớ thương <br/>
Cả cuộc đời cho gió sương <br/>
Nhớ hôm em dàu dàu nhìn anh đi <br/>
Long lanh bờ mi <br/>
Ngắt cành hoa ngay bên đường <br/>
Anh hỏi buồn không <br/>
Mà sao em không nói <br/>
Cho thấy cuộc đời thêm ấm vui <br/>
Thấy nụ cười trên khoé môi <br/>
Chiều nay giữa lúc nhớ mong <br/>
Nương theo gió qua <br/>
Tiếng sáo buồn đưa <br/>
<br/>
Ngày ấy người em gái thuở xa xưa <br/>
Chờ khi đầu xuân <br/>
Đến lúc tàn đông lạnh lẽo <br/>
Mà người đi mãi phương trời nào lãng quên <br/>
Hồn còn tìm thấy bóng dáng xa xưa <br/>
Thời gian mấy chốc đi qua <br/>
Người anh còn vui kiếp sông hồ nơi trời xa <br/>
Thì người em gái nơi quê nhà chờ mấy đông <br/>
Tình qua bên sông <br/>
<br/>
Chiều nay tìm về thăm xóm ấy <br/>
Nghe kể rằng một ngày cuối đông <br/>
Pháo hồng nhuộm trên bến sông <br/>
Có cô em nho nhỏ đẹp thương mong <br/>
Bước xuống thuyền hoa <br/>
Kết bằng màu sắc hồng <br/>
Ôi buồn làm sao <br/>
Chiều nay bên xóm vắng <br/>
Không tiếng thì thầm chuyện cũ qua <br/>
Ước ngày mai đời nở hoa <br/>
Chỉ biết vương trên bến sông <br/>
Tiếng sáo nhớ mong <br/>
Nghe sáo lạnh lòng.