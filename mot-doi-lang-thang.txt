<i>(nhạc: Anh Bằng - thơ: Hồ Dzếnh)</i><br/>
<br/>
<br/>
Anh cứ hẹn nhưng anh đừng đến nhé <br/>
Để một mình em dạo phố lang thang <br/>
Quán vắng quanh đây nụ hôn quá nồng nàn <br/>
Em bước vội để che hồn trống vắng <br/>
<br/>
Anh cứ hẹn nhưng anh đừng đến nhé <br/>
Để chuyện tình em đợi đến si mê <br/>
Những lúc xa nhau là tiếng sóng gần kề <br/>
Không dỗi hờn xót xa làm ướt mi <br/>
<br/>
Tình yêu chỉ đẹp phút hẹn thề <br/>
Tình yêu sẽ buồn khi bước vào vòng đam mê <br/>
Tình như trái mộng chín rung rinh trên đầu cành <br/>
Tình như nắng lụa hóa mộng mơ <br/>
<br/>
Anh cứ hẹn nhưng anh đừng đến nhé <br/>
Tình chỉ đẹp khi còn dở dang thôi <br/>
Những cánh thư yêu đừng nên kết vội vàng <br/>
Những cánh buồm đừng nên dừng bến đỗ <br/>
<br/>
Anh cứ hẹn nhưng anh đừng đến nhé <br/>
Cuộc đời buồn khi tình đã lên ngôi <br/>
Có biết bao nhiêu tình say đắm tuyệt vời <br/>
Đều dở dang như tình mình thế thôi.