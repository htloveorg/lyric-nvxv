Chiều nay em ra phố về <br/>
Thấy đời mình là những chuyến xe <br/>
Còn đây âm vang não nề <br/>
Ngày đi đêm tới trăm tiếng mơ hồ. <br/>
<br/>
Chiều nay em ra phố về <br/>
Thấy đời mình là những đám đông <br/>
Người chia tay nhau cuối đường <br/>
Ngày đi đêm tới trăm tiếng hư không. <br/>
<br/>
Có ai đang về giữa đêm khuya, <br/>
rượu tàn phai dưới chân đi ơ hờ <br/>
Vòng tay quen hơi băng giá, <br/>
Nhớ một người tình nào cũ, <br/>
Khóc lại một đời người quá ê chề. <br/>
<br/>
Chiều nay em ra phố về <br/>
Thấy đời mình là những quán không <br/>
Bàn in hơi bên ghế ngồi <br/>
Ngày đi đêm tới đã vắng bóng người. <br/>
<br/>
Chiều nay em ra phố về <br/>
Thấy đời mình là con nước trôi, <br/>
Đèn soi trên vai rã rời <br/>
Ngày đi đêm tới còn chút hao gầy.