Anh hẹn em cuối tuần, chờ nhau nơi cuối phố. <br/>
Biết anh thích màu trời, em đã bồi hồi chọn màu áo xanh. <br/>
Chiều thứ bảy người đi, sao bóng anh chẳng thấy. <br/>
Rồi nhẹ đôi gót hài, chiều nghiêng bóng dài, áo em dần phai. <br/>
<br/>
Sáng chủ nhật trời trong, nhưng trong lòng dâng sóng <br/>
Chẳng thấy bóng anh sang. <br/>
Nên thứ hai thu tàn, nên thứ ba thu vàng <br/>
Mùa đông thứ tư sang. <br/>
<br/>
Qua thứ năm nhẹn ngào, giận anh đêm thứ sáu <br/>
Quyết, em quyết dặn lòng không nói nửa lời, dù là ghét anh. <br/>
Chiều thứ bảy mưa rơi, ai bảo anh lại tới <br/>
Ai bảo anh xin lỗi, ai bảo anh nhiều lời, <br/>
Cho mắt em lệ rơi.