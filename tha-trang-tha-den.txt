Chúng mình không còn yêu nhau thì thôi <br/>
Anh nói ra đi chẳng đem một lời <br/>
Có gì mà ngại anh ơi <br/>
Có gì mà đành gian dối <br/>
Em không hề kết tội anh đâu <br/>
<br/>
Duyên tình không trọn gieo neo làm chi <br/>
Em cũng như anh sướng vui được gì <br/>
Chẳng thà đường tình đôi nơi <br/>
Em về trọn tình duyên mới <br/>
Còn anh đi cưới vợ là xong <br/>
<br/>
Duyên kiếp đôi ta từ đây thôi bẽ bàng <br/>
Kỷ niệm chôn kín trong tim <br/>
Đời sẽ đổi thay và thời gian xóa mờ <br/>
Bận lòng chi mối duyên hờ <br/>
<br/>
Duyên tình không trọn xa nhau là hơn <br/>
Gian díu chi thêm đớn đau tủi hờn <br/>
Có gì mà gại anh ơi <br/>
Chẳng còn gì mà suy tính <br/>
Tình không thương xin đừng lụy vương