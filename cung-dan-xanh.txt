Ghi-ta, anh đàn em hát <br/>
Thiết tha, thiết tha là khúc tình ca <br/>
Xôn xao xô bờ con sóng <br/>
Đắm say, đắm say biển hát bao ngày <br/>
Ghi-ta, ưu phiền năm tháng <br/>
Đã qua đã qua, thuở ấy tình xa <br/>
Miên man hoa biển hương cát <br/>
Có nhau, có nhau một thoáng ban đầu <br/>
Âm vang tiếng ghi-ta như tình anh, tháng năm vụt nhanh <br/>
Lang thang sóng xa khơi như tình em, mắt nâu biển xanh <br/>
Ngày nào thổn thức mưa trắng, giờ đây ngây ngất hương nắng <br/>
Ôi cung đàn khát vọng <br/>
Em ơi, tiếng ghi-ta như lòng anh với em ngày mưa <br/>
Em ơi, sóng xa khơi như lòng anh với em chiều xưa <br/>
Ngàn đời biển vẫn xanh ngát, tình mình in dấu trên cát <br/>
Ôi cung đàn biển xanh.