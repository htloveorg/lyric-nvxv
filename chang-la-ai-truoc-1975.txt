Này chàng, từ hậu phương hay biên cương <br/>
Nơi đây, chàng đến, áo vương bụi đường <br/>
Nhìn chàng là người trai, ôi, phong sương <br/>
Tim tôi đã mến biết bao lâu rồi <br/>
<br/>
Chàng từ miền ngược xuôi hay nơi đâu <br/>
Nơi đây, dừng bước kết duyên ban đầu <br/>
Một lời, đẹp tình xin trao cho nhau <br/>
Tuy tôi chưa biết hỡi chàng là ai <br/>
<br/>
Dẫu biết cho rằng chàng là người say chiến đấu <br/>
Dẫu biết cho rằng chàng là người yêu phiêu lưu <br/>
Dẫu biết cho rằng chàng dệt đời ngàn cánh hoa <br/>
Dẫu biết cho rằng chàng là người của nắng mưa <br/>
<br/>
Đ.K<br/>
<br/>
Tôi vẫn yêu chàng vì biển đời trong khóe mắt <br/>
Tôi vẫn yêu chàng vì trời tình in trên môi <br/>
Tôi vẫn yêu chàng vì đời chàng là gió sương <br/>
Tôi vẫn yêu chàng vì đời chàng là hiên ngang <br/>
<br/>
Này chàng, hãy cùng tôi vui đêm nay <br/>
Mai đây dù có cách xa chân trời <br/>
Một lời mặn mà tin trao cho ngay <br/>
Tuy tôi chưa biết hỡi chàng là ai...