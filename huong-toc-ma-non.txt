Nghe em hát câu dân ca sao mượt mà lòng anh thương quá. <br/>
Tiếng ngọt ngào nào đong đưa nhớ xa xưa trời trưa bóng dừa <br/>
Hẹn hò nhau tình quê hai đứa <br/>
Mùi mạ non hương tóc em biết bao kỷ niệm <br/>
Nhắc lại thấy thương nghe thật buồn <br/>
<br/>
Lâu nay muốn qua thăm anh nhưng ngại vì cầu tre lắc lẻo <br/>
Tháng ngày tuổi đời trôi theo níu chân nhau, bạc thêm mái đầu <br/>
Còn tìm đâu ngày xưa yêu dấu <br/>
Ðường về hai thôn cách xa, thoáng cơn gió chiều <br/>
Nhớ mùi tóc em hương đậm đà. <br/>
<br/>
<br/>
Lòng chợt buồn mênh mông <br/>
Dáng xưa tan theo giấc mộng <br/>
Chắc người đã bước sang sông <br/>
Ðang mùa lúa trổ đòng đòng <br/>
Làm sao em quên, những ngày khi mới quen tên <br/>
Bên gốc đa ven đường hai đứa ngồi tỏ tình yêu thương <br/>
<br/>
Anh thương tóc em bay bay trong chiều chiều gợi bao nỗi nhớ <br/>
Nhớ từng nụ cười ngây thơ thắm duyên mơ <br/>
chiều nghiêng nắng đổ <br/>
Về quê em phù sa bát ngát <br/>
Tình mình dù ngăn cách sông chứ đâu cách lòng <br/>
Mỗi lần nhớ em sao nghẹn lòng