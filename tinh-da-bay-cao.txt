Ai ra xứ Huế thì ra <br/>
Ai về là về núi Ngự <br/>
Ai về là về sông Hương <br/>
Nước sông Hương còn thương chưa cạn <br/>
Chim núi Ngự tìm bạn bay về <br/>
Người tình quê ơi người tình quê thương nhớ lắm chi <br/>
<br/>
Ai ra xứ Huế thì ra <br/>
Ai về là về Vỹ Dạ <br/>
Ai về là về Nam Dao <br/>
<br/>
Dốc Nam Dao còn cao mong đợi <br/>
Trăng Vỹ Dạ còn gợi câu thề <br/>
Người tình quê ơi người tình quê có nhớ xin trở về <br/>
<br/>
Hò ơi à ơi ! <br/>
Cầu Tràng Tiền sáu vai mười hai nhịp <br/>
Thương nhau rồi chớ xin kíp về mau <br/>
À ơi ơi à! Hò ơi! <br/>
Kẻo mai tê bóng xế qua cầu <br/>
Bạn còn thương bạn chứ biết gởi sầu về nơi mô <br/>
À ơi ơi à! <br/>
<br/>
Ai ra xứ Huế thì ra <br/>
Ai về là về bến Ngự <br/>
Ai về là về Vân Lâu <br/>
<br/>
Bến Vân Lâu còn sâu thương nhớ <br/>
Thuyền Bến Ngự còn đợi khách về <br/>
Người tình quê ơi người tình quê, có nhớ xin trở về <br/>
Người tình quê ơi người tình quê, có nhớ xin trở về...