Giá buốt hoang lạnh <br/>
Tuyết rơi thêm bao hiu quạnh <br/>
Trắng xóa đêm dài ngồi nhớ ai <br/>
Nhớ tới đêm nào <br/>
Thiết tha chung ly rượu đào <br/>
đắm đuối men nồng tình ngất ngây <br/>
Ngày dài với bao mong chờ <br/>
Ai hay niềm nhớ <br/>
Quyện hồn chết trong men sầu <br/>
Nghe nhói tim đau <br/>
Giờ người đã xa lìa ta mãi <br/>
Ngồi mình trách sao tình oan trái <br/>
Còn đâu nữa hương nồng đã hết <br/>
Còn đâu nữa tình yêu đã chết <br/>
Người yêu hỡi sao đành quay gót <br/>
Ðể ta khóc riêng mình đau xót <br/>
Tình yêu kia sẽ là mãi mãi trao về ai <br/>
Thức suốt đêm dài <br/>
Nhớ thương ai thêm u hoài <br/>
Giá buốt tâm hồn niềm đớn đau <br/>
Nắn nót giọt sầu <br/>
Trách ai ôi duyên tình đầu <br/>
Tiếc nuối chi nhiều vì đã yêu.