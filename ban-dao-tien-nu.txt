Mỗi chiều chiều biển tím mênh mông <br/>
Cả tình yêu thăm thẳm đợi chờ <br/>
Ánh hào giăng mặt trời đi ngủ <br/>
Sóng đốt đèn đêm biển với thuyền <br/>
<br/>
Thuyền rời bến, bến buồn tim tím <br/>
Tiễn thuyền đi, bến ngẩn ngơ chiều <br/>
Hàng phi lao bên bờ nhân chứng <br/>
Bàn tay che đôi mắt thuyền về <br/>
<br/>
Bình minh lên hoa sắc hồng đo đỏ <br/>
Con sóng chở thuyền đưa máu về tim <br/>
Chỉ nhận lại nụ hôn nồng ray rứt <br/>
Để dào dạt ngân mãi tình ca