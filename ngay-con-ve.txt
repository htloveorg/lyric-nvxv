Với Em tình yêu như giấc mơ khi Em gặp Anh <br/>
Phút giây gần nhau, Em ngỡ chìm sâu trong ánh mắt người <br/>
Có Anh mà thôi trong trái tim xót xa tình Em <br/>
Có Anh mà thôi trong giấc mơ về ngày mai <br/>
Và Em sẽ mãi đợi chờ sẽ mãi đợi chờ <br/>
Dù cho trôi qua hết những ngày xanh <br/>
Tình Em như sống sô bờ, sóng biếc xô bờ <br/>
Ngày xa Anh dâng cao nỗi nhớ ... <br/>
Ngàn năm khó phai một bóng hình <br/>
Tình Em thiết tha với bao kỷ niệm <br/>
Anh mang về đâu thời gian có nhau <br/>
Sẽ mãi xa vời một vầng mây xanh <br/>
Ngàn năm khó phai lời ru này <br/>
Bờ môi ấm êm hương xưa vẫn còn <br/>
Yêu Anh ngàn năm người ơi biết không <br/>
Âm thầm tình Em ....