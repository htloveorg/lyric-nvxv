Ngày xưa em như chim sáo, sống vô tư hay mộng mơ nhiều <br/>
Nhìn em đi qua cuối xóm, làn tóc mây bay má ửng hồng <br/>
Chiều nay theo em anh bước, bước bên em trên con đường làng <br/>
Nhìn em anh như muốn nói: "Này cô bé kia chờ anh đi cùng!" <br/>
<br/>
Ô hay, em này kỳ ghê, người ta đi về, chung đường thời kệ người ta <br/>
Cớ sao em vội đi mau, để khi ngoảnh lại nhìn nhau em thẹn thùng <br/>
Em ơi! đường về còn xa, để anh đưa về, đưa về anh chẳng tính công <br/>
Dẫu mai em có lấy chồng, anh xin làm người, làm người đưa sáo qua sông! <br/>
<br/>
 <br/>
Thời gian trôi đi nhanh quá, tiếng yêu tôi chưa kịp xếp vần <br/>
Một ngày kia lũy tre cuối xóm, chẳng thấy em chiều nay đi về <br/>
Hỏi ra mới hay chim sáo đã sang sông sáo đi lấy chồng <br/>
Còn đâu những chiều theo bước, giờ chỉ có anh lẻ loi đi về ... <br/>
<br/>
Ai đem chim sáo sang sông, để cho chim sáo sổ lồng, sổ lồng bay xa <br/>
Sáo bay bỏ lại mình ta, bơ vơ một nẻo, xa xăm đi về <br/>
Sáo ơi! Bây chừ ngồi đây chờ ai ai chờ, thôi rồi hết đợi hết trông <br/>
Trách cho số kiếp bọt bèo, duyên kia chẳng đặng, thì đành nhìn sáo sang sông!