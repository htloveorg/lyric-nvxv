<i>(nhạc: Doãn Bình - thơ: Lưu Trọng Lư)</i><br/>
<br/>
Em là gái bên khung cửa <br/>
Anh là mây bốn phương trời <br/>
Anh theo cánh gió chơi vơi <br/>
Em vẫn năm trong nhung lụa <br/>
<br/>
Em chỉ là em gái thôi <br/>
Người em sầu mộng muôn đời <br/>
Tình như tuyết giăng đầu núi <br/>
Vằng vặc muôn thu nét tuyệt vời <br/>
<br/>
Ai bảo em là giai nhân <br/>
Cho đời anh đau buồn <br/>
Ai bảo em ngồi bên song <br/>
Cho vương vấn nợ thi nhân <br/>
<br/>
Ai bảo em là giai nhân <br/>
Cho lệ đêm dâng tràn <br/>
Cho tình giăng đầy trước ngõ <br/>
Cho mộng tràn gối chăn