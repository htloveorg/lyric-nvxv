Nếu em là thi nhân <br/>
Cho anh biết làm thơ <br/>
Nếu em là mùa xuân <br/>
Cho anh biết mộng mơ <br/>
<br/>
Ôi bài thơ tuyệt vời <br/>
Của mùa xuân ngát trời <br/>
Cho tình yêu một thời <br/>
Để nhớ thương nhau trọn đời <br/>
<br/>
Nếu em là thánh nhân <br/>
Anh không còn ngoại đạo <br/>
Nếu em là thiên thần <br/>
Anh đi tìm xóm đạo <br/>
Tin nước Chúa vô cùng <br/>
Và tin em trọn đời <br/>
Như trái tim tuyệt vời <br/>
Trong Thánh Nữ Đồng Trinh <br/>
<br/>
Ơ… vì em là giấc mơ <br/>
Anh viết lên bài thơ <br/>
Cho người tình thi nhân <br/>
Cho người tình thánh nhân <br/>
Cho người tình giai nhân <br/>
Và cho người tình tha nhân <br/>
<br/>
Nếu em là giai nhân <br/>
Anh làm gã tình si <br/>
Trên đường trần gian nan <br/>
Thương tình cõi ngây thơ <br/>
<br/>
Yêu em thật vụng về <br/>
Trong bồi hồi ngỡ ngàng <br/>
Nhưng trọn nỗi ước thề <br/>
Của những ngày tháng trôi ngang <br/>
<br/>
Nếu em là tha nhân <br/>
Anh người tình viễn xứ <br/>
Lê buồn theo gót chân <br/>
Vương sầu tình thiên thu <br/>
<br/>
Nếu em là tình nhân <br/>
Trong muôn vàn yêu thương <br/>
Cho cuộc tình lâng lâng <br/>
Để ru hồn người tha phương