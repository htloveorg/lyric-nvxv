Ai qua miền quê binh khói, <br/>
Nhắn giúp rằng nơi xa xôi: <br/>
Tôi vẫn mơ tùm tre xanh ngát <br/>
Tim sắt se cảnh xưa hoang tàn <br/>
<br/>
Bao nhiêu ngày vui thơ ấu <br/>
Bao nhiêu lều tranh yêu dấu <br/>
Theo khói binh lều tan tre nát <br/>
Theo khói binh lòng quê héo tàn <br/>
<br/>
Ôi quạnh hiu, ôi quạnh hiu <br/>
Lòng quê khô héo <br/>
Luyến tình quê, luyến tình quê <br/>
hẹn sẽ trở về <br/>
<br/>
Về quê xưa để sống êm đềm giấc mơ <br/>
Về quê xưa tìm bóng những ngày đã qua <br/>
Và say sưa cuộc sống bên ngàn lũy tre <br/>
Xa lánh cuộc đời khắt khe <br/>
trăm đau ngàn thương.