1. Đường khuya vắng người mến thương xa rồi <br/>
mình tôi lắng hồn theo tiếng mưa rơi <br/>
lối về còn là bao hình bóng <br/>
nhớ lúc chia tay giữa đêm sương mơ <br/>
bùi ngùi thôi chẳng nói <br/>
bóng ai xa rồi còn ai đứng nhìn theo <br/>
<br/>
ĐK <br/>
Đêm lắng chìm vào ngàn xa cuối trời những hạt mưa rơi <br/>
trên lối mòn còn mình tôi nhớ người <br/>
ngàn phương xa vắng <br/>
Đêm ánh đèn mờ nẻo khuya vẫn còn mưa tuôn <br/>
nghe gió lanh mà xao xuyến lòng chan chứa tình thương <br/>
Đường khuya phố buồn nhớ thương giăng mờ <br/>
Người ơi biết rằng nơi chốn xa xưa <br/>
có người tìm vần thơ mà để <br/>
tiễn bước ai đi xông pha muôn trùng để ngày mai <br/>
nối câu duyên lành mà vui nốt ngày xanh <br/>
<br/>
2. Người đi nhớ gì giữa đêm kinh kỳ <br/>
trời khuya có người mơ bước ai đi <br/>
suối đời cùng rừng xanh vạn lý <br/>
gió núi mưa tuôn xe đôi vai nặng vì tình non tình nước <br/>
ước mơ mai về kể hết chuyện xưa <br/>
(ĐK).