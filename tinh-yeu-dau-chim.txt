Em đứng lên gọi mưa vào Hạ <br/>
Từng cơn mưa từng cơn mưa <br/>
Từng cơn mưa mưa thì thầm dưới chân ngà <br/>
<br/>
Em đứng lên mùa Thu tàn tạ <br/>
Hàng cây khô cành bơ vơ <br/>
Hàng cây đưa em đi về giọt nắng nhấp nhô <br/>
<br/>
Em đứng lên mùa Đông nhạt nhòa <br/>
Từng đêm mưa từng đêm mưa <br/>
Từng đêm mưa mưa lạnh từng ngón sương mù <br/>
<br/>
Em đứng lên mùa Xuân vừa mở <br/>
Nụ xuân xanh cành thênh thang <br/>
Chim về vào ngày tuổi em trên cành bão bùng <br/>
<br/>
Rồi mùa Xuân không về <br/>
Mùa Thu cũng ra đi <br/>
Mùa Đông vời vợi <br/>
Mùa Hạ khói mây <br/>
<br/>
Rồi từ nay em gọi <br/>
Tình yêu dấu chim bay <br/>
Gọi thân hao gầy <br/>
Gọi buồn ngất ngây <br/>
<br/>
Ôi tóc em dài đêm thần thoại <br/>
Vùng tương lai chợt xa xôi <br/>
Tuổi xuân ơi sao lạnh dòng máu trong người <br/>
<br/>
Nghe xót xa hằn trên tuổi trời <br/>
Trẻ thơ ơi trẻ thơ ơi <br/>
Tin buồn từ ngày mẹ cho mang nặng kiếp người