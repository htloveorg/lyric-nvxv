Tôi kể người nghe đời Lan và Điệp, một chuyện tình cay đắng <br/>
Lúc tuổi còn thơ tôi vẫn thường mộng mơ đem viết thành bài ca <br/>
Thuở ấy Điệp vui như bướm trắng, say đắm bên Lan <br/>
Lan như bông hoa ngàn, thương yêu vô vàn, <br/>
nguyện thề non nước sẽ không hề lìa tan <br/>
<br/>
Chuông đổ chiều sang , chiều tan trường về, <br/>
Điệp cùng Lan chung bước <br/>
Cuối nẻo đường đi, đôi bóng hẹn mùa thi Lan khóc đợi người đi <br/>
Lần cuối gặp nhau, Lan khẽ nói, thương mãi nghe anh, <br/>
em yêu anh chân tình nếu duyên không thành, <br/>
Điệp ơi Lan cắt tóc quên đời vì anh <br/>
<br/>
Nhưng ai có ngờ, lời xưa đã chứng minh khi đời tan vỡ <br/>
Lan đau buồn quá khi hay Điệp đã xây mộng gia đình <br/>
Ai nào biết cho ai, đời quá chua cay duyên đành lỡ vì ai <br/>
Bao nhiêu niềm vui cũng vùi chôn từ đây, vùi chôn từ đây <br/>
<br/>
Lỡ một cung đàn, phải chi tình đời là vòng giây oan trái <br/>
Nếu vì tình yêu Lan có tội gì đâu sao vướng vào sầu đau <br/>
Nàng sống mà tim như đã chết <br/>
Duyên bóng cô đơn đôi môi xin phai tàn <br/>
Thương thay cho nàng <br/>
Buồn xa nhân thế náu thân cửa Từ Bi