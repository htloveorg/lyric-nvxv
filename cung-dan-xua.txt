Buồn đến khi trong lòng xa vắng rồi <br/>
chiều xuống mênh mông hồn vàng rơi <br/>
rượu đắng cho tình say ôi tuyệt vời <br/>
nhạc buồn ghi dấu xưa rồi thế thôi <br/>
<br/>
còn đâu trên hương sắc lối xưa em đi <br/>
tình xưa nay đã chết bên bờ trăng thề <br/>
môi cưòi rồi đã hết như chiều tàn về <br/>
đã quên rồi chiều xưa trao lời ân ái <br/>
<br/>
buồn đến trong sương chiều xa vắng người <br/>
buồn đến khi sương mờ vàng rơi <br/>
tình đã như dòng sông xa vời vợi <br/>
tình buồn con nước xưa hờ hững trôi