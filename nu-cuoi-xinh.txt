Em ơi! có phải ngoài trời đang mưa ?? <br/>
Em ơi! có phải trời đã sang đông ?? <br/>
Mùa đông giá băng anh đang chờ <br/>
Mùa đông ái ân anh đang tìm <br/>
Tìm màu áo cưới cho em.... <br/>
<br/>
Ô hay, mắt ngọc lại buồn hay sao ?? <br/>
Khi anh đã nguyện một đời yêu em <br/>
Dù cho nét son môi phai mờ <br/>
Dù cho mắt xanh kia hững hờ <br/>
Và dù năm tháng phôi pha. <br/>
<br/>
DK Ta quen biết nhau ... Khi tàn xuân <br/>
Ta yêu thiết tha ... Khi hè sang <br/>
Và khi thu đến anh gom ánh sao <br/>
Cho đêm đêm kết thành vương miện <br/>
Để mùa đông đám cưới đôi mình <br/>
<br/>
Em ơi! xích lại thật gần bên anh.. <br/>
Cho anh xiết chặt nụ cười xinh xinh <br/>
Từ đây những đêm trăng thanh đầy <br/>
Mình không lẻ loi không u sầu <br/>
Nguyện cầu ta mãi bên nhau