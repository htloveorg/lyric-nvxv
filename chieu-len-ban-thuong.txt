Gió cuốn theo chiều xuống qua bao đồi nương. <br/>
Nắng úa trên nhành lá khi ánh chiều buông. <br/>
Tiếng hát cô vường nương trên rừng chiều bao la, <br/>
qua suối đồi khe lá. <br/>
Tiếng hát ai buồn quá bên lưng đèo cao, <br/>
tiếng suối bên ngành đá dư âm về đâu. <br/>
Nghe ngợi bao niềm thương yêu về miền cô liêu <br/>
bên bản xưa rừng chiều. <br/>
<br/>
ĐK: <br/>
<br/>
Hò ơi, vang ca vang rừng sâu bên nhau, <br/>
vai kề vai trong tay ly rượu ấm. <br/>
Nhìn nhau đi anh cho niềm thương mong manh <br/>
lan rừng xanh bên thác uốn quanh. <br/>
Rừng ơi, vang lên muôn lời ca xa xa trong màn sương, <br/>
âm u khi chiều xuống. <br/>
Về đây hôm nao nghe đời thôi thương đau, <br/>
mong ngày sau rừng xanh thắm màu. <br/>
Khói sáng vương đồi núi lan trong chiều sương. <br/>
Thấp thoáng trong làn khói, <br/>
bóng ai dừng chân, thắm thiết câu mộng mơ. <br/>
Ôi rừng đồi hoang xơ tôi vẫn còn ghi nhớ. <br/>
Nhớ biết bao hình bóng không bao giờ không bao giờ phai. <br/>
Nhớ tiếng khen chiều ấy như ru lòng ai. <br/>
Nhớ dáng ai ngồi đây khi chiều nào quay tơ bên bản xưa đợi chờ