Mỗi ngày tôi chọn một niềm vui <br/>
Chọn những bông hoa và những nụ cười <br/>
Tôi nhặt gió trời mời em giữ lấy <br/>
Để mắt em cười tựa lá bay <br/>
<br/>
Mỗi ngày tôi chọn đường mình đi <br/>
Đường đến anh em đường đến bạn bè <br/>
Tôi đợi em về bàn chân quen quá <br/>
Thảm lá me vàng lại bước qua <br/>
<br/>
Và như thế tôi sống vui từng ngày <br/>
Và như thế tôi đến trong cuộc đời <br/>
Đã yêu cuộc đời này bằng trái tim của tôi <br/>
<br/>
Mỗi ngày tôi chọn một niềm vui <br/>
Cùng với anh em tìm đến mọi người <br/>
Tôi chọn nơi này cùng nhau ca hát <br/>
Để thấy tiếng cười rộn rã bay <br/>
<br/>
Mỗi ngày tôi chọn một lần thôi <br/>
Chọn tiếng ru con nhẹ bước vào đời <br/>
Tôi chọn nắng đầy, chọn cơn mưa tới <br/>
Để lúa reo mừng tựa vẫy tay <br/>
<br/>
Và như thế tôi sống vui từng ngày <br/>
Và như thế tôi đến trong cuộc đời <br/>
Đã yêu cuộc đời này bằng trái tim của tôi <br/>
<br/>
Mỗi ngày tôi chọn ngồi thật yên <br/>
Nhìn rõ quê hương, ngồi nghĩ lại mình <br/>
Tôi chợt biết rằng vì sao tôi sống <br/>
Vì đất nước cần một trái tim !