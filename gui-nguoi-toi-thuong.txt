Người ơi thắm thoát niên học hết rồi. <br/>
Chúc đi cạn lời giây phút ly bôi. <br/>
Ngày mai tan trường mình không chung lối. <br/>
Thương nhau nhiều biết gửi về mô. <br/>
Kỷ niệm cũ tan vào hư vô. <br/>
<br/>
Cầm tay bốn mắt thương cảm nỗi sầu. <br/>
Tiễn đưa bùi ngùi bốn mắt như nhau. <br/>
Đời không bao giờ gặp nhau mãi mãi. <br/>
Thương yêu rồi nỡ đành biệt nhau. <br/>
Để nhung nhớ muôn vạn ngày sau. <br/>
<br/>
Thôi nhé, từ đây cách xa trong đời. <br/>
Vẫn buồn theo tháng ngày trôi. <br/>
Nụ cười khô héo trên môi. <br/>
Mỗi lần thấy phượng nở tim xao xuyến. <br/>
Bạn bè đâu chỉ ta một mình. <br/>
Nỗi buồn này đành câm nín. <br/>
<br/>
Rồi đây, có những khi buồn não lòng. <br/>
Cố nhân biền biệt có nhớ nhau không. <br/>
Ngoài kia hoa phượng rụng rơi tơi tả. <br/>
Dư âm làm sống lại đời ta. <br/>
Dù ngăn cách nhớ hoài ngày qua