<i>(nhạc: Võ Tá Hân - thơ: Thích Quảng Thanh)</i><br/>
<br/>
<br/>
Hôm nay ngày mẹ nhớ thương <br/>
Con quỳ lạy Phật dâng hương nguyện cầu <br/>
Cầu xin cho mẹ sống lâu <br/>
Mẹ là tất cả nhiệm mầu thiêng liêng <br/>
<br/>
Mẹ là thủy thủ con thuyền <br/>
Vượt bao sóng ngàn hiểm nguy muôn trùng <br/>
Mẹ như biển rộng bao dung <br/>
Tình thương chan chứa vô cùng những cho <br/>
<br/>
Mẹ cho sự sống ấm no <br/>
Mẹ cho hương sắc, mẹ cho mặn nồng <br/>
Lòng mẹ như một dòng sông <br/>
Con như chiếc lá bềnh bồng nước trôi <br/>
<br/>
Mẹ như cả một bầu trời <br/>
Vòng tay kỳ diệu trọn đời thương con <br/>
Thời gian sức mẹ hao mòn <br/>
Nhưng tình nguyên vẹn đầy tròn trước sau <br/>
<br/>
Hôm nay ngày mẹ nêu cao <br/>
Món quà dâng mẹ ngọt ngào thương yêu <br/>
Mẹ là biểu tượng cao siêu <br/>
Cho con tất cả thật nhiều tinh hoa <br/>
<br/>
Mẹ là vũ trụ bao la <br/>
Vòng tay của mẹ thiết tha vô ngần <br/>
Nề chi cam khổ gian nan <br/>
Mẹ đưa vai gánh phong trần thay con <br/>
<br/>
Bài thơ tình mẹ sắt son <br/>
Con xin dâng hiến ngợi ca mẹ hiề