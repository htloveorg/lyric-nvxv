Trời thì bao la ...thuyền ai lướt trên dòng sông nước xanh <br/>
Chìm trong sương sa ...Muôn ánh sao huyền lấp lánh <br/>
Như cảm thương tình đã lỡ khuất xa <br/>
Sóng vàng triền miên <br/>
Lắng câu ca hò tình duyên <br/>
Vài bóng lau thưa <br/>
nhìn sông vắng ôi tiêu điều bến mơ <br/>
<br/>
Đàn ai buông tơ <br/>
dưới ánh trăng mờ hơi sương <br/>
Trời thu vấn vương <br/>
Theo gió mây muôn ngàn lá úa bay <br/>
Tháng ngày chờ mong <br/>
Bấp bênh con thuyền trên giòng <br/>
<br/>
Một chiều thu xưa <br/>
Lòng say mê theo gió đưa <br/>
Đắm say trên giòng này mơ màng <br/>
Tiếng ca trầm lắng <br/>
<br/>
Rồi một ngày mai <br/>
thuyền ai trôi xuôi buông lái <br/>
Kiếp tha phương mặc giòng nước mờ <br/>
Quên tình bến mơ <br/>
<br/>
Chiều nay trên sông <br/>
Tình đưa sóng trôi theo người viễn phương <br/>
Và trong mênh mông <br/>
Đôi én giang hồ không hướng <br/>
Vượt mây sánh đôi <br/>
Như nhắn ai hận lòng bao mối nguôi <br/>
Bẽ bàng đường tơ <br/>
Cho sầu vương bến mơ<br/>
<br/>
Nguồn : ĐT