(nhạc: Phạm Thế Mỹ - thơ Nhất Hạnh)<br/>
<br/>
<br/>
Một bông Hồng cho em <br/>
Một bông Hồng cho anh <br/>
Và một bông Hồng cho những ai <br/>
Cho những ai đang còn Mẹ <br/>
Đang còn Mẹ để lòng vui sướng hơn <br/>
Rủi mai này Mẹ hiền có mất đi <br/>
Như đóa hoa không mặt trời <br/>
Như trẻ thơ không nụ cười <br/>
ngỡ đời mình không lớn khôn thêm <br/>
Như bầu trời thiếu ánh sao đêm <br/>
<br/>
Mẹ, Mẹ là giòng suối dịu hiền <br/>
Mẹ, Mẹ là bài hát thần tiên <br/>
Là bóng mát trên cao <br/>
Là mắt sáng trăng sao <br/>
Là ánh đuốc trong đêm khi lạc lối <br/>
<br/>
Mẹ, Mẹ là lọn mía ngọt ngào <br/>
Mẹ, Mẹ là nải chuối buồng cau <br/>
Là tiếng dế đêm thâu <br/>
Là nắng ấm nương dâu <br/>
Là vốn liếng yêu thương cho cuộc đời <br/>
<br/>
Rồi một chiều nào đó anh về nhìn Mẹ yêu, nhìn thật lâu <br/>
Rồi nói, nói với Mẹ rằng "Mẹ ơi, Mẹ ơi, Mẹ có biết hay không ?" <br/>
-Biết gì ? "Biết là, biết là con thương Mẹ không ?" <br/>
<br/>
Đóa hoa màu hồng vừa cài lên áo đó anh <br/>
Đóa hoa màu hồng vừa cài lên áo đó em <br/>
Thì xin anh, thì xin em <br/>
Hãy cùng tôi vui sướng đi.