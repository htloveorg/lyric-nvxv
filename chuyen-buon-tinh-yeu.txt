Nói đi em câu chuyện buồn tình yêu <br/>
Nói đi em vì mình thương quá nhiều <br/>
Khóc làm chi cho hoen úa rèm mi <br/>
Cho nát cánh xuân thì <br/>
Nhớ thương nhau rồi đi <br/>
Nói đi em khi chuyện mình dở dang <br/>
Nói đi em vì tình duyên lỡ làng <br/>
Khóc làm chi cho chua xót tình yêu <br/>
Cho cay đắng thêm nhiều <br/>
Nhưng chẳng được bao nhiêu <br/>
Mai lên xe hoa em sầu trong áo cưới <br/>
Mai lên xe hoa còn thương nhớ một người <br/>
Ai gieo đau thương đếm từng đêm tiếc nuối <br/>
Em đã đi rồi em đã quên người tình rồi <br/>
Nói đi em cho vơi niềm thương đau <br/>
Nói đi em để lòng nguôi nỗi sầu <br/>
Khóc làm chi cho đau đớn người đi <br/>
Cho héo úa xuân thì ta âm thầm chia ly