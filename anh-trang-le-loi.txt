Này người yêu hỡi, xin đừng quay bước<br/>
Vì em đã trót yêu anh<br/>
Tiếng yêu đầu ngại ngùng, dẫu ân tình ngàn trùng<br/>
Muôn bể dâu em nào biết đâu<br/>
<br/>
Này người yêu hỡi, ân tình em đó<br/>
Ngàn năm như ánh trăng tan<br/>
Vẫn muôn đời dịu dàng, vẫn muôn đời nồng nàn<br/>
Tha thiết yêu dù tình yêu trái ngang<br/>
<br/>
Ái ân như bọt sóng trào<br/>
Tan vỡ mau bước chân sỏi đá<br/>
Trái tim êm bờ cát mềm<br/>
Bàn chân ai vội vàng im dấu quên<br/>
<br/>
Thuyền tình xa bến, mê đời hoang phế<br/>
Buồn trôi theo ánh trăng tan<br/>
Những đêm về lạnh lùng, gió mưa gào ngàn trùng<br/>
Hoen mắt em đường khuya trăng lẽ loi