<i>(nhạC: Văn Giảng - lời: Võ Phương Tùng)</i><br/>
<br/>
<br/>
<br/>
Canh dài ta ngồi trong rừng cây vang âm hồn thiên thu <br/>
Trời vắng hồn lắng tiếng sơn hà trong gió hú: <br/>
<br/>
"Ai thấy chăng xưa hùng cường? <br/>
Ai thấy chăng nay xiềng cùm <br/>
Đằng đằng nặng hận thù? <br/>
Ai đắp non sông trường tồn? <br/>
Ai kết lên dân tài hùng <br/>
Xua tan giặc Đông Hán <br/>
Xua tan giặc xâm lấn?" <br/>
<br/>
Ta cùng chung lòng mong ngày vang danh thơm dòng oai linh <br/>
Thề quyết rèn chí quét quân thù đang cướp nước <br/>
Ta cháu con dân Việt hùng <br/>
Nơi Mê Linh ta trùng phùng <br/>
Đồng lòng nguyền vẫy vùng <br/>
Ta chiến binh đang thề nguyền <br/>
Quanh ánh thiên nung lòng bền <br/>
Gian nguy càng hăng chí <br/>
Xung phong chờ đến ngày <br/>
<br/>
Ai vì nước? <br/>
Ai thề ước? <br/>
Ta xung phong nguyền dâng thân <br/>
Hiên ngang nguyện đấu tranh xua tan quân Đông Hán <br/>
Ai trung thành? <br/>
Ai liều mình? <br/>
Thề hy sinh, thề tung hoành hiên ngang <br/>
Thề kiên trung chiến đấu, thề chiến thắng! <br/>
<br/>
Canh dài ta ngồi mơ ngày đi xông pha giành non sông <br/>
Ngời chói bừng sáng ánh tươi hồng hăng chí nóng <br/>
Quanh ánh thiêng reo bùng bùng <br/>
Ta nắm tay ca trầm hùng <br/>
Hẹn ngày rạng Lạc Hồng <br/>
Mơ xuất quân đi rập ràng <br/>
Mơ quét tan quân bạo tàn <br/>
Xua tan giặc Đông Hán <br/>
Xua tan giặc xâm lấn