Ngày xưa trời ở trên trời <br/>
Trời xui đằng ấy, đến ngồi bên ta <br/>
Ngày xưa đằng ấy nhà xa <br/>
Tan trường mưa quá nên ta đưa về <br/>
<br/>
Ngày xưa đằng ấy tóc thề <br/>
Ta thời tóc ngắn nên về tương tư <br/>
Ngày xưa ta viết phong thư <br/>
Đằng ấy nhận được hình như bằng lòng <br/>
<br/>
Ngày xưa có tiếng thì thầm <br/>
Mai sau đừng có thay lòng đổi thay <br/>
Ngày xưa đằng ấy đâu quên <br/>
Tại trời mưa quá cho nên phải lòng <br/>
<br/>
Thôi thì trời đã sang đông <br/>
Thôi thì người đã đành lòng quên ta <br/>
Trời ơi, trời ở cao xa <br/>
Ngày xưa nhắc lại chỉ là ngày xưa