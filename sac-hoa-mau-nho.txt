Hoa phượng rơi đón mùa thu tới <br/>
Màu lưu luyến nhớ quá thu ơi <br/>
Ngàn phượng rơi bay vương tóc tôi <br/>
Sắc tươi màu pháo vui <br/>
tiễn em chiều năm ấy <br/>
<br/>
Xưa từ khu chiến về thăm xóm <br/>
Ngàn xác pháo lấp ánh sao hôm <br/>
Chiều hành quân nay qua lối xưa <br/>
Giữa một chiều gió mưa <br/>
xác hoa hồng mênh mông <br/>
<br/>
Đời tôi quân nhân, chút tình duyên gởi núi sông <br/>
Yêu màu gợi niềm thủy chung <br/>
Xa rồi vẫn nhớ, một trời vẫn nhớ đời đời <br/>
Phượng rơi rơi trong lòng tôi <br/>
<br/>
Thu vừa sang sắc hồng tô lối <br/>
Tình thu thắm thiết quá thu ơi <br/>
Nhìn màu hoa xưa tan tác rơi <br/>
Nhớ muôn vàn nhớ ơi <br/>
hát trong màu hoa nhớ <br/>
<br/>
Tôi lại đi giữa trời sương gió <br/>
Màu hoa thắm vẫn sống trong tôi <br/>
Chiều thu sau không qua lối xưa <br/>
Đến những trời gió mưa <br/>
xác hoa hồng mênh mông