Tôi vẫn nhìn thấy em <br/>
Giữa đám đông xa lạ <br/>
Vì em mang trong mắt <br/>
Nỗi yêu đời thiết tha <br/>
Tôi vẫn nhìn thấy em <br/>
Giữa đám dông xa lạ <br/>
Vì trong đôi mắt đó <br/>
Có quê hương bạn bè <br/>
<br/>
Em đã đến giữa quê hương <br/>
Có những nghìn năm xưa <br/>
Hoá thân em bây giờ <br/>
Nên tôi vẫn nhìn thấy em <br/>
Giữa đám đông xa lạ <br/>
Vì em như chim trắng <br/>
Giữa trống đồng bước ra <br/>
Tôi vẫn nhìn thấy em <br/>
Giữa đám đông xa lạ <br/>
Vì em như hoa lá <br/>
Giữa thiên nhiên hiền hòa