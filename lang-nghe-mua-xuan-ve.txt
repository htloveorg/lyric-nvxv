Giọt mưa nào rơi thật êm trên phố phường <br/>
Mùa hương nào thơm thật thơm trong gió thoảng <br/>
Và em đợi anh đợi anh như đã hẹn <br/>
Nghe trong mưa đêm mùa xuân lặng lẽ sang. <br/>
Phải chăng mầm non mùa xuân đang hé nở <br/>
Phải chăng nụ hoa mùa xuân đang hé nở <br/>
Phải chăng ngày xuân đầu tiên đang gõ cửa <br/>
Khi anh trông em ung dung bên thềm nhà. <br/>
Kìa tiếng chim rộn hót xa vời, <br/>
Cánh hoa đào bỗng như cười, báo tin mùa xuân về <br/>
Kìa bóng đêm mùa cũ đâu rồi, <br/>
Với em chỉ thấy xanh ngời, lá hoa của xuân tươi, rồi anh tới.. <br/>
<br/>
Giọt mưa nào rơi thật êm trên phố phường <br/>
Mùa hương nào thơm thật thơm trong gió thoảng <br/>
Và em đợi anh đợi anh như đã hẹn <br/>
Nghe trong mưa đêm mùa xuân lặng lẽ sang. <br/>
Phải chăng mầm non mùa xuân đang hé nở <br/>
Phải chăng nụ hoa mùa xuân đang hé nở <br/>
Phải chăng ngày xuân đầu tiên đang gõ cửa <br/>
Khi anh trông em ung dung bên thềm nhà. <br/>
Và chúng ta lại đón giao thừa, <br/>
Phút giây lặng lẽ mong chờ, lắng nghe mùa xuân về <br/>
Để biết ta còn mãi trong đời, <br/>
Phút mong chờ ấy tuyệt vời, chứa chan niềm tin yêu, kìa anh tới.