<em><b>Lời: Thảo Chi</em></b><br/>
<br/>
<br/>
Trèo lên con dốc, em tìm nắng <br/>
Đẩy hết mù sương xuống thung lũng sâu <br/>
Chỉ thấy mênh mang toàn là tuyết trắng <br/>
Và em mỗi bước một sầu thêm, <br/>
Và em mỗi bước một thêm sầu... <br/>
<br/>
Trèo lên con dốc, em tìm gió <br/>
Xé toạc rừng mây phủ núi xanh <br/>
Chỉ thấy ngàn cây run nhịp thở <br/>
Và em mỗi bước một mong manh <br/>
Và em mỗi bước một mong manh... <br/>
<br/>
Trèo lên con dốc, em tìm suối <br/>
Vạch lá vi lao đứng kín đồi <br/>
Chỉ thấy quạ rừng kêu hấp hối <br/>
Và em mỗi bước một xa rời <br/>
Và em mỗi bước một xa xôi... <br/>
<br/>
Ôi con đường dốc, không đường nắng <br/>
Đám lá nằm co chẳng gió trêu <br/>
Suối lạnh trên ngàn đong tuyết trắng <br/>
Dốc mòn sỏi đá trổ rong rêu