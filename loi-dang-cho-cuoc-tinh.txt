Cuối cùng rồi mình vẫn thế <br/>
Khóc chi em, khóc chi em <br/>
Cho phai má hồng được gì <br/>
Giọt lệ này giành để mai đây <br/>
Về cùng người khóc giữa đêm vui <br/>
Hơi đâu, hơi đâu mà xót thương thân anh <br/>
<br/>
Những ngày còn nồng ân ái <br/>
Giữa đôi ta, giữa đôi ta <br/>
Đã bao cách biệt trùng trùng <br/>
Người thì mộng một trời cao sang <br/>
Người thì còn trắng áo tay trơn <br/>
Nên anh, nên anh đã biết được ngày này <br/>
<br/>
Em ơi, em ơi anh vẫn biết rằng <br/>
Con chim quý phải ở lầu son <br/>
Nên anh không trách là em bội bạc <br/>
Mà em ơi anh chỉ trách em <br/>
Trách em, sao em không nói thật lòng mình <br/>
Sao em gian dối để làm gì <br/>
<br/>
Hãy nhìn một lần sau chót <br/>
Chén ly bôi, uống đi em <br/>
Sao em mắt lệ nhạt nhòa <br/>
Cuộc tình nào rồi cũng phôi pha <br/>
Một đường tàu biết mấy sân ga <br/>
Xin em xem anh như một ga nhỏ dọc đường