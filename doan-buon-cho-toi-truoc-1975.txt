Từ một ngày từ ngày mới yêu nhau <br/>
Mình hằng mơ không dang dở tình đầu ! <br/>
Tình còn đầy mà người tình đã quên mau, <br/>
Người đành lòng không xót đau <br/>
Lạnh lùng bỏ đi không nói. <br/>
<br/>
Đoạn đường nào từng dìu bước nhau đi ? <br/>
Người đã quên ôi quên biệt đường về.. <br/>
Sầu gợi sầu, rã rời lệ ướt hoen mi, <br/>
Đoạn buồn người ơi có nghe <br/>
Khi mưa mưa lạnh về đêm. <br/>
<br/>
<br/>
Nhớ hôm ao ta mới giận hờn. <br/>
Đưa nhau về đường phố mưa trơn <br/>
Ôi bây giờ xa cách ngàn trùng <br/>
Người đời thường hay gian dối. <br/>
Tôi xin chấp nhận tủi buồn. <br/>
<br/>
Còn một mình mốt mình bước đơn côi <br/>
Và từ đây tôi xin lạy người đời <br/>
Đừng hẹn hò và đừng gặp gỡ chi thêm <br/>
Trọn nghiệp đàn ca thế thôi <br/>
Cho nhân gian mượn tìm vui.