Chiều mưa biên giới anh đi về đâu? <br/>
Sao còn đứng ngóng nơi giang đầu <br/>
Kìa rừng chiều âm u rét mướt <br/>
Chờ người về vui trong giá buốt <br/>
người về bơ vơ <br/>
<br/>
Tình anh theo đám mây trôi chiều hoang <br/>
Trăng còn khuyết mấy hoa không tàn <br/>
Cờ về chiều tung bay phất phới <br/>
Gợi lòng này thương thương nhớ nhớ <br/>
Bầu trời xanh lơ <br/>
<br/>
Đêm đêm chiếc bóng bên trời <br/>
Vầng trăng xẻ đôi <br/>
Vẫn in hình bóng một người <br/>
Xa xôi cánh chim tung trời <br/>
Một vùng mây nước <br/>
Cho lòng ai thương nhớ ai <br/>
<br/>
Về đâu anh hỡi mưa rơi chiều nay <br/>
Lưng trời nhớ sắc mây pha hồng <br/>
Đường rừng chiều cô đơn chiếc bóng <br/>
người tìm về trong hơi áo ấm <br/>
Gợi niềm xa xăm <br/>
<br/>
Người đi khu chiến thương người hậu phương <br/>
Thương màu áo gởi ra sa trường <br/>
Lòng trần còn tơ vương khanh tướng <br/>
Thì đường trần mưa bay gió cuốn <br/>
Còn nhiều anh ơi <br/>
<br/>
Lòng trần còn tơ vương khanh tướng <br/>
Thì đường trần mưa bay gió cuốn <br/>
Còn nhiều anh ơi