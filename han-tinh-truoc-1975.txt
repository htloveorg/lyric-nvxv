Nếu mai em chết anh có buồn không ? <br/>
Sao anh không đến khi em còn sống !? <br/>
Em đi âm thầm giữa chiều lồng lộng <br/>
Phố đông có người ta vui, <br/>
Mà em một bóng trông mong... <br/>
<br/>
Lỡ mai em chết anh khóc nhiều không ? <br/>
Sao không âu yếm khi em còn sống !? <br/>
Cho em không còn nghĩ đời tuyệt vọng <br/>
Những đêm đứng nhìn qua sông <br/>
Tình em yêu đến ngập lòng ...! <br/>
<br/>
Anh ơi, yêu là gì ? <br/>
Khi một người đêm ngóng chờ ngày thương nhớ <br/>
Khi một người vùi đời hoa trong xót xa... <br/>
<br/>
Em đi giữa sương đêm <br/>
Một mình buồn nát con tim <br/>
Ôm mối hận cho mối tình của đời em... <br/>
<br/>
Lỡ mai em chết anh hứa gì không.. <br/>
Em xin bia đá thêm tên ngưòi sống. <br/>
Em xin ngôi mộ đắm lệ tình nồng <br/>
Những khi gió lùa đêm đông, <br/>
Hồn em không quá lạnh lùng...!