Sàigòn mùa xuân còn thoáng lá vàng bay <br/>
Có mùa thu nào đang ở lại <br/>
Mặt đường bình yên nằm ngoan như con suối <br/>
Kết hoa vàng cho lộng lẫy đời <br/>
<br/>
Ngọn gió rung cành khi chiều chưa hết nắng <br/>
Đường phố em về tóc cùng hoa quyến luyến <br/>
Chồi lá khoe mầm cho đời biết tên <br/>
Mùa xuân thay lá mùa đông <br/>
Để cho chim hót chuyện tình. <br/>
Mùa xuân thay lá mùa đông <br/>
Để cho chim hót chuyện tình. <br/>
<br/>
Sàigòn mùa xuân về dưới những hàng cây <br/>
Có nhiều tiếng cười như trẻ lại <br/>
Ngày vội vàng lên bình minh thay đêm tối <br/>
Nắng phai từ lâu chiều vẫn dài. <br/>
<br/>
Ngọn gió rung cành khi chiều chưa hết nắng <br/>
Đường phố em về tóc cùng hoa quyến luyến <br/>
Chồi lá khoe mầm cho đời biết tên <br/>
Mùa xuân thay lá mùa đông <br/>
Để cho chim hót chuyện tình. <br/>
Mùa xuân thay lá mùa đông <br/>
Để cho chim hót chuyện tình.