(nhạc: Phạm Duy - thơ: Diệu Văn)<br/>
<br/>
<br/>
Ra biển chiều nay thấy mầu máu đỏ<br/>
Máu đỏ bầu trời, máu đỏ chân mây<br/>
Gió rên siết như ngàn lời uất hận<br/>
Sóng gầm gừ giận dữ cát tung bay<br/>
Ra biển mà coi xác người trôi dạt<br/>
Xác của người già, xác của em thơ<br/>
Xác trôi quanh tóc nhoà theo sóng cuộn<br/>
Hố mắt đen ngòm, bầy cá đùa chơi...<br/>
Ra biển chiều nay thấy người hấp hối<br/>
Ngồi ôm nhau, nhịn đói đã bao ngày<br/>
Chiếc thuyền con chồng chềnh theo ngọn sóng<br/>
Ði về đâu trời bão tố đêm nay.<br/>
Một lũ điên cuồng mang mầu chủ nghĩa<br/>
Khổng giáo tam tài, phong kiến vua tôi<br/>
Lão vô vi, Phật luân hồi, Chúa bác ái<br/>
Ðong mãi không đầy một bát cơm vơi<br/>
Ra biển chiều nay thấy máu đỏ<br/>
Máu của chị và máu của anh, em<br/>
Tư bản đến : bom đạn và thuốc độc<br/>
Lũ Cộng vào : xác ngập Thái Bình Dương<br/>
Ra biển mà coi lũ người ác nghiệt<br/>
Xua đuổi thuyền bè, cướp của hãm hiếp<br/>
Máu lương dân sẽ oà trong giấc mộng<br/>
Dân chúng quanh vùng miền nước Biển Ðông<br/>
Ra biển mà thương khóc người máu mủ<br/>
Ra biển mà nuôi oán thù loài cầm thú<br/>
Loài vô nhân, loài ích kỷ, loài ác quỷ...