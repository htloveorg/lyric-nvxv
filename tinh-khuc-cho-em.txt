Như hoa đem tin ngày buồn <br/>
Như chim đau quên mùa xuân <br/>
Còn trong hôn mê buồn tênh <br/>
Lê mãi những bước ê chề <br/>
Xin cho thương em thật lòng (2x) <br/>
Còn có khi lòng thôi giá băng <br/>
<br/>
Cho em môi hôn vội vàng <br/>
Cho em quen ân tình sâu <br/>
Dù em không mong dài lâu <br/>
Xin cất lấy ước mơ đầu <br/>
Cho tôi yêu em nồng nàn (2x) <br/>
Dù tháng năm buồn vui bàng hoàng <br/>
<br/>
Vì đâu mê say phồn hoa <br/>
Như áo gấm sáng lóng lánh <br/>
Ôm rách nát trong tâm linh <br/>
Ôm tiếng hát không hơi rung nghèo nàn <br/>
Còn yêu chi hoa ngày xanh <br/>
Héo hon vì mong manh <br/>
Bỏ quên lại người sau lỡ làng. (ngỡ ngàng) <br/>
<br/>
Thương em khi yêu lần đầu <br/>
Thương em lo âu tình sau <br/>
Dù gương xưa không được lau <br/>
Soi lấy bóng mối duyên sầu <br/>
Cho tôi yêu em nồng nàn (2x) <br/>
Dù biết yêu tình yêu muộn màng