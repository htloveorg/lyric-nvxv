Đừng đi em ơi <br/>
Bên kia núi đâu có trời xanh hơn <br/>
Bên kia sóng đâu vỗ về miên man <br/>
Bên kia đời đâu có gì hân hoan <br/>
<br/>
Đừng xa nhau em <br/>
hạt sương cũ chan chứa tình đơn sơ <br/>
Bông hoa cũ vẫn nở trọn trong ta <br/>
Những kỷ niệm đâu dễ gì phôi pha <br/>
<br/>
Đừng đi em nhé <br/>
vì những buồn vui <br/>
sẽ nối lòng ta <br/>
Thành mối dây đời trói ta từ đây <br/>
Thành mối dây đời trói ta từ đây <br/>
<br/>
Anh xin làm cỏ dại đón bước chân em <br/>
Anh xin làm tàng cây che mát thân em <br/>
<br/>
Đừng đi em ơi <br/>
Bên kia gió đâu có gì xôn xao <br/>
Bên kia nắng đâu có còn lung linh <br/>
Bên kia trời đâu có nhiều trăng sao <br/>
Đừng xa nhau em dòng thư cũ em có còn say mê <br/>
Chân em có khi mỏi mòn lê thê <br/>
Hãy trở về bên gối mộng đêm nao.