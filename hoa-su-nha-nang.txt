Đêm đêm ngửi mùi hương, mùi hoa sứ nhà nàng <br/>
Hương nồng hoa tình ái, đậm đà đây đó gọi tên <br/>
Nhà nàng cách gần bên, giàn hoa sứ quanh tường <br/>
Nhìn sang trộm nhớ thương thầm, mơ ngày mai lứa đôi <br/>
<br/>
Hôm qua mẹ bảo tôi, nhờ hoa sứ nhà nàng <br/>
Ướp trà thơm đãi khách họ hàng cô bác đều khen <br/>
Nhờ nàng hái giùm tôi, màu hoa thắm chưa tàn <br/>
Nụ hoa còn giữ nhụy vàng, Chắc nàng hiểu tình tôi <br/>
<br/>
Nhưng đêm trở sầu, em bước qua cầu <br/>
Cuộc tình tan theo bể dâu, biết chăng ngày mai <br/>
Khi ngõ về gần nhau, tình yêu đã vội phai mầu <br/>
<br/>
Đêm đêm ngửi mùi hương, mùi hoa sứ bẻ bàng <br/>
Nhà nàng với nhà tôi tình thân thiết vô vàng <br/>
Làm sao nàng nỡ phụ phàng, để tình tôi dở dang