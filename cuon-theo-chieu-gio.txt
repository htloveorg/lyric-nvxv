Một ngày nào về thăm đất mẹ <br/>
Đường về mưa bay giăng mắc lối đi <br/>
Còn tìm đâu xanh xanh hoa cỏ <br/>
Tìm đâu đêm đêm trăng tỏ <br/>
Tìm đâu tiếng ru vào nôi <br/>
<br/>
Còn tìm đâu tìm đâu mái nhà <br/>
Còn tim đâu ngôi trường cũ mến yêu <br/>
Còn tìm đâu mây buông tóc xõa <br/>
Tìm đâu em thơ nho nhỏ <br/>
Tìm đâu tuyết sương mẹ già <br/>
<br/>
Gió cuốn nước chảy lạnh lùng <br/>
Đường xưa muôn lối biết đi tìm đâu ngày thơ <br/>
Lặng nhìn mưa mưa rơi nức nở <br/>
Mây trắng xây thành buồn dâng dâng lên ngất trời <br/>
<br/>
Ngày thơ ơi đã qua mất rồi <br/>
Còn niềm tin trong lòng vẫn nở hoa <br/>
Còn dìu nhau trong cơn mưa gió <br/>
Dìu nhau trong cơn giông bão <br/>
Dìu nhau dắt nhau vào đời