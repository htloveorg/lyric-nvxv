Hòa bình ơi, <br/>
Tình yêu em như sông biển rộng. <br/>
Tình yêu em như lúa ngoài đồng. <br/>
Tình yêu em tát cạn biển đông. <br/>
<br/>
Hòa Bình ơi, ơi hòa bình ơi <br/>
Sao em nỡ lòng kẻ đợi người trông. <br/>
Sao em nỡ lòng lúa khô ngoài đồng. <br/>
Sao em nỡ lòng. <br/>
<br/>
Người về đây xin may áo cuới <br/>
Tặng người yêu vui trong lúa mới. <br/>
Tôi đón em đi về. Tôi đón em đi về. <br/>
Xây dựng lại tình quê. <br/>
<br/>
Hòa bình ơi, chờ trông nhau như con chờ mẹ <br/>
Chờ trông nhau như gió mùa hè <br/>
Chờ trông nhau nắng đẹp tình quê <br/>
Hòa bình ơi, ơi hòa bình ơi <br/>
Ba muơi tuổi đời thoát từ vành nôi <br/>
Ba muơi năm truờng khổ đau nhiều rồi. <br/>
Về đây hỡi người ơi! Về đây hỡi người ơi...!