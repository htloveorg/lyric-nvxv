Thôi là hết anh đi đường anh <br/>
Tình duyên mình chỉ bấy nhiêu thôi <br/>
Còn mong gì hình bóng xa xôi <br/>
Nhắc làm gì chuyện năm xưa <br/>
Cho tim thêm ngẩn ngơ <br/>
<br/>
Thôi là hết em đi đường em <br/>
Từ nay sầu dẫm nát tim côi <br/>
Vì sao trời đành bắt duyên em <br/>
Lỡ làng cùng người em thương <br/>
Lỡ làng cùng người em yêu <br/>
<br/>
Từ đây anh xin người yêu <br/>
Đừng oán trách hay giận hờn vì anh <br/>
Nếu trước chúng ta đừng quen <br/>
Thì thương nhớ không về trong đêm nay <br/>
<br/>
Nhiều đêm chăn gối bên người không quen biết <br/>
Trong tim em có thấy nghe cô đơn <br/>
Tại em không nói, hay tại anh không biết <br/>
Mà tình ta tan vỡ theo thời gian <br/>
<br/>
Thôi là hết, chia ly từ đây <br/>
Người phương trời kẻ sống bơ vơ <br/>
Nhiều đêm buồn về chiếu tâm tư <br/>
Nghe lòng mình còn thương ai <br/>
Nghe lòng mình còn yêu ai