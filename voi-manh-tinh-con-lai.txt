<em>nhạc Nhật<br/>
Lời Việt: Anh Khôi </em><br/>
<br/>
<br/>
Một mình anh trong đêm lẻ loi, <br/>
lòng chợt nghe vương mang bóng ai, <br/>
vòng tay yêu thương ôm nỗi đau bước đi mà hồn ở phương nào. <br/>
Đường xưa nay khơi bao xót xa, một mình anh lang thang phố khuya, <br/>
chợt nghe miên man khát khao phút giây mình bước chung đường. <br/>
Ôi yêu thương khi xưa giờ phai dấu, <br/>
giấc mơ đôi ta bên nhau nay còn đâu? <br/>
Trong hơi men say mơ hồ nỗi đau. <br/>
Bước chân em nơi xa xăm mù khơi. <br/>
Những hương nồng dĩ vãng còn đây, đắng cay lòng anh nhớ về em. <br/>
Dẫu ân tình năm tháng nhạt nhòa, với anh ân tình vẫn chưa phai. <br/>
Một người trong đêm thâu vấn vương, tìm lại nơi buông rơi ánh trăng. <br/>
Chợt bừng lên trong tim nhớ thương chốn xưa cùng ngắm trăng bên hồ. <br/>
Nhưng trăng xưa đêm nay chỉ còn làm nuối tiếc <br/>
Bước chân em qua mang theo những thầm lặng Đem bao yêu thương chôn vào dĩ vãng. <br/>
Biết em nay vui phiêu du cùng ai? <br/>
Có bao giờ em nhớ đến ngày xưa mảnh trăng đầy năm ấy còn đây, <br/>
Những men nồng ân ái vẫn hiện về bóng em mờ khuất trong đêm.