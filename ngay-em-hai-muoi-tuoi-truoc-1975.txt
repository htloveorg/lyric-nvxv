(Saigon-1961)<br/>
<br/>
Ngày em hai mươi tuổi<br/>
Tay cắt mái tóc thề<br/>
Giã từ niềm vui nhé <br/>
Buồn ơi ! Hãy chào mi !<br/>
Ngày em hai mươi tuổi<br/>
Chưa biết nhớ nhung gì<br/>
Trên nụ cười mới hé<br/>
Niềm thương đã tràn mi.<br/>
Ôi ! Ðã thoáng qua tuổi thơ <br/>
Khi suốt đêm hồn ngơ<br/>
Nghe trái tim ngủ mơ<br/>
Ôi ! Khi ánh trăng thẩn thơ<br/>
Ru giấc mơ hiền khô<br/>
Môi tiết trinh nở hoa. <br/>
Ôi ! Bỗng sáng nay đàn chim<br/>
Bay tới thương dùm em<br/>
Thương sót hoa tàn đêm<br/>
Ôi ! Nghe gió reo ngoài hiên<br/>
Mưa sẽ rơi triền miên<br/>
Sẽ hết chuyện thần tiên.<br/>
Ngày em hai mươi tuổi <br/>
Tay níu chân cuộc đời<br/>
Cho ngừng lời giăng giối<br/>
Thời gian cũng đừng trôi.<br/>
Ngày em hai mươi tuổi <br/>
Mới chớm biết yêu người<br/>
Ðã buồn vì duyên mới<br/>
Rồi đây sẽ nhạt phai.