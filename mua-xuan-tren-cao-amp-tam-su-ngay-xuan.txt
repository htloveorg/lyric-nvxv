Trời bây giờ trời đã sang Xuân <br/>
Anh và mai ngủ bên bìa rừng <br/>
Chờ giấc ba mươi mộng ảo <br/>
Mùa Xuân vẫn đẹp vô cùng <br/>
Nếu xuân này môi em còn hồng <br/>
<br/>
Tình Yêu nào chợt về đêm xuân <br/>
Ta cần nhau, gặp nhau vài lần <br/>
Nhìn én bay qua đầu núi <br/>
thì xuân đã ngập trong lòng <br/>
Thương anh vào những ngày lập đông <br/>
<br/>
Quê hương trong thời đau thương <br/>
Mùa Xuân chia ly là thường <br/>
bao nhiêu khổ nhục tủi hờn <br/>
Hát lên nhân loại <br/>
Trả buồn cho đông <br/>
<br/>
Trời bây giờ trời đã sang Xuân <br/>
Ta nhìn nhau tình yêu thành gần <br/>
Mộng ước xanh như màu cỏ <br/>
Dù bao lửa hạ đông buồn <br/>
Mong Xuân này em vẫn còn Xuân