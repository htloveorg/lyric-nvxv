Mai đây Hòa Bình. <br/>
Ta về ngắm lại dòng sông xưa. <br/>
Ðồng hoang xơ xác hai bên <br/>
Sẽ mai này thơm mùi lúa chín. <br/>
Trên sông người về <br/>
Con đò chở đầy vầng trăng quê <br/>
Hò khoan cô lái du dương <br/>
Ðón đưa người đi về chung đường <br/>
<br/>
Hòa Bình Ơi! Hòa Bình Ơi! <br/>
Hòa Bình Ơi! Hòa Bình Ơi! <br/>
Ta gánh chung đau thương một trời <br/>
Nam Bắc ơi. Quê hương, Tình người <br/>
Việt Nam Ơi! Việt Nam Ơi! <br/>
<br/>
Mai đây Hòa Bình <br/>
Con tàu chở đoàn người di cư <br/>
Về thăm đất Bắc thân yêu <br/>
Ðã xa lìa cả thời niên thiếu <br/>
Ta mê từng mùa. <br/>
Trưa Hè cánh phượng hồng lung lay <br/>
Chờ Thu ngắm lá Thu bay <br/>
Rước Xuân về vui hội Xuân đầy <br/>
<br/>
Hòa Bình Ơi! Hòa Bình Ơi! <br/>
Hòa Bình Ơi! Hòa Bình Ơi! <br/>
Ta gánh chung đau thương một trời <br/>
Nam Bắc ơi. Quê hương, Tình người <br/>
Việt Nam Ơi! Việt Nam Ơi! <br/>
<br/>
Mai đây Hòa Bình <br/>
Khung trời quê mình rộng bao la <br/>
Ðàn chim tung cánh bay xa <br/>
Bắc Nam rồi không còn ngăn cách <br/>
Hôm qua Sàigòn <br/>
Bây giờ có mặt tại Kontum <br/>
Chiều nay khăn gói ra Trung <br/>
Sớm mai này đã về Hải Phòng. <br/>
<br/>
Hòa Bình Ơi! Hòa Bình Ơi! <br/>
Hòa Bình Ơi! Hòa Bình Ơi! <br/>
Ta gánh chung đau thương một trời <br/>
Nam Bắc ơi. Quê hương, Tình người <br/>
Việt Nam Ơi! Việt Nam Ơi!