<i>(nhạc: Văn Phụng - thơ: Nguyễn Bính)</i><br/>
<br/>
<br/>
<br/>
<br/>
Sáng trăng sáng cả vườn chè <br/>
Một gian nhà nhỏ đi về có nhau <br/>
Vì tằm tôi phải chạy dâu <br/>
Vì chồng tôi phải qua cầu đắng cay <br/>
Chồng tôi thi đỗ khoa này <br/>
Bõ công kinh sử từ ngày lấy tôi. <br/>
Kẻo không rồi chúng bạn cười <br/>
Rằng tôi nhan sắc cho người say sưa <br/>
Tôi hằng khuyên sớm khuyên trưa <br/>
Anh chưa thi đỗ thì chưa (thì chưa) động phòng. <br/>
<br/>
Một quan là sáu trăm đồng <br/>
Chắt chiu tháng tháng cho chồng (mà) đi thi <br/>
Chồng tôi cỡi ngựa vinh quy <br/>
Hai bên có lính hầu đi dẹp đường <br/>
Tôi ra đón tận gốc bàng <br/>
Chồng tôi xuống ngựa cả làng ra xem. <br/>
<br/>
Đêm nay mới thật là đêm <br/>
Ai đem trăng sáng lên trên vườn chè.