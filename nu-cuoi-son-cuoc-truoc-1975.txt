Tôi nhớ mãi một chiều xuân.. chia phôi, <br/>
mây mờ buông xuống núi đồi <br/>
và trong lòng mưa hơn cả ngoài trời. <br/>
Cỏ cây hoa lá, thương nhớ mãi người đi <br/>
và dâng sầu lên mi mắt người về. <br/>
<br/>
Thơ thẩn đàn chim ngừng tiếng hót, <br/>
và mưa Xuân đang tưới luống u sầu, <br/>
buồn cho dòng nước mờ xóa bóng chim uyên <br/>
và gió chiều còn khóc thương mãi <br/>
mối tình còn vấn vương. <br/>
<br/>
Ai về sau dãy núi Kim - Bôi, <br/>
nhắn giùm tim tôi chưa phai mờ, <br/>
hình dung một chiếc thắt lưng xanh, <br/>
một chiếc khăn màu trắng trăng, <br/>
một chiếc vòng sáng lóng lánh, <br/>
với nụ cười nàng quá xinh. <br/>
<br/>
Nàng ơi, tôi đã rút tơ lòng, <br/>
dệt mấy cung yêu thương <br/>
gởi lòng trong trắng, <br/>
của mấy bông hoa rừng <br/>
đời đời không tàn với khúc nhạc lòng tôi.