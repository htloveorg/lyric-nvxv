<i>(nhạc: Võ Tá Hân - thơ: Sơn Cư)</i><br/>
<br/>
<br/>
Người từ Tây Trúc về đây ai đón một lần <br/>
Người từ Tây Trúc về đây thơm ngát thiền hương <br/>
Người đến phương đông ngoảnh mặt nhân gian <br/>
Suốt chín năm dài tịnh mặc an nhiên <br/>
<br/>
Ngồi nhìn làn mây dần bay xa hút chân trời <br/>
Từng giọt sương rơi là pháp âm diệu huyền <br/>
Có đâu ngờ một hôm tuyết phủ đầu non <br/>
Rừng thu lá dậy hải triều triền dâng <br/>
<br/>
<br/>
Ngờ đâu một sớm bình minh <br/>
Thần Quang cúi lạy mắt người lung linh <br/>
Một nụ hoa thơm nở bừng hương ngát <br/>
Một vừng trăng thanh thắp sáng hoàng hôn <br/>
<br/>
Trần gian rồi thoát tử sinh <br/>
Thiền tông pháp mầu chiếu ngời quang minh <br/>
Khuể dục si mê phải dần tan biến <br/>
Bồ Đề nhân duyên cứu vớt quần sinh <br/>
<br/>
Bồ Đề Đạt Ma thiền quang lồng lộng <br/>
Một vì sao mai chuyển hướng trời đông <br/>
Bao kiếp mong chờ thời cơ đã đến <br/>
Đèn tâm thắp dậy pháp mầu truyền trao <br/>
<br/>
Một nụ tâm khôi nở bừng sáng lạn <br/>
Một trời yêu thương tưới xuống hồn hoang <br/>
Tình chung vời vợi người về nơi đây <br/>
Một tình u mê chiếu sáng ngàn năm