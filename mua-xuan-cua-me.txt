Mẹ ơi hoa cúc hoa mai nở rồi <br/>
Giờ đây đời con đang còn lênh đênh <br/>
Đèo cao gió lộng ngày đêm bạt ngàn <br/>
Áo trận sờn vai bạc màu <br/>
Nhìn xuân về lòng buồn mênh mang <br/>
<br/>
Ngày đi con hứa xuân sau sẽ về <br/>
Mà nay đã bao xuân rồi trôi qua <br/>
Giờ đây chắc Mẹ già tóc bạc nhiều <br/>
Sớm chiều vườn rau vườn cà <br/>
Mẹ biết nhờ cậy vào tay ai? <br/>
<br/>
Đêm nay núi rừng gió nhẹ sang xuân <br/>
Thoáng mùi mai nở đâu đây <br/>
Nghe lòng lạc loài chơi vơi <br/>
<br/>
Khi xưa, những ngày binh lửa chưa sang <br/>
Bếp hồng quây quần bên nhau <br/>
Nghe Mẹ kể chuyện đời xưa <br/>
<br/>
Mẹ ơi con hứa con sẽ trở về <br/>
Dù cho, dù cho xuân đã đi qua <br/>
Dù cho én từng bầy bay về ngàn <br/>
Dẫu gì rồi con cũng về <br/>
Chỉ bên Mẹ là mùa xuân thôi