Người yêu xa tôi rồi <br/>
Còn chi mà nhớ mong <br/>
Tình yêu vỗ cánh bay, <br/>
bay xa thật xa <br/>
<br/>
Còn đâu một bóng hình <br/>
Ngày xưa hẹn ước mơ <br/>
Giờ đây đã cách xa, <br/>
xa vạn nẽo đường <br/>
<br/>
ĐK: <br/>
Yêu thương lắm cho nhiều <br/>
Rồi cay đắng muôn chiều <br/>
Đường trần không bóng anh <br/>
Hàng cây buồn thương nhớ <br/>
Nhớ ngày xưa chung đôi <br/>
<br/>
Giờ đây thôi hết rồi <br/>
Mà sao lòng vấn vương <br/>
Người ơi tôi vẫn thương, <br/>
yêu người suốt đời