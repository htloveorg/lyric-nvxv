Bao năm qua thế thôi đành thôi mình thương nhớ nhau rồi <br/>
Sau lưng kỷ niệm lạnh băng <br/>
Bước khoan chưa dừng hối tiếc chưa ngừng <br/>
Nửa tuổi đời mang theo lòng. <br/>
<br/>
Trông hôn mê giấc đau chợt nghe buồn cũ quay về <br/>
Mang chua cay với vừa tầm tay <br/>
Cuối đêm lưu đày dấu vết phơi bày <br/>
Nụ cười nước mắt còn đây. <br/>
<br/>
Đời không cho ta vui trọn vui <br/>
Em bước đi theo người <br/>
Anh bước theo một người <br/>
Hơn một lần ngoảnh mặt lại <br/>
Tưởng niệm những mối tình đi qụa <br/>
<br/>
Tay xua tay chối bỏ tình yêu dù thiết tha nhiều <br/>
Mang suy tư đến ngày nào đó <br/>
Với em tạ từ khép mắt anh nhìn <br/>
Một vùng đất tủi nằm im..... !!!!