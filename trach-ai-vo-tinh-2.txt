Ai có về bên bến sông Tương, <br/>
nhắn người duyên dáng tôi thương, bao ngày ôm mối tơ vương. <br/>
Tháng với ngày mờ nhuốm đau thương, <br/>
tâm hồn mơ bóng em luôn, mong vài lời em ngập hương. <br/>
<br/>
Thu nay về vương áng thê lương, <br/>
vắng người duyên dáng tôi thương, mối tình tôi vẫn cô đơn. <br/>
Xa muôn trùng lưu luyến nhớ em, <br/>
mơ hoài hình bóng không quên, hương tình mộng say dịu êm <br/>
<br/>
Bao ngày qua, Thu lại về mang sầu tới <br/>
Nàng say tình mới hồn tôi tơi bời, <br/>
nhìn hoa cười đón mừng vui duyên nàng <br/>
Tình thơ ngây từ đây nát tan <br/>
<br/>
Hoa ơi ! Thôi ngưng cười đùa lả lơi. <br/>
Cùng tôi buồn đắm đừng vui chi tình, <br/>
đầy bao ngày tháng dày xéo tâm hồn này lệ sầu hoen ý thu. <br/>
<br/>
Ai có về bên bến sông Tương, <br/>
nhắn người duyên dáng tôi thương, sao đành nỡ dứt tơ vương. <br/>
Ôi duyên hờ từ nay bơ vơ. <br/>
Dây tình tôi nắn cung tơ, rút lòng sầu trách người mơ...