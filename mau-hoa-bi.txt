Đâu có thể phai mờ kỷ niệm tuổi thơ <br/>
Đâu có thể phai mờ <br/>
Cho ta còn đó chút ngây thơ <br/>
Đến mãi bây giờ nhớ hoài hoa hoa Bí vàng của tuổi thơ <br/>
Nhớ hoài con bướm vàng thời mộng mơ. <br/>
<br/>
Ôi cái thuở ban đầu dệt mộng ngày xanh <br/>
Ôi cái thuở ban đầu <br/>
Bâng khuâng một mái tóc bay bay <br/>
Tóc xỏa vai gầy tóc dài hoa Bí cài vào làn mây <br/>
Bóng chiều đang xuống dần mà nào hay. <br/>
<br/>
ĐK: <br/>
<br/>
Nhớ lúc tan trường anh cùng em bắt bướm <br/>
Bướm bay vô vườn mà nước mắt rưng rưng <br/>
Anh đền em màu hoa Bí <br/>
<br/>
Hoa Bí vàng thay con bướm vàng <br/>
Nghe thương nhớ mênh mang. <br/>
<br/>
Nay Bí trổ hoa vàng về lại trường xưa <br/>
Nay Bí trổ hoa vàng <br/>
Bâng khuâng chợt nhớ sắc hoa xưa <br/>
Hoa vẫn tươi màu <br/>
Mái trường xưa có là Hoàng Hạc Lâu <br/>
Biết tìm con bướm vàng giờ ở đâu...???