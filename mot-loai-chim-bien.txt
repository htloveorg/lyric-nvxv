Kể chuyện em nghe anh em nói rằng<br/>
Biển khơi có cánh chim nhỏ xinh <br/>
Trong chiều hoang dại kéo nhau về đây<br/>
Đậu trên mũi tàu nhìn trời cao vun vút <br/>
Xanh xanh màu xanh nước trời <br/>
Để lòng anh mơ áo em ngày xưa<br/>
Thành phố xa rồi mưa có đi về <br/>
Làm người yêu đơn côi. <br/>
<br/>
Ngày đi không gian tím buồn <br/>
Nhìn nhau muốn nói nhưng lại thôi<br/>
Đôi bờ mi nhỏ chớp nhanh lệ rơi<br/>
Người yêu lính thủy nghẹn ngào khi xa cách<br/>
Bao la trời mây bốn bề<br/>
Nhìn loài chim anh nhớ thương thành đô <br/>
Mưa gió ơi đừng che kín khung trời <br/>
Làm buồn vương mắt ai. <br/>
 <br/>
Lênh đênh đi giữa muôn trùng <br/>
Nhiều khi chợt anh nhớ em<br/>
Chiều nay sao trên trời đi vắng<br/>
Gió nhiều nhưng mây buồn không bay<br/>
Sóng lên hoa sóng giăng đầy<br/>
Nhiều khi mưa vương đêm phố nhỏ<br/>
Em có ước nguyện cầu cho trăng sáng <br/>
Soi đường tàu anh đi. <br/>
<br/>
Có một loài chim hay dỗi hờn  <br/>
Loài chim biết khóc khi biệt ly <br/>
Giận anh những chiều tím mây trời bay<br/>
Thường ghen với tàu làm người yêu đi mãi <br/>
Anh ơi ngày mai lối về<br/>
Tạ từ trùng dương vấn vương lòng trai<br/>
Sau chuyến hải hành <br/>
Xa cách lâu rồi mình lại tay trong tay