Lá rơi chiều thu gieo nhiều thương nhớ <br/>
Ngày nào anh đi cho trọn ước nguyện <br/>
Chiều tàn mưa thu lạnh buốt tim tôi <br/>
Ngồi nhìn hoàng hôn vào tối <br/>
Xót xa duyên tình hai lối <br/>
<br/>
Lá thu vàng rơi mang tình yêu tới <br/>
Nụ cười thơ ngây nay đã héo tàn <br/>
Từ ngày anh đi chinh chiến quan sang <br/>
Bồi hồi mình em một bóng <br/>
Buồn theo thu tím lá vàng <br/>
<br/>
Đếm những lá rơi <br/>
Anh đi đã mấy thu rồi <br/>
Một hình bóng ấy nhớ hoài suốt đời <br/>
<br/>
Chờ một ngày mai <br/>
Đường về cùng chung hướng đời <br/>
Mình thương yêu nhau như dòng suối <br/>
<br/>
Nhớ thương người đi khi mùa thu đến <br/>
Một mùa chia phôi mang một nỗi buồn <br/>
Nhịp cầu phân ly Chức Nữ Ngưu Lang <br/>
Tìm về người yêu chẳng thấy <br/>
Thu tím lá vàng từ đây