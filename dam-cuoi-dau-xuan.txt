Ngày xưa ngày xưa đôi ta chung nón đôi ta chung đường. <br/>
Lên sáu lên năm đôi ta cùng sách đôi ta cùng trường. <br/>
Đường qua nhà em nghiêng nghiêng sân nắng, <br/>
nghiêng nghiêng mây hồng. <br/>
Chiều nao đuổi bướm, bướm bay vô vườn <br/>
mà nước mắt rưng rưng ... <br/>
<br/>
Rồi một ngày kia em khoe áo mới xanh hơn mây trời. <br/>
Hai đứa chung vui khi xuân vừa tới thơ ngây cuộc đời. <br/>
Trò chơi trẻ con em cô dâu mới chưa nghe nặng sầu. <br/>
Chú rể ngẩn ngơ ra hái hoa cà làm quà cưới cô dâu. <br/>
<br/>
Mười mấy năm qua khi hoa vừa hé nhụy <br/>
thì đời trai vui chinh chiến <br/>
Anh xuôi miền xa bao lần đếm bước xuân qua <br/>
Em ơi kỷ niệm xưa anh còn giữ mãi trong lòng <br/>
<br/>
Em biết không em, xuân nay lại trở về, <br/>
đường rừng hành quân sương xuống. <br/>
Thương sao là thương trong màu tím sắc hoa sim ... <br/>
Dĩ vãng đâu trôi về nhắc anh ngày thơ. <br/>
<br/>
Chuyện xưa chuyện xưa, <br/>
chuyện từ xuân trước xuân nay chưa nhòa <br/>
Anh nói em nghe thương em từ lúc hoa chưa mặn mà <br/>
Cầu cho mùa xuân nồng nàn trên má em tôi đợi chờ <br/>
Giữa lòng chiều hoang nâng cánh sim rừng <br/>
Ngỡ màu tím hoa xưa