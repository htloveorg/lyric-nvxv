Vừa chiều hôm nao anh với tôi đi dạo phố <br/>
Hai đứa vòng tay âu yếm như đôi tình nhân <br/>
Cười tươi như cô gái thơ ngây vui tình xuân <br/>
Chúng mình thân quá thân <br/>
Phòng trà Mỹ Trân nghe Thái Thanh ca Biệt Ly <br/>
Anh ngước nhìn tôi qua khói hương thơm café <br/>
Giọt buồn không tên lén tâm tư qua đêm đen <br/>
Mình thức đêm thật khuya <br/>
Qua ngày đó tôi nghe người nói anh lên đường xa thật rồi <br/>
Tôi buồn nhớ tim đau rặng vỡ ôi thương anh thương nhất đời <br/>
Bàng hoàng như trong cơn chim bao tôi thầm nghĩ <br/>
Non nước điêu linh yêu quê hương anh phải đi <br/>
Can đảm lên đường quên đàn em bé ngồi nhớ anh nhớ đêm trường <br/>
Từ ngày xa nhau chinh chiến đưa anh về đâu <br/>
Vai súng vượt đêm mưa nắng khe xanh rừng sâu <br/>
Người thân ai cũng nhắc tên anh trong thương yêu <br/>
Biết giờ anh trốn nào <br/>
Bạn bè của ta mỗi đứa tha phương một nơi <br/>
Khu phố ngày xưa nay vắng anh không còn vui <br/>
Và hàng cây me trút lá khô trên vai tôi <br/>
Càng nhớ thương bạn ơi