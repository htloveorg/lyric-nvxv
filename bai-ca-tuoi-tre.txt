Một nụ hoa trên mái tóc hững hờ <br/>
một tình thương cho cuộc sống đang chờ đang đợi ta <br/>
đừng vùi tương lai kiếp sa hoa <br/>
đừng vì tin yêu trong e ngại với rụt rè hỡi những người thân tuổi trẻ <br/>
<br/>
Đừng nhìn tương lai với những lo sợ <br/>
đừng nhìn tha nhân trong nỗi nghi ngờ hay dèm pha <br/>
đừng sợ chông gai vướng chân ta <br/>
đừng ngại gian lao suốt tâm hồn sáng chân người <br/>
đang trên đường dựng đời mới <br/>
<br/>
ĐK: <br/>
Đừng sợ bạn ơi hãy đứng thẳng lên ca ngợi quê hương của chúng ta <br/>
bằng niềm tin chứa chan trong tim người thanh niên <br/>
<br/>
Đừng sợ bạn ơi hãy đứng thẳng lên cuộc đời đang dang tay <br/>
đón ta bằng yêu thương ta đi xóa tan mọi căm hờn <br/>
<br/>
** <br/>
Đừng ngồi yên nghe tiếng khóc quanh mình <br/>
đừng ngồi yên trên nhung gấm vô tình hỡi bạn thân <br/>
đừng vùi lương tri dưới gót chân <br/>
đừng nhìn tha nhân đang kêu gào chống ngục tù xin công bằng <br/>
đòi cơm áo <br/>
<br/>
Đừng đùa vui khi đói khát vẫn còn <br/>
đừng đùa vui khi áp bức vẫn còn nhân loại ơi <br/>
đừng làm quê hương thêm tả tơi <br/>
đừng khoe khoang trên những xác người <br/>
đã ngã gục chết cho đời được thêm vui