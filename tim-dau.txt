Sẽ có lúc tôi về <br/>
Nhìn ngắm mây trôi trên sông thuở xưa <br/>
Tôi ngồi, hát lên ru câu tình ca <br/>
Và đâu tình yêu thuở ấy <br/>
như trăng vàng mãi trong lành <br/>
Hơi ấm đâu đây ngày xưa người ơi luôn trong tim khó phai <br/>
<br/>
Vẫn cứ mãi đi tìm, <br/>
Tìm dáng em yêu trên bao lối xưa <br/>
Đâu rồi, những đêm ta luôn gần nhau <br/>
Người ơi! Tìm đâu trong đáy nước bao hình bóng... năm nào <br/>
Thôi hết yêu thương còn đây mình anh, trở về chốn cũ <br/>
<br/>
ĐK: <br/>
Nhớ! Những phút giây êm đềm <br/>
Những con đường, lá vàng lại rơi <br/>
Mùa xuân về qua, cuộc tình vừa xa <br/>
Nhìn từng đàn chim qua, với nỗi sầu <br/>
Hỡi! Những dấu yêu xa rồi <br/>
Vẫn một đời mãi chờ đợi nhau <br/>
Đường xưa còn đây, lại còn mình anh <br/>
Nhìn từng bàn chân xưa <br/>
Em Dấu Yêu Ơi !!