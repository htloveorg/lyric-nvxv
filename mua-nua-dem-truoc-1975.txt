Đêm chưa ngủ nghe ngoài trời đổ mưa <br/>
Từng hạt rơi gác nhỏ đèn le lói bóng dáng in trên tường loang <br/>
Anh gối tay tôi để ôn chuyện xưa cũ gói trọn trong tuổi nhớ <br/>
<br/>
Tôi muốn hỏi có phải vì đời chưa trọn vòng tay <br/>
Có phải vì tâm tư dấu kín trong thư còn đây <br/>
Nên những khi mưa nửa đêm làm xao xuyến giấc mộng chưa đến tìm <br/>
<br/>
Ngoài hiên mưa tuôn mưa lạnh xuyên qua áo ai canh dài nghe bùi ngùi <br/>
Mưa lên phố nhỏ có một người vừa ra đi đêm nay <br/>
Để bao nhiêu luyến thương lại lòng tôi <br/>
<br/>
Khi trót gửi những hình ảnh của tim vào lòng đêm <br/>
Những kỷ niệm cho nhau nếu mất đi xin đừng quên <br/>
Tôi thích đi trong niềm vui và đêm rớt những giọt mưa cuối cùng <br/>
<br/>
Mưa nửa đêm mưa vào gác nhỏ <br/>
Mưa buồn mưa lạnh vào tim