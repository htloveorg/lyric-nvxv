<em>nhạc Nga</em><br/>
<br/>
Vầng dương chiếu sáng muôn tia nắng vàng <br/>
Nhè nhẹ tỏa sáng bên hồ và hàng dương liễu mến yêu này <br/>
Làm ta dịu hết nỗi buồn <br/>
<br/>
Liễu thân yêu xanh, xanh dịu dàng <br/>
Đứng bên sông nghiêng nghiêng nhẹ nhàng <br/>
Nói đi em ta nghe, đừng dối nữa, người yêu ta ở nơi nào