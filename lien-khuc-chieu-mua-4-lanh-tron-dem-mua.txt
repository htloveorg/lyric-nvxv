Mưa buồn ơi thôi ngừng tiếng <br/>
Mưa cho phố nhỏ càng buồn thêm <br/>
Mưa rơi gác xưa thêm lạnh vắng <br/>
Phòng côi lắng tiêu điều <br/>
Đường khuya vắng dìu hiu <br/>
<br/>
Đêm sầu đi trong tủi nhớ <br/>
Bao thương nhớ chỉ là mộng mơ <br/>
Đêm nay tiếng mưa rơi buồn quá <br/>
Mưa đêm sầu riêng ai <br/>
Buồn ơi đến bao giờ <br/>
<br/>
Mưa ơi! mưa ơi! <br/>
mưa gieo sầu nhân thế <br/>
Mưa nhớ ai <br/>
biết người thương có còn nhớ hay quên <br/>
Riêng ta vẫn u hoài <br/>
Đêm đêm tiếp đêm nhớ mong người <br/>
đã cách xa. <br/>
<br/>
Mưa buồn rơi rơi ngoài phố <br/>
Nghe như tiếng nhạc buồn triền miên <br/>
Đêm nao chốn đây ta dìu nhau <br/>
Trao muôn ngàn lời thơ <br/>
Chờ mong đến kiếp nào. <br/>
<br/>
Mưa trọn đêm <br/>
Mưa trọn đêm <br/>
Mưa trọn đêm