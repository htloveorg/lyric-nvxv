Em vừa nghe lời văng vắng trong đêm <br/>
Phải chăng lời ngày đó đang về tim <br/>
Nhắc cho em một thời đầm ấm <br/>
Lúc tung tăng chiều hè ngặp nắng <br/>
Lúc trăng lên bên thềm đôi má kề . <br/>
<br/>
Khi hoàng hôn về vây kín không gian <br/>
Mới hay rằng hạnh phúc đang vỡ tan <br/>
Tiếc thương cho lời nồng ngày đó <br/>
Sớm theo anh gọi người đầu ngõ <br/>
Bước chân đi, làm nát duyên ban đầu . <br/>
<br/>
<br/>
Anh mang theo giấc mơ trong đời <br/>
Vòng tay yêu không rời <br/>
Một niềm tin vừa tới <br/>
Phải không anh Tình vui khi đắm say <br/>
Đời em sao chẳng may <br/>
Mới tìm vào một lần <br/>
Mà tình đã cao bay. <br/>
<br/>
Con đường yêu nhiều giông tố hôm qua <br/>
Đã hoang tàn vì vắng chân người xa <br/>
Bóng cô đơn tràn vào ngõ tối <br/>
Tiếng mưa đêm hòa lệ tình cuối <br/>
Cố nhân ơi, lòng chết khi xa người