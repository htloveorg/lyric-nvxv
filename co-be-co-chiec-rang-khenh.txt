Này cô bé có chiếc răng khểnh <br/>
Sao thừa một cái chắc để làm duyên! <br/>
Vội vàng chi mà hình như lơ đễnh <br/>
Để lại sau lưng tiếng hót vành khuyên. <br/>
<br/>
Này cô bé sóng mắt mơ mộng <br/>
Môi cười nở thắm những đóa hồng duyên <br/>
Làm lòng anh từng đêm ngơ ngẩn <br/>
Để mộng mơ sao nhớ quá nụ cười. <br/>
<br/>
(ĐK): <br/>
Cô bé ơi! Cô bé ơi! <br/>
Lỡ gặp làm chi rồi vấn vương <br/>
Ôi nhớ thương, ôi nhớ thương <br/>
Tháng ngày ngỡ như chuyện hoang đường <br/>
<br/>
Cô bé đâu là hoa mắc cỡ <br/>
Mà sao rụt rè, nhút nhát bé ơi! <br/>
Cô bé là nụ hoa mới nở <br/>
Để đêm buồn ray rứt những vần thơ <br/>
<br/>
Này cô bé có chiếc răng khểnh <br/>
Sao thừa một cái chắc để làm duyên <br/>
Vội vàng chi mà sao tôi không biết <br/>
Để lại tôi năm tháng nhớ nhung hoài