Nắng hạ đi <br/>
mây trôi lang thang cho hạ buồn <br/>
coi khói đốt đồng, để ngậm ngùi chim nhớ lá rừng <br/>
<br/>
Ai biết mẹ buồn vui khi mẹ kêu cậu tới gần <br/>
biểu cậu ngồi mẹ nhổ tóc sâu, hai chị em tóc bạc như nhau <br/>
<br/>
Đôi mắt cậu buồn hiu phiêu lưu <br/>
rong chơi những ngày đầu chừa ba vá miếng dừa <br/>
đường mòn xưa, dãi nắng dầm mưa <br/>
<br/>
Ai cách xa cội nguồn, <br/>
ngồi một mình <br/>
Nhớ lũy tre xanh dạo quanh khung trời kỷ niệm <br/>
Chợt thèm rau đắng nấu canh <br/>
<br/>
Xin được làm mây mà bay khắp nơi giang hồ <br/>
Ghé chốn quê hương xa rời từ cất bước ly hương <br/>
Xin được làm gió dập dìu đưa điệu ca dao <br/>
Chái bếp hiên sau cũng ngọt ngào một lời cho nhau <br/>
<br/>
Xin sống lại tình yêu đơn sơ, <br/>
rong chơi những ngày đầu chừa ba vá miếng dừa <br/>
đường mòn xưa, dãi nắng dầm mưa <br/>
<br/>
Xin nắng hạ thổi buồn để mình ngồi nhớ lũy tre xanh <br/>
dạo quanh, khung trời kỷ niệm <br/>
Chợt thèm rau đắng nấu canh