(Hà Nội - 1946)<br/>
<br/>
Tiếng chân ngân dài từ từ đi ngoài đêm thâu<br/>
Tiếng chân dưới lầu, ôi tàn mộng Bích Câu <br/>
Ôi tiếng chân ai mang dài năm tháng <br/>
Ôi tiếng chân ai đã tàn tuổi thơ<br/>
Có phải đó là bước chân miệt mài của người ăn chơi <br/>
Vừa lăn ra khỏi chốn yên hoa<br/>
Mang mùi hương úa<br/>
Hay đó là người nào đi, khóc cuộc từ ly<br/>
Ðêm tối chân tần ngần<br/>
Thương bóng ai tàn Xuân <br/>
Nhưng tiếng chân sao vẫn còn rên xiết <br/>
Nhưng tiếng chân sao vẫn còn tha thiết <br/>
Vẫn còn kể lể<br/>
Vẫn còn đê mê<br/>
Vẫn còn ê chề..<br/>
Ôi bước chân nào rầu rầu<br/>
Chen tiếng mưa Ngâu<br/>
Ôi bước chân sầu<br/>
Vọng lời ai oán dân nghèo<br/>
Ôi tiếng cơ cầu<br/>
Lên với trời cao<br/>
Ôi tiếng kêu gào không mầu.<br/>
Có phải đó là bước chân ngậm ngùi của người lầm than<br/>
Âm thầm trong đêm vang lời ưu phiền <br/>
. . . . . . . .<br/>
Tiếng chân cứ dần từ từ đi vào đêm mưa.<br/>
Tiếng chân thẫn thờ đi vào hồn tôi...