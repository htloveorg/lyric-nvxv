Những chiều không có em, ngõ hồn sao hoang vắng. <br/>
Ôi! dừng chân đây, đường phố cũ, <br/>
Ngồi nhớ tới, nhớ tới dáng người em thơ <br/>
cùng bước dưới trời mưa lòng trao chuyện lòng. <br/>
Những chiều không có em, lá vàng tan tác bay, <br/>
Thường lang thang về lối cũ, <br/>
tìm dư hương ngày xưa vào hồn, <br/>
mà nhớ nuối tiếc nhớ mãi<br/>
Những chiều,những chiều tôi với em tay trong tay<br/>
<br/>
Ôi, thành đô ơi! <br/>
Ôi! thời gian như chiếc bóng, <br/>
Người đi không thấy về bến mộng. <br/>
Ôi! chiều nay chiều nay sao nhớ quá. <br/>
Giấc mơ tình yêu chưa tròn. <br/>
<br/>
Những chiều không có em, phố buồn nằm im bóng. <br/>
Ai chờ ai đây, mà bâng khuâng nhặt lấy chiếc lá úa, <br/>
Tiếc thời xuân xanh tựa chiếc lá vàng kia <br/>
khi mùa thu gọi hồn. <br/>
<br/>
Những chiều mây trắng bay, <br/>
những chiều không có em, <br/>
Người yêu ơi còn thấy nhớ gì hay không? <br/>
Từ đây một mình đành sống kiếp cô đơn âm thầm, <br/>
Âm thầm như những đêm không trăng sao.