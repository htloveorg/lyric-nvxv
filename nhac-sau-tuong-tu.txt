<i>(nhạc: Hoàng Trọng - lời: Hoàng Dương)</i><br/>
<br/>
1953 <br/>
<br/>
<br/>
 <br/>
Chiều rơi cho lòng lạc loài chơi vơi <br/>
Ngày rơi ai buồn giây phút qua rồi <br/>
Thời gian luống phụ cho ai mãi đâu <br/>
Luống hận cho ai mãi đâu <br/>
Muôn kiếp u sầu <br/>
<br/>
Chiều ơi trôi về miền nào xa xôi <br/>
Tìm ai tiếng lòng thổn thức vắn dài <br/>
Tình ơi mắt lệ chan chứa khắp nơi <br/>
Gió đừng khóc nữa gió ơi tan nát tơi bời <br/>
<br/>
 <br/>
Mây trôi bơ vơ mang theo niềm nhớ <br/>
Ánh trăng vàng úa soi bóng hình ai phương trời nào đây <br/>
Môi em thơ ngây, mái tóc vương dài <br/>
Ðôi mắt u buồn lệ thắm đêm nào ướt hoen khăn hồng <br/>
<br/>
<br/>
Vì đâu cho lòng tràn đầy thương đau <br/>
Vì đâu cho đời ta xa cách nhau <br/>
Ngày trôi xóa tình duyên cũ nghĩa xưa <br/>
Ðắm chìm theo lớp gió mưa trong cõi xa mờ