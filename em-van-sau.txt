Đây mùa Đông thứ mấy kỷ niệm mình cách xa <br/>
Em từ nơi xứ lạ nhớ Noel năm nào <br/>
Xưa nửa đêm đi lễ mình quỳ gần với nhau <br/>
Những lời hứa ban đầu còn nhớ gì không anh? <br/>
<br/>
<br/>
Từ ngày xa lìa nhau lòng em vẫn yêu tình em vẫn sâu <br/>
Vẫn ôm theo mộng ước trải khắp đường đi hẹn với ngày về <br/>
Từ mùa Đông gần đây lòng anh đã thay tình anh đã phai <br/>
Tiếng chuông ngày nào giờ đã qua nghẹn ngào hồn em xót đau. <br/>
<br/>
Đêm mùa Đông băng giá ngoài trời đầy tuyết xa <br/>
Em ngồi trong quán trọ nhớ thương anh vô bờ <br/>
Thôi còn đâu Thánh Lễ mình quỳ gần với nhau <br/>
Những lời hứa ban đầu giờ cũng thành thương đau