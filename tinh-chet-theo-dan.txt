Rừng trong đêm nghe gió vi vu <br/>
Có ai xuôi đến chốn biên thùy <br/>
Năm tháng qua lòng người tha phương <br/>
Dầu chết đi theo bụi bên đường <br/>
<br/>
Đời muôn dân say trong đảo điên <br/>
Lúc sông núi đang còn nguy biến <br/>
Ai nỡ yên mơ tình say duyên <br/>
Ngoài biên quan nghe gió chiều lên <br/>
<br/>
Có ai lạc bước <br/>
Mau về đây cùng người ngàn phương <br/>
Đời cùng sương gió <br/>
Tiếng vó câu ngàn trùng xa đưa <br/>
Đêm dần xuống, muôn bóng quân tiến binh <br/>
Tan dần mất bao lớp quân chiến chinh <br/>
Khúc ca oai hùng vang rừng núi <br/>
Mất dần sau chân đồi xa xôi <br/>
<br/>
Có ai về nhắn <br/>
Biên thùy xa chờ ngày bình yên <br/>
Lòng đừng lưu luyến <br/>
Nhớ núi sông bằng nghìn yêu thương