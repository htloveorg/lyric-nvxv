Thơ: <br/>
<i>Ai mua trăng, tôi bán trăng cho <br/>
Trăng nằm yên trên cành liễu đợi chờ <br/>
Ai mua trăng, tôi bán trăng cho <br/>
Chẳng bán tình duyên ước hẹn hò. </i><br/>
<br/>
Đường lên dốc đá nửa đêm trăng tà nhớ câu chuyện xưa <br/>
Lầu ông Hoàng đó thuở nao chân Hàn Mặc Tử đã qua <br/>
Ánh trăng treo nghiêng nghiêng, bờ cát dài thêm hoang vắng <br/>
Tiếng chim kêu đau thương, như nức nở dưới trời sương <br/>
Lá rơi rơi đâu đây sao cứ ngỡ bước chân người tìm về giữa đêm buồn <br/>
<br/>
Đường lên dốc đá nhớ xưa hai người đã một lần đến <br/>
Tình yêu vừa chớm xót xa cho chàng cuộc sống phế nhân <br/>
Tiếc thay cho thân trai, một nửa đời chưa qua hết <br/>
Trách thay cho tơ duyên chưa thắm nồng đã vội tan <br/>
Hồn ngất ngây điên cuồng cho trời đất cũng tang thương, mà khổ đau niềm riêng. <br/>
<br/>
Hàn Mạc Tử xuôi về quê cũ, giấu thân nơi nhà hoang <br/>
Mộng Cầm hỡi thôi đừng thương tiếc, tủi cho nhau mà thôi <br/>
Tình đã lỡ xin một câu hứa, kiếp sau ta trọn đôi <br/>
Còn gì nữa thân tàn xin để một mình mình đơn côi.<br/>
<br/>
Tìm vào cô đơn đất Quy Nhơn gầy đón chân chàng đến <br/>
Người xưa nào biết, chốn xưa ngập đường pháo cưới kết hoa <br/>
Chốn hoang liêu tiêu sơ Hàn âm thầm nghe trăng vỡ <br/>
Khóc thương thân bơ vơ, cho đến một buổi chiều kia <br/>
Trời đất như điên cuồng khi hồn phách vút lên cao <br/>
Hàn Mặc Tử nay còn đâu? <br/>
<br/>
Trăng vàng ngọc, trăng ân tình chưa phỉ <br/>
Ta nhìn trăng, khôn xiết ngậm ngùi chăng.