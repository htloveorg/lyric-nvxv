[Nhạc: Tôn Nữ Đồng Khánh - Lời: Phạm Nhuận]<br/>
<br/>
<br/>
Buổi sáng chim bay về biển tây <br/>
Ta đi trời bạt gió sông đầy <br/>
Chiều hôm con vạc về bên núi <br/>
Ta cũng mềm theo hạt sương vay <br/>
Chân bước chậm hay dốc đồi núi mỏi <br/>
Cứ lên cao xem ngọn gió vô thường <br/>
Ôi quá hẹp những con đường trầm ngãi <br/>
Trầm tích chi trong tiếng vạc kêu sương <br/>
<br/>
Thôi xếp lại mơ non cùng ước bể <br/>
Bờ bến xa xin hẹn lại một ngày <br/>
Sông vốn hẹp nhưng đời cho ta thế <br/>
Càn khôn ơi xin rót rượu giùm ngay <br/>
<br/>
Ta đã qua sông vào lần nhì <br/>
Nhớ chăng mùa hạ rớt đâu đây <br/>
Ô kìa sông sao sương mù thế <br/>
Ừ nhỉ đông sang đã mấy ngày