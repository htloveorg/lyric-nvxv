Một thời quá khứ xa xôi lòng còn tiếc nhớ khôn nguôi <br/>
Một thời áo trắng bên kia nhìn về rưng rưng một trời <br/>
Tình đầu với những đêm mưa nằm chờ tiếng dế bên hiên <br/>
Áo trắng thư sinh, và lòng giây trắng tinh nguyên <br/>
Một thời áo trắng ngây thơ đời còn quá đỗi đơn sơ <br/>
Tình đầu vẫn tiếng rao khuya hàng quà thơm ngon từng mùa <br/>
Tình đầu chớm biết yêu em từ độ mái tóc chia ngôi <br/>
Bỡ ngỡ tinh khôi phút chốc xoa tay vào đời <br/>
<br/>
Em còn nhớ không em trong ta một thời áo trắng <br/>
Mỗi ngày thắm thân yêu xôn xao kỷ niệm xa gần <br/>
Em còn nhớ em đi đêm xưa lòng thuyền sướt mướt <br/>
Khi bờ bến sau lưng còn ngập dấu chân <br/>
<br/>
<br/>
Dòng đời cứ ngỡ hôm qua nhìn lại ngót mấy mươi năm <br/>
Một thời áo trắng xa xưa chợt về nghe em gọi thầm <br/>
Mộng đời réo bước chân đi mộng tình réo phút chia phôi <br/>
Tiếc nhớ khôn nguôi áo trắng xa ta đời đời <br/>
<br/>
Từ độ áo trắng phôi pha thành vòng ngũ sắc lung linh <br/>
Dòng lệ nối tiếp trôi theo dòng đời, trôi theo cuộc tình <br/>
Vì đời sớm bước chân đi, vì tình sớm biết chia phôi <br/>
Tiếc nhớ khôn nguôi áo trắng xa ta đời đời ... <br/>
Vì đời sớm bước chân đi, vì tình sớm biết chia phôi <br/>
Tiếc nhớ khôn nguôi áo trắng xa ta đời đời