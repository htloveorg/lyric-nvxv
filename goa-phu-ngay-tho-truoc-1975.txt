Đà Lạt lạnh môi em vừa đủ ấm <br/>
Đời chia ly nên đẹp chuyện tương phùng <br/>
Con dốc nhỏ hoa anh đào lấm tấm <br/>
Lối sỏi mòn hai đứa đã đi chung <br/>
Em nhẩm tính trên lóng tay tháp bút <br/>
Là cách xa biền biệt tháng năm trôi <br/>
Tuổi trẻ ơi sao quá nhiều nước mắt <br/>
Chiến tranh ơi bóng tương lai mịt mù <br/>
<br/>
Đơn xin cưới, một tờ đơn xin cưới <br/>
Anh thảo rồi, anh lại xé anh ơi <br/>
Bởi không muốn thấy người yêu nhỏ bé <br/>
Một sớm nào, thành góa phụ ngây thơ <br/>
Nên đơn cưới, một tờ đơn xin cưới, <br/>
Anh viết rồi, rồi anh lại xé em ơi <br/>
<br/>
Em đoán thấy trong hố sâu quầng mắt <br/>
Từng xác đêm chồng chất nỗi theo nhau <br/>
Trong tình yêu em thiệt thòi nhiều nhất <br/>
Em có gì cho mộng ước mai sau <br/>
Đơn xin cưới, một tờ đơn xin cưới <br/>
Anh viết rồi, anh viết rồi, sao lại xé anh ơi <br/>
<br/>
Thượng Đế xa vời, thiên đàng đóng cửa <br/>
Tiếng cười chưa tan, nước mắt ròng ròng <br/>
Số phận con người , đồng tiền sấp ngửa <br/>
Em, em ơi, em có hiểu gì không <br/>
Nên đơn cưới, một tờ đơn xin cưới <br/>
Anh viết rồi, rồi anh lại xé em ơi <br/>
<br/>
Không, anh không muốn thấy người yêu anh nhỏ bé <br/>
Một sớm nào thành góa phụ ngây thơ