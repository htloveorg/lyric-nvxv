Chiều đã tắt nhạc buồn héo hắt <br/>
Ðường phố vắng đèn vàng héo úa <br/>
Lặng lẽ bước chập chờn bóng tối <br/>
Bước chân âm thầm ai đến <br/>
Tình vừa mới đó lời nào mới hứa <br/>
Mà đã nỡ vội vàng gió cuốn <br/>
Tình đã mất một trời tiếc nuối <br/>
Một đời buồn mãi không nguôi <br/>
<br/>
Tình yêu vẫn mãi ngàn đời chập chờn <br/>
Ngày vui quá ngắn cuộc đời chẳng dài <br/>
Ðời ta sau mãi lạc loài tìm hoài hạnh phúc nơi nào <br/>
Người yêu dấu hỡi còn đợi chờ gì <br/>
Mùa xuân sẽ hết cuộc tình rồi tàn <br/>
Ngày nào tha thiết giờ thành muộn màng tình đã bay xa <br/>
<br/>
Anh hỡi anh đang nơi nao hay chăng có em đợi chờ <br/>
Bao đêm cô đơn lạnh lùng <br/>
ngồi chờ bóng ai phương trời phiêu lãng <br/>
Anh ơi xa xăm mà chi mà còn mãi chưa quay về <br/>
Ðêm xuân âm thầm lạnh lùng qua đi đâu chờ ta mãi <br/>
<br/>
Người yêu dấu hỡi còn đợi chờ gì <br/>
Ngày vui quá ngắn cuộc đời còn dài <br/>
Ðời ta sau mãi lạc loài tìm hoài hạnh phúc nơi nào <br/>
Tình yêu như bóng mây bay về đâu <br/>
Tình yêu như cánh chim trong mù khơi <br/>
Ngày nào tha thiết giờ thành muộn màng tình đã bay xa...