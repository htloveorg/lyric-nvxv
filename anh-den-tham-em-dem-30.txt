[Lời: Nguyễn Ðình Toàn - phổ nhạc: Vũ Thành An]<br/>
<br/>
<br/>
Em đến thăm anh đêm ba mươi <br/>
Còn đêm nào vui bằng đêm ba mươi <br/>
Anh nói với người phu quét đường <br/>
Xin chiếc lá vàng làm bằng chứng yêu em. <br/>
<br/>
Tay em lạnh để cho tình mình ấm <br/>
Môi em mềm cho giấc ngủ em thơm <br/>
Sao giao thừa xanh trong đôi mắt ngoan <br/>
Trời sắp Tết hay lòng mình đang Tết. <br/>
<br/>
Tháng ngày đã trôi qua <br/>
Tình đã phôi pha <br/>
Người khuất xa <br/>
Chỉ còn chút hương xưa <br/>
Rồi cũng phong ba <br/>
Rụng cùng mùa. <br/>
<br/>
Dòng sông đêm <br/>
Hồn đen sâu thao thức <br/>
Ngàn vì sao mọc <br/>
Hay lệ khóc nhau <br/>
Đá buồn chết theo sau <br/>
Ngày vực sâu <br/>
Rớt hoài xuống hư không <br/>
Cuộc tình đau.