Vùng ngoại ô, tôi có căn nhà tranh <br/>
Tuy bé nhưng thật xinh <br/>
Tháng ngày sống riêng một mình <br/>
Nhà gần bên, em sống trong giàu sang <br/>
Quen gấm nhung đài trang <br/>
Đi về xe đón đưa <br/>
<br/>
Đêm đêm dưới ánh trăng vàng <br/>
Tôi với cây đàn âm thầm thở than <br/>
Và cô nàng bên xóm <br/>
Mỗi lúc lên đèn, sang nhà làm quen <br/>
<br/>
ĐK: <br/>
Tôi ca không hay tôi đàn nghe cũng dở <br/>
Nhưng nàng khen nhiều và thật nhiều <br/>
Làm tôi thấy trong tâm tư xôn sao <br/>
Như lời âu yếm mặn nồng <br/>
Của đôi lứa yêu nhau <br/>
<br/>
Hai năm trôi qua, nhưng tình không dám ngỏ <br/>
Tôi sợ thân mình là bọt bèo <br/>
Làm sao ước mơ tơ duyên mai sau <br/>
Tôi sợ ngang trái làm phận đời <br/>
Chua xót thương đau <br/>
<br/>
Rồi một hôm tôi quyết đi thật xa <br/>
Tôi cố quên người ta <br/>
Những hình bóng trong xa mờ <br/>
Nhờ thời gian, phương thuốc hay thần tiên <br/>
Chia cách đôi tình duyên <br/>
Nên người xưa đã quên <br/>
<br/>
Hôm nay đón cánh thiệp hồng <br/>
Em báo tin mừng lấy chồng giàu sang <br/>
Đời em nhiều may mắn <br/>
Có nhớ anh nhạc sĩ nghèo này không?