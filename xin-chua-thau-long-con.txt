Chúa ngự ở trên cao <br/>
Lòng trời bể bao la <br/>
Ngày lớn con đã tin rằng <br/>
Khi lòng khổ đau <br/>
Tìm đến Chúa ban an lành <br/>
<br/>
Lạy Chúa mùa Giáng Sinh xưa <br/>
Ngày đầu vướng yêu đương <br/>
Người ấy hứa với con rằng <br/>
Tan mùa chiến chinh <br/>
Ngày đó anh về kết đôi <br/>
<br/>
Nhưng từng đêm chiếc lá lìa ngàn <br/>
Đêm từng đêm giấc mơ kinh hoàng <br/>
Nhân loại còn ngủ say bên những <br/>
Kiếp người quê hương đọa đầy <br/>
<br/>
Ôi đêm thánh vô cùng <br/>
Lời thương dệt khắp muôn trùng <br/>
Người mau về đi đừng gieo biệt ly <br/>
Từng hồi chuông nửa đêm sầu bi <br/>
<br/>
Chiến cuộc mấy mươi năm <br/>
Mảnh trời Bắc gian truân <br/>
Lạy Chúa chinh chiến lâu rồi <br/>
Cho mùa giáng sinh này đến thanh bình <br/>
Chúa ơi ... <br/>
<br/>
Lạy Chúa chinh chiến lâu rồi <br/>
cho người núi sông rũ áo tang bồng Chúa ơi !