Em có nhớ bài ca dao đầu đời <br/>
Bên chiếc võng mẹ ru ầu ơ hờ <br/>
Hàng dừa xanh soi mình bên nương dâu <br/>
Sông dài quê nghèo quấn quýt với nhau <br/>
<br/>
Em đã lớn dần trên quê hương mình <br/>
Trong chiếc áo bà ba đượm thắm tình <br/>
Ngọt ngào thơm hương mạ non sáng sớm <br/>
Xa mấy xa hương hạt gạo thơm <br/>
<br/>
<br/>
Bao năm rồi từ lúc em đi <br/>
Mẹ trông chờ em hoài mà chẳng thấy <br/>
Mâm cơm chiều bên mái tranh xưa, <br/>
Con nước lên nghe tiếng bìm bịp kêu chiều <br/>
Thương đôi mái trèo khua đêm trăng mờ <br/>
Thương ai vẫn từng đêm đêm ngóng chờ <br/>
Đành đoạn sao quên lời thề năm xưa <br/>
Nay giòng sông buồn heo hút gió mưa <br/>
<br/>
Em có nhớ lời quê hương đậm đà. <br/>
Trong bao tháng ngày tha phương vắng nhà <br/>
Kỷ niệm xưa êm đềm tha thiết quá <br/>
Như giấc mơ ru đẹp đời ta