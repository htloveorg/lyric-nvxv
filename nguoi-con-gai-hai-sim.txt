Chuyện người con gái hái sầu riêng nhà nghèo <br/>
Nàng là trinh nữ tuổi tình yêu vừa lớn <br/>
Nhiều chàng trai tráng đến vườn chơi muốn cầu hôn <br/>
Mong được em trao tim vàng, nhung lòng nàng vẫn không màn <br/>
<br/>
Rồi chàng ca sĩ ghé làm quen một chiều <br/>
Nhìn chàng cô gái thoáng hồng lên bờ má <br/>
Nõn nà tay nhỏ áo bà ba mắt hồn nhiên <br/>
Ô kìa em mang tim vàng cho người ca sĩ hào hoa <br/>
Ai xui cô gái nhà quê, quen anh ca sĩ làm chi <br/>
Yêu lầm, yêu lỡ câu ca cung đàn <br/>
<br/>
Tình chàng ca sĩ tài danh, <br/>
Như loài ong bướm loanh quanh <br/>
Ai xui cay đắng hồng nhan <br/>
Hỡi cô em hái sầu riêng, mang sầu riêng bán <br/>
Cớ sao riêng sầu, riêng sầu say trái trong tim <br/>
Sao nàng không bán dễ yên <br/>
Hận chàng ca sĩ đã làm đau lòng người <br/>
Một lần gãy đã thấm vào tim đắng mật thêm <br/>
Bao chiều ngồi nhìn qua đồi, trong vườn em vẫn riêng sầu <br/>
Rồi ngày mưa gió có một đêm thật buồn <br/>
Rằng người con gái tuổi tròn trăng đã mất <br/>
Buồn tình trong lúc hái sầu riêng lỡ trượt chân <br/>
Em tựu sao băng sao mùa, ôm tình theo giấc ngàn thu