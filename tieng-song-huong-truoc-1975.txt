Miền Trung vọng tiếng, em xinh em bé tên là Hương giang,<br/>
đêm đêm khua ánh trăng vàng mà than. <br/>
Hò ơi, phiên Đông Ba buồn qua cửa chợ, <br/>
bến Vân Lâu thuyền vó đơm sâu. <br/>
Hỡi hò, hỡi hò. <br/>
Quê hương em nghèo lắm ai ơi, <br/>
mùa đông thiếu áo hè thì thiếu ăn. <br/>
Trời rằng, <br/>
trời hành cơn lụt mỗi năm à ơi, <br/>
khiến đau thương thấm tràn, <br/>
lấp Thuận An để lan biển khơi, <br/>
ơi hò ơi hò.<br/>
<br/>
<br/>
Hò ơi...... Ai là qua là thôn vắng, <br/>
nghe sầu như mùa mưa nắng cùng em xót dân lều tranh chiếu manh.<br/>
Hò ơi...... Bao giờ máu xương hết tuôn tràn, <br/>
quê miền Trung thôi kiếp điêu tàn cho em vang khúc ca nồng nàn.<br/>
<br/>
<br/>
Ngày vui, tan đao binh, mẹ bồng, con sơ sinh, <br/>
chiều đầu xóm xôn xao đón người trường chinh. <br/>
Ngậm ngùi hân hoan tiếng cười đoàn viên .<br/>
<br/>
<br/>
<br/>
Bài này là một các ca khúc của trường ca "Hội Trùng Dương" sáng tác trong thập niên 60: <br/>
<br/>
- Tiếng Sông Hồng <br/>
- Tiếng Sông Hương <br/>
- Tiếng Sông Cửu Long.