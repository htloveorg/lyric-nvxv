1. <br/>
Tôi trở về đây lúc đêm vừa lên <br/>
Giăng mắt trời mưa phố xưa buồn tênh <br/>
Gót mòn tìm dư hương ngày xưa <br/>
Bao nhiêu kỷ niệm êm ái <br/>
Một tình yêu thoát trên tầm tay <br/>
<br/>
2. <br/>
Tôi trở về đây với con đường xưa <br/>
Đâu bóng người thương cố nhân về đâu? <br/>
Tiếng buồn chợt đâu đây vọng đưa <br/>
Công viên lạnh lùng hoang vắng <br/>
Ngọn đèn đêm đứng im cúi đầu <br/>
<br/>
Điệp khúc <br/>
Thu đến thu đi cho lá vàng lại bay <br/>
Em theo bước về nhà ai <br/>
Ân tình xưa đã lỡ <br/>
Thời gian nào bôi xóa <br/>
Kỷ niệm đầu ai đành lòng quên? <br/>
<br/>
3. <br/>
Phố buồn mình tôi bước chân lẻ loi <br/>
ray rứt trời mưa bỗng nghe mặn môi <br/>
Nỗi niềm chuyện tâm tư người ơi <br/>
Xin ghi nhạc lòng thương nhớ <br/>
Mình gọi nhau cố nhân u sầu.