<em>Thơ: Nguyễn Tất Nhiên</em><br/>
<br/>
Vì tôi là linh mục <br/>
Không mặc chiếc áo dòng <br/>
Nên suốt đời hiu quạnh <br/>
Nên suốt đời lang thang <br/>
<br/>
Vì tôi là linh mục <br/>
Có được một tín đồ <br/>
Nhưng không là thánh thần <br/>
Nên tín đồ đi hoang <br/>
<br/>
Vì tôi là linh mục <br/>
Giảng lời tình nhân gian <br/>
Nên không còn tiếng khóc <br/>
Nên không còn tiếng trách <br/>
Nên không biết kêu than <br/>
Nên tôi rất bơ vơ <br/>
Nên tôi rất dại khờ <br/>
<br/>
Vì tôi là linh mục <br/>
Không mặc chiếc dòng <br/>
Nên suốt đời hiu quạnh <br/>
Nên suốt đời lang thang <br/>
<br/>
Vì tôi là linh mục <br/>
Có được một tín đồ <br/>
Nhưng không là thánh thần <br/>
Nên tín đồ đi hoang <br/>
<br/>
Vì tôi là linh mục <br/>
Tưởng đời là hạnh phúc <br/>
Nên tin lời thiếu nữ <br/>
Như tin vào Đức Chúa <br/>
Câu kinh sớm chưa yêu <br/>
Câu kinh tối chưa mê <br/>
Vẫn mất mát ê chề <br/>
<br/>
Mất vì tin tín đồ là người tình <br/>
Có ngờ đâu người tình là ác quỷ <br/>
Ác quỷ đầy quyền năng <br/>
Giam tôi trong tín đồ <br/>
Tín đồ là người tình <br/>
<br/>
Người tình bỏ tôi đi <br/>
Thiêu hủy lòng tin si <br/>
Người tình bỏ tôi đi <br/>
Thiêu hủy lời kinh xưa <br/>
Người tình bỏ tôi đi <br/>
Giáo đường buồn lê thê <br/>
Lời chia xa ... <br/>
<br/>
Vì tôi là linh mục <br/>
Chưa rửa tội bao giờ <br/>
Nên âm thầm qua đời <br/>
Tội ác còn trong tôi <br/>
<br/>
<br/>
Vì tôi là linh mục <br/>
Chưa rửa tội bao giờ <br/>
Nên âm thầm qua đời <br/>
Tội ác còn trong tôi <br/>
<br/>
Vì tôi là linh mục <br/>
Vì tôi là linh mục <br/>
Người ơi một linh mục <br/>
Rất  dại khờ