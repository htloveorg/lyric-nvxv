Sẽ không bao giờ em phụ tình anh <br/>
Sẽ không bao giờ em dối lòng mình <br/>
Thà rằng không yêu thì thôi <br/>
Đã nguyện yêu thương nhau rồi <br/>
Em xin thề trọn đời chung đôi <br/>
<br/>
Sẽ không bao giờ em làm buồn anh <br/>
Sẽ không bao giờ lạc nẽo đường tình <br/>
Lòng nào anh tin người ta <br/>
Trách hờn chi em bao lời <br/>
Tội nghiệp em oán lắm người ơi <br/>
<br/>
Bên nhau đôi ta như cây liền cành <br/>
Dịu dàng như chim chung cánh <br/>
Chan hòa như bướm vờn quanh <br/>
Mỗi hơi thở này dành riêng anh <br/>
Mỗi nhịp đập tim em là riêng anh <br/>
Còn chi đâu san sẻ cho người <br/>
<br/>
Biết rằng anh hờn ghen chỉ vì yêu <br/>
Biết rằng yêu nhiều nên ghen hờn nhiều <br/>
Đã đành nhưng xin anh hiểu <br/>
Bởi vì yêu anh vô cùng <br/>
Trời cho em mang tính thủy chung