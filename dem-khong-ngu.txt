Bao đêm không ngủ gối chiếc đã hoen lệ sầu. <br/>
Bao đêm không ngủ tiếng khóc xót thuơng tìm nhau <br/>
Anh ơi đi đâu phuơng trời nào có thấu <br/>
Những nỗi đắng cay buồn đau cho tình ban đầu. <br/>
Bên song mưa đổ chiếc bóng nhớ thuơng nhiều rồi. <br/>
Tim em đau khổ biết nói với riêng mình thôi <br/>
Canh khuya đơn côi quanh mình là bóng tối <br/>
Nước mắt đã bao lần rơi dòng đời vẫn trôi. <br/>
Anh ơi! Bao giờ anh về, <br/>
Bao giờ anh về nghe tiếng nói dịu êm. <br/>
Anh ơi! Chim kia tìm tổ ấm. <br/>
Nước vẫn trôi về nguồn sao nỡ để em buồn. <br/>
Cho em hơi thở ấm cúng những khi lạnh lùng <br/>
Cho môi em nở thắm thiết giữa đêm trời đông <br/>
Đôi ta trao nhau huơng đời và ý sống <br/>
Nối tiếc với bao ngày xanh đừng phụ nhé anh