Buồn ơi sao còn đến <br/>
Lòng đã quá sầu đau <br/>
Ai đã đem chuyện xưa <br/>
Lời hứa ban đầu chôn kín vào tim . <br/>
<br/>
Ngày xưa nay còn đâu <br/>
Tình xưa biết tìm đâu <br/>
Thôi đón đưa mà chi <br/>
Tình đã không thành <br/>
Còn ước mơ gì. <br/>
<br/>
<br/>
Ôi mấy năm rồi <br/>
Đã mòn mà sâu chưa nguôi <br/>
Tiếc nhớ từ đây <br/>
Người ơi vĩnh biệt là ngàn đời. <br/>
<br/>
Xe hoa đưa người <br/>
Đến nơi mộ chôn bao nhiêu ân hương tình cũ <br/>
Hương xưa người mang về dâng cho tình mới <br/>
Ái ân chưa tròn nhưng đã sầu chia phôi. <br/>
<br/>
Chiều nay ôn chuyện cũ <br/>
Mà thương tiếc ngày qua <br/>
Thôi kiếp sau gặp nhau <br/>
Giờ đã muộn màng <br/>
Chỉ biết lỡ làng