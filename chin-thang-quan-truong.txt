Tuổi thư sinh gối mộng đăng trình <br/>
Vui buớc quân hành dọc ngang đời lính <br/>
Ba tháng quân trường mồ hôi đổ <br/>
Ngày đầu tiên bỡ ngỡ tay súng với nhịp đi <br/>
Lâu dần rồi chẳng khó khăn chi <br/>
Chín mươi hai ngày lẻ, bạn bè vui vẻ <br/>
Còn một đêm nay trước phút chia tay <br/>
Ôi tâm sự vơi đầy <br/>
<br/>
Rời Quang Trung đôi ta cùng nhau nguyện <br/>
Dù đường đời mỗi đứa cách một nơi <br/>
Nẻo ấy nếu có xa xôi <br/>
Nhớ mang theo một lời <br/>
Nguyền mến thương nhau hoài <br/>
<br/>
Ngày mai đây trên con đường xuôi ngược <br/>
Bạn về miền Trung tôi ở miền Tây <br/>
Thế gian dù có đổi thay <br/>
Tình ta vẫn đẹp như ngày đầu tiên