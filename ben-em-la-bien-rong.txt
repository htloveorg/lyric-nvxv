* <br/>
Tình ơi sao đi mãi nên sông dài mênh mông <br/>
Bàn tay ôm nỗi nhớ xôn xao biển rộng <br/>
Vì em mất anh mất anh <br/>
Mùa xuân đã qua rất nhanh <br/>
Còn chăng nỗi đau nỗi đau tuổi xanh <br/>
** <br/>
Tình anh như cơn lũ cuốn đôi bờ mưa giông <br/>
Tình em như sông vắng trong xanh phẳng lặng <br/>
Mùa thu đã qua đã qua <br/>
Mùa đông đã sang đã sang <br/>
Tình đã ra đi vội vàng <br/>
<br/>
*** <br/>
Khi anh xa em, sóng thôi không xô bờ <br/>
Khi em xa anh, đá bơ vơ <br/>
Con sông lang thang đã khô nơi đầu nguồn <br/>
Bên em bên em, biển đã chết <br/>
<br/>
(Repeat **) <br/>
<br/>
(Repeat from * to ***) <br/>
<br/>
(Repeat ** and *** and **)