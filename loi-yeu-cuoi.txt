Nhớ anh nhiều và thương thật nhiều <br/>
Từ khi đời ngăn cách đôi ta <br/>
Đêm mưa buồn nhìn lá bay xa <br/>
Viết cho nhau những lời yêu cuối <br/>
Viết cho nhau để giết u hoài <br/>
Đến bên nhau bằng giấc mơ dài <br/>
<br/>
Sao con đường tình nhiều cay đắng <br/>
Qua một lần đã biết tương tư <br/>
Và lần này là lần cuối <br/>
Ngày tình đang lên khơi <br/>
Có thấy chi lừa dối <br/>
Có biết đâu tình yêu xa vời <br/>
Ngỡ trong tay nhưng đã vội bay. <br/>
<br/>
Tiếng kinh buồn chìm trong sương chiều <br/>
Lời nguyện cầu tha thiết tim côi <br/>
Xin thương người vừa đã quên tội <br/>
Tiếng chuông ngân trong mùa hoa cưới <br/>
Chúa ơi con khổ đến muôn đời <br/>
Giúp cho con về dưới chân người.