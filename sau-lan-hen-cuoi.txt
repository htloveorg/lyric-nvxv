Sau lần hẹn cuối, em về với người <br/>
Còn lại anh suy tư mãi tình đời <br/>
Thương quá để rồi, yêu quá để rồi <br/>
Thành nụ cười khô héo trên môi. <br/>
<br/>
Sau lần hẹn cuối, em về với chồng <br/>
Ðể mặc anh ôm kỷ niệm vào lòng <br/>
Em đã đi rồi, em đã xa rồi <br/>
Mà tình này vẫn sống trong tôi. <br/>
<br/>
Đường nào vào yêu không mang nhiều nhớ nhung <br/>
Ðường nào vào yêu không vương vấn đợi chờ <br/>
Tình nào vào mơ không trở thành dang dở <br/>
Không nức nở em ơi... <br/>
<br/>
Sau lần hẹn cuối, nghe lòng nhớ nhiều <br/>
Tìm vào mơ đau thương cả một chiều <br/>
Duyên kiếp không thành, hai đứa thôi đành <br/>
Nhìn cảnh đời ngăn cách đôi nơi .