Ngày xưa khi còn bé <br/>
Tôi yêu quá cuộc đời <br/>
Tôi yêu thương loài người <br/>
Ngồi vẽ lấy tương lai <br/>
<br/>
Ngày xưa khi còn bé <br/>
Giữa chói chang trưa hè <br/>
Dưới lũ mưa đông về <br/>
Lòng tôi xao xuyến quá <br/>
<br/>
Ngày xưa khi còn bé <br/>
Tôi mơ có cuộc tình <br/>
Như mơ ước được gần với những nụ hồng <br/>
<br/>
Nhưng hôm nay không còn trẻ nhỏ như xưa <br/>
Tôi thấy tôi là chiếc bóng phai mờ <br/>
Nhưng hôm nay không còn một hồn bao la <br/>
Tôi thấy tôi là chút vết mực nhoè <br/>
<br/>
Ngày nay không còn bé <br/>
Tôi quên sống thật thà <br/>
Tôi đã không còn là <br/>
Là hạnh phúc ngu ngơ <br/>
<br/>
Ngày nay sao buồn thế <br/>
Những sáng hay đêm về <br/>
Vẫn thấy rất ơ hờ <br/>
Bình yên như kiếp đá <br/>
<br/>
Ngày nay thôi đành nhé <br/>
Tôi như đá nặng nề <br/>
Trong giây phút tình cờ rớt xuống mịt mù ...