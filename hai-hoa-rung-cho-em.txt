Hái trộm hoa rừng về trao một người ngày xưa anh đã hứa <br/>
Màu hoa kỷ niệm tuy đã tàn úa, tâm tư vẫn dạt dào <br/>
Năm xưa lối này vẫn thường dìu em đi <br/>
Tàn cây năm trước anh viết tên em <br/>
Còn xanh lá cành, ghi dấu kỷ niệm những ngày còn bên nhau <br/>
Ghế lạnh đá buồn, để em đợi chờ chiều xa xưa năm ấy <br/>
Vì anh lỗi hẹn cho em hờn dỗi, cho mi em nhạt nhòa <br/>
Anh ra đi rồi mới nhận được tin nhau <br/>
Hàng trăm thư viết anh gởi cho em <br/>
Ðời trai chiến trận, em biết lỗi hẹn có phải tại anh đâu <br/>
<br/>
Tiền đồn heo hút, <br/>
Tinh tú quây quần nghe anh kể chuyện đời lính <br/>
Khi nao lỗi hẹn, em anh dỗi hờn lòng anh thêm xao xuyến <br/>
Ngày đi anh chẳng được gặp em <br/>
Nhưng hứa sẽ tìm về tặng em đóa hoa yêu <br/>
Ðã bao lâu rồi cành hoa úa tàn mà ước mơ chưa tàn <br/>
<br/>
Biết chuyện chúng mình, <br/>
tình không nhạt nhòa mà xui cho ngăn cách <br/>
Chiều nay trở lại, trong tay mình nói: Ta yêu nhau trọn đời <br/>
Anh đi em chờ, gối mộng dệt đêm thâu <br/>
Cành xưa anh viết anh khắc tên em <br/>
Chiều nay kết nhụy cho thắm men tình những người còn đi xa