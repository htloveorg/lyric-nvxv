Trời đêm dần tàn tôi đến sân ga <br/>
Đưa tiễn người trai lính về ngàn <br/>
Cầm chắc đôi tay ghi vào đời tâm tư ngày nay <br/>
Gió khuya ôi lạnh sao, ướt nhẹ đôi tà áo <br/>
<br/>
Tàu xa dần rồi, thôi tiếc thương chi khi biết người ra đi vì đời <br/>
Trở gót bâng khuâng, tôi hỏi lòng đêm nay buồn không, <br/>
chuyến xe đêm lạnh không <br/>
Để người yêu vừa lòng <br/>
<br/>
Đêm nay lặng nghe gió lùa qua phố vắng, <br/>
Trăng rằm về xa xăm <br/>
Trong giây phút này, tôi mơ ước sao nắm trọn vào tay nhau <br/>
<br/>
Ngày tháng đợi chờ, tôi đến sân ga nơi tiễn người trai lính ngày nào <br/>
Tàu cũ năm xưa mang người tình biên khu về chưa? <br/>
Trắng đêm tôi chờ nghe tiếng tàu đêm tìm về <br/>
<br/>
Dù xa vời vợi <br/>
Tôi vẫn tin anh trên bước đường tha hương còn dài <br/>
Nợ nước hai vai anh phải trả tương lai ngày mai<br/>
Nhớ thư anh hẹn tôi, sẽ về thăm một tối <br/>
Dù câu chuyện đời e ấp trên môi<br/>
Đêm ước hẹn cho nhau nụ cười <br/>
Hình bóng thương yêu <br/>
Anh để vào tâm tư còn không? <br/>
Giữ trong tim được không <br/>
Những chuyện xưa của lòng? <br/>
<br/>
Đêm qua nằm mơ thấy người trai lính chiến xuôi tàu về quê hương <br/>
Vui trong phố phường quên đi phút giây <br/>
Gió lạnh ngoài biên cương <br/>
<br/>
Một đêm mùa hè tôi đến sân ga <br/>
Đưa tiễn người trai lính ngày nào <br/>
Tàu cũ năm xưa mang về trả cho tôi người xưa <br/>
Để đêm nay ngồi đây <br/>
Viết lại tâm tình này..