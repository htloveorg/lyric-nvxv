Mây trắng bay qua khi trăng dần lan <br/>
Muôn câu hò nhịp nhàng khắp thôn trang <br/>
Đoàn người say sưa vui tiếng hát vang <br/>
Lúa dâng sữa ngọt đậm tình ta với nàng <br/>
<br/>
Đêm lắng sâu khi sương lam dần xuống <br/>
Hương ngạt ngào tình ruộng sắn ngô thơm <br/>
Trời về khuya nghe man mác gió lộng <br/>
Chắp duyên thắm dịu đep tinh ta với mình <br/>
<br/>
Ô thoáng nghe câu hò, sớm bên nhẹ ngân ru hò <br/>
Hò hò khoan tự do ấm no <br/>
Nắm tay hát lên ngàn câu thanh bình <br/>
Tình tang tình đồng quê tốt tươi <br/>
Ánh trăng đang tỏa sáng ngời <br/>
Tình tang ơ này tình tang ta ngước xem ông sao thần nông <br/>
Bên sông ngân hà vắng bóng ngưu lang trầm ngâm nhớ nàng <br/>
<br/>
Em có nghe chăng dư âm đồng quê <br/>
Khi trăng ngàn mờ tỏa chiếu trên đê <br/>
Đoàn người nông phu vui gánh lúa về <br/>
Bóng trai gái làng hẹn hò nhau ước thề <br/>
<br/>
Đom đóm bay quanh trên ao bèo xa <br/>
Khi bao nguoi dìu dịu giấc nam kha <br/>
Mộng triền miên say sưa dưới mái vàng <br/>
Ánh trăng khuất chìm vào rặng tre cuối làng