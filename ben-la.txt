Hai mươi năm rồi người đã quên ta <br/>
Hay hai mươi năm ta đã quên người <br/>
Người đã ra đi hay người ở lại <br/>
Ta mù nẻo đời một bóng chim di <br/>
<br/>
Người ở ta đi mình còn lại chi <br/>
Thiên thu mây trôi trên đường khép cửa <br/>
Trái tim lưu vong, trái tim ngục tù <br/>
Một kiếp đoạ đày tội lỗi thương vay <br/>
<br/>
Người đành quên ta ta còn hoài nhớ <br/>
Nhớ Người thân xưa, nhớ mưa Sài Gòn <br/>
Nhớ hàng me nghiêng, áo dài nẻo phố <br/>
Nhớ lá sân trường một cõi ngây thơ <br/>
Hai mươi năm dài đời cùng ta đi <br/>
Lang thang trôi theo muôn trùng bến lạ <br/>
Con tim hoang mang con tim tội tình <br/>
Người ở bên trời còn nhớ hay quên?