Em yêu ơi, tình đã trôi theo tháng năm đi xa vời mà lòng anh vẫn không nguôi sầu <br/>
tiếc cho ngày tháng buồn vui trong vòng ân ái. <br/>
Trong đêm mơ tình vẫn xanh như lá xanh không phai màu tình yêu anh vẫn như bao ngày <br/>
dấu chân mình đã hằng ghi trên bờ cát trắng <br/>
Cuộc tình dù biết đã có hôm nay nhưng lòng anh vẫn cứ luôn âu sầu lệ ướt bờ mi <br/>
Người tình đừng chớ quay gót với anh cho lòng anh vẫn như se lạnh <br/>
dù cho nắng ấm xuân đang về ngất ngây nồng say chốn đây. <br/>
<br/>
ĐK: <br/>
<br/>
Em yêu ơi, em có nhớ về anh một chiều cùng nhau ngồi trên góc phố ngày xưa <br/>
Tình yêu anh đã trao cho em thiết tha như lần đầu tiên anh mới biết yêu <br/>
Rồi đông đã mang em về chốn nào, tình yêu buồn vui ngày đó còn đâu <br/>
Hôm nay đây khi cánh én mùa xuân lại về, anh ngồi đây một mình lặng lẽ quạnh hiu, nhớ người.