Em đi về cỏ hoang thấp lối <br/>
Môi ướt mềm chìm thung lũng buồn <br/>
Em đi về nghe nắng đổi thay <br/>
Từng đôi chân còn vương nơi đây <br/>
<br/>
Em đi về lòng ai héo hắt <br/>
Xưa mắt buồn đôi tay vẫy gọi <br/>
Em đi về anh chết nào hay <br/>
Đôi mắt biếc nhạt mi cay <br/>
<br/>
Em đi về đường khuya ướt bước <br/>
Bàn chân ai thương theo dấu mòn <br/>
Em đi về anh có nào hay <br/>
Anh rã rời say <br/>
<br/>
ÐK: <br/>
Chiều mưa rơi em còn nghe tiếng nhạc phai <br/>
Em còn thương tiếc bờ môi <br/>
Em về cho bóng anh tàn <br/>
Một mình buổi chiều còn vương mắt xanh <br/>
<br/>
Trong tim em xót xa đầy <br/>
Giọt lệ nào vương gót chân <br/>
Em về lê bước buồn tênh <br/>
Em ngồi che dấu quạnh hiu <br/>
<br/>
<br/>
Em đi về bàn tay buốt giá <br/>
Thôi giã từ cơn mê cuối mùa <br/>
Em đi về anh vẫn chờ đây <br/>
Mang ánh mắt buồn dâng mây <br/>
<br/>
Em đi về còn nhiều nuối tiếc <br/>
Chờ mong ai thương theo dấu mòn <br/>
Em đi về em có nào hay <br/>
Anh rã rời say