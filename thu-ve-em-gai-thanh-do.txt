Nhiều khi tôi muốn viết thư thăm em<br/>
Về kể chuyện rừng xanh<br/>
Chuyện vui buồn quân ngũ<br/>
Chuyện quân hành đất đỏ<br/>
Nhiều đêm dài mưa đổ<br/>
Nhưng ngại em nhớ tôi chăng?<br/>
<br/>
Mười năm tôi xa mái trường yêu<br/>
Mang theo bao hẹn hò<br/>
Rời tuổi xanh học trò<br/>
Dù đời còn lắm mộng mơ!<br/>
Dù biết bao giờ,<br/>
Tìm về người cũ hoa xưa?<br/>
<br/>
Giờ đây, nghe nói em đang vui say<br/>
Chiều hoa lệ thành đô<br/>
Vòng tay ngà đua mở<br/>
Cùng hoa đèn sáng rỡ<br/>
Vùi em vào giấc ngủ<br/>
Quay cuồng tiếng hát đam mê<br/>
<br/>
Hỏi em, mai sương gió đường xa<br/>
Vai ba lô nặng đầy<br/>
Tay súng giữa đêm dài<br/>
Miệt mài tranh đấu vì ai?<br/>
Đừng để cho nhau<br/>
Một lời đau xót ngày mai!<br/>
<br/>
Nhớ đêm nào, đường về nhà em<br/>
Có trăng lên sáng soi lối vào<br/>
Có đôi mình, cùng chung bóng hình<br/>
Chung tiếng hát ấm êm ngày xanh<br/>
<br/>
Nhớ câu thề, một lần chia tay<br/>
Có hoa rơi cuối chiều nắng hè<br/>
Để bây giờ, dù ai hải hồ<br/>
Xin em nhớ tiếng xưa đợi chờ<br/>
<br/>
Làm sao tôi nói hết trong trang thư<br/>
Tình yêu gởi về em<br/>
Mười năm dài chưa mỏi<br/>
Đời trai còn trôi nổi<br/>
Vì nghe lời khắc khoải<br/>
Quê mình đau xót em ơi!<br/>
<br/>
Trời biên khu, trăng sáng triền miên<br/>
Nhưng tim tôi lạnh đầy<br/>
Ngồi viết trang thư này<br/>
Gửi người em gái thành đô<br/>
Tôi vẫn chưa quên<br/>
Lời nguyện câu ước ngày xưa. .