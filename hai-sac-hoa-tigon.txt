Một chiều thu trước khi nắng chiều nhuộm vàng tóc ai <br/>
Khi hoàng hôn tới đôi người chung lối lúc hoa nở trên bước đi <br/>
Vuốt tóc người yêu anh nhìn đường xa hoang vắng <br/>
Nhật cánh tigôn anh bảo rằng như tim vỡ <br/>
Anh sợ tình ta cũng vỡ thôi <br/>
<br/>
Để người em gái xanh tuổi đời một lần vấn vương <br/>
Đâu ngờ giông tố phai màu hương phấn héo xuân thì khi chốt thương <br/>
Có biết gì đâu khi nhìn đường xa hoang vắng, nhặt cánh tigôn em bảo tình yêu trong trắng <br/>
Muôn đời vạn kiếp không biến duyên <br/>
<br/>
Rồi một người ra đi bên cát lạ ánh trăng sầu <br/>
Để một người cô đơn ôm mối lẻ khóc duyên đầu <br/>
Rồi thu qua, và từng thu qua, thu nào mình hẹn hò <br/>
thu nào xa <br/>
<br/>
Rồi một mùa thu sau, con đò tách bến sang ngang <br/>
Mà đường dài miên mang thu còn dấu bước anh sang <br/>
Vườn Thanh ơi, người xưa ơi có nghe trái sầu đang rụng rơi <br/>
<br/>
Rồi từng thu chết, thu chán chường tìm lại xứ Thanh <br/>
Khi tình căm nín cho buồn lên tiếng <br/>
Những đêm trường ngỡ bước anh <br/>
Nhớ lúc hẹn xưa anh nhìn đường xa vắng <br/>
Nhặt cánh tigôn, anh bảo rằng như tim vỡ <br/>
Khi hiểu ... Tình xưa nào thấy đâu