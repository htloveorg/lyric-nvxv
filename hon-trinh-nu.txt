Chiều về chầm chậm trong hiu quạnh <br/>
Trên cành bao tơ liễu kéo nhau chạy xuống hồ <br/>
Mây xám bay bay làm tôi thấy <br/>
Quanh tôi và tất cả một trời chít khăn xô <br/>
Sáng nay vô số lá vàng rơi <br/>
Người trinh nữ ấy đã xa lìa cõi đời <br/>
Có một chiếc xe màu trắng đục <br/>
Hai con ngựa trắng xếp thành hàng đôi <br/>
<br/>
Mang theo một chiếc quan tài trắng <br/>
Với những bông hoa buồn hoa cũng rũ sầu <br/>
Theo bước những người khăn áo trắng <br/>
Khóc hồn trinh trắng thương mãi không thôi <br/>
Chắc đêm qua tựa gối nệm êm <br/>
Nàng còn say giấc mơ hoa bên gối mềm <br/>
Giấc mơ ước một trời xuân nồng <br/>
Một trời xuân sắc đến tận tàn canh <br/>
<br/>
ĐK: <br/>
Sáng nay nàng đã lặng im <br/>
Nàng đã lặng im máu ngưng trôi trong buồng tim <br/>
Sáng nay người Mẹ già kia <br/>
Đã vội vàng quấn tấm khăn lên đầu bầy em <br/>
<br/>
Đau thương đã bao năm rồi <br/>
Tuổi đã xế chiều Mẹ còn đâu nước mắt <br/>
Thương bầy em thơ <br/>
Những môi non từ đây chẳng còn gọi "Chị ơi" <br/>
<br/>
Một người lạnh lùng trong đêm dài <br/>
Đi mặc cho mưa gió ướt vai lạnh buốt hồn <br/>
Trong bóng đêm đen chàng xao xuyến <br/>
Như say dạ thấy mềm chàng vờn tay níu hư không <br/>
Chắc đây trong số những người quen <br/>
Đã bao nhiêu năm tháng ấp ôm một mối tình <br/>
Cách đây ít hôm người láng giềng <br/>
Nghe tin chàng toan tính đến chuyện hợp hôn <br/>
<br/>
Nhưng hồn nàng tựa con thuyền bến <br/>
Đã đắm nghìn thu... ở suối vàng...!!!