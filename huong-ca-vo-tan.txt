Hát nữa đi Hương hát điệu nhạc buồn điệu nhạc quê hương. <br/>
Hát nữa đi Hương hát lại bài ca tiễn anh lên đường. <br/>
Ngày đao binh chưa biết còn bao lâu, <br/>
cuộc phân ly may lắm thì qua mau <br/>
Hát nữa đi Hương hát để đợi chờ. <br/>
<br/>
Hương ơi...sao tiếng hát em, <br/>
nghe vẫn dạt dào, nghe vẫn ngọt ngào <br/>
Dù em ca những lời yêu đương, <br/>
hay chuyện tình gẫy gánh giữa đường. <br/>
Dù em ca nỗi buồn quê hương, <br/>
hay mưa giăng thác đổ đêm trường. <br/>
<br/>
Hát chuyển vai em tóc xõa bồng mềm dịu ngọt môi em. <br/>
Hát mãi nghe Hương cho hồng làn da kẻo đời chóng già. <br/>
Ngày xa xưa em vẫn nằm trong nôi, <br/>
Mẹ ru em câu hát dài buông lơị <br/>
Hát để yêu cha ấm lại ngày già. <br/>
<br/>
Hương ơi, sao tiếng hát em, <br/>
nghe vẫn dạt dào, nghe vẫn ngọt ngào <br/>
Dù em ca những lời yêu đương, <br/>
hay chuyện tình gãy gánh giữa đường. <br/>
Dù em ca nỗi buồn quê hương, <br/>
hay mưa giăng thác đổ đêm trường. <br/>
<br/>
Hát nữa đi Hương câu nhạc thành nguồn gợi chuyện đau thương. <br/>
Hát kể quê hương núi rừng đầy hoa bỗng thành chiến trường. <br/>
Đồng tan hoang nên lúa ngại đâm bông, <br/>
Thuyền ham đi nên nước còn trông mong. <br/>
Khiến cả đêm thâu tiếng em rầu rầu. <br/>
<br/>
Hát nữa đi Hương, hát đi Hương, hát mãi đi Hương.