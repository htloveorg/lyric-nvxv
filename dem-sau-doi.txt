Dêm nay mùa Dông dã sang,<br/>
Bước chân di về thấy tình còn vương mang<br/>
Con sông thời gian giá băng<br/>
Nước chia dôi dòng vẫn lạnh lùng dâng sóng<br/>
Người thì gần mà bờ môi chưa ấm<br/>
Tình thì dau mà lòng chưa xa<br/>
Nghiêng vai trăm năm dã qua<br/>
<br/>
ĐK:<br/>
Tiếng hát buồn cất lên<br/>
Ngỡ quên tên người tình<br/>
Tiếng hát chợt vút cao<br/>
Xôn xao dời cơm áo<br/>
<br/>
Tiếng hát còn nổi dau<br/>
Xốn xang qua từng mùa<br/>
Hát giữa dời bể dâu<br/>
Hồn nhiên còn bao lâu<br/>
<br/>
Mai dây về nơi núi cao <br/>
Nhớ nơi sông hồ biết lòng người nông sâu<br/>
Ta vui vì em biết dau<br/>
Tiếng ca vương buồn giữa dường trần phai dấu<br/>
Còn cuộc tình dể dời ta nương náu<br/>
Còn lời ca dế tìm về bên nhau<br/>
Cho ta quên trăm năm dớn dau<br/>
<br/>
<br/>
Trình bày: Mỹ Tâm<br/>
Tên album: Vẫn nợ cuộc dời