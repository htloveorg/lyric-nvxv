Anh mong chờ mùa Thu <br/>
Trời đất kia ngả mầu xanh lơ <br/>
Đàn bướm kia đùa vui trên muôn hoa <br/>
Bên những bông hồng đẹp xinh. <br/>
<br/>
Anh mong chờ mùa Thu <br/>
Dìu thế nhân dần vào chốn Thiên Thai <br/>
Và cánh chim ngập ngừng không muốn bay <br/>
Mùa Thu quyến rũ Anh rồi. <br/>
<br/>
Mây bay về đây cuối trời <br/>
Mưa rơi làm rụng lá vàng <br/>
Duyên ta từ đây lỡ làng <br/>
Còn đâu những chiều <br/>
Dệt cung đàn yêu. <br/>
<br/>
Thu nay vì đâu tiếc nhiều <br/>
Thu nay vì đâu nhớ nhiều <br/>
Đêm đêm nhìn cây trút lá <br/>
Lòng thấy rộn ràng <br/>
Ngỡ bóng ai về. <br/>
<br/>
Anh mong chờ mùa Thu <br/>
Tà áo xanh nào về với giấc mơ <br/>
Mầu áo xanh là mầu Anh trót yêu <br/>
Người mơ không đến bao giờ.