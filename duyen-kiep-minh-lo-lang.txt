(nhạc: Phạm Đình Chương - lời: Hoàng Ngọc Ẩn)<br/>
<br/>
Những con đường đẹp nắng thủ đô <br/>
Nhớ chiều xưa mình đã hẹn hò <br/>
Những con đường giờ đây hoang vắng <br/>
Thương người tình lạc lỏng bơ vơ. <br/>
Nghe buồn đau những ngày hạ cuối <br/>
Ðêm nằm đây đếm tuổi lưu đày <br/>
Tháng năm xưa ngút ngàn tiếc nuối <br/>
Trong tâm linh ngàn kiếp không phai. <br/>
Trắng ba đêm cho dài nhung nhớ <br/>
Ai ra đi xót kẻ cuối trời <br/>
Phía trên mây cũng đành duyên lỡ <br/>
Lục địa này câm nín muôn đời. <br/>
<br/>
“Ðôi chúng ta đôi bờ khóc hận <br/>
Mênh mông sầu như sóng biển sâu <br/>
Và đôi nơi xót xa thân phận <br/>
Nghìn trùng xa nặng trĩu thương đau” (2) <br/>
<br/>
Tiếc mà chi hỡi người tình lỡ <br/>
Thương mà chi ước mộng xa rồi <br/>
Nếu mai sau có lần gặp gỡ <br/>
Xin cho nhau một nét môi cười. <br/>
<br/>
Tiễn nhau đi bên trời phiêu lãng <br/>
Ta lang thang cuối nẻo lưu đày <br/>
Xác tương lai tiếc về dĩ vãng <br/>
Chút mộng lòng cất cánh xa bay.