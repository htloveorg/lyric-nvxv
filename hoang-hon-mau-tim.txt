Chiều hoàng hôn tím cả giòng sông <br/>
Đò neo bến vắng mà nhớ anh thiết tha trong lòng <br/>
Người anh sang sông quên lời hẹn ước <br/>
Để lại tình em mênh mông sông nước <br/>
Con sáo xổ lồng sáo bay theo pháo đỏ rượu hồng <br/>
<br/>
<br/>
Người ơi nhớ câu thề xưa <br/>
Dù nắng hay mưa dù nghèo hay khổ <br/>
Dù ai sang giàu cũng không bao giờ chia cách đôi ta <br/>
Mà giờ đây sao anh nỡ đoạn đành <br/>
Tham phú phụ bần anh phụ tình em <br/>
<br/>
Khi chiều buông em đứng bên bờ sông <br/>
Đò sang bến mới mà nhớ anh xót xa trong lòng <br/>
Còn đâu đêm trăng ta về chung lối <br/>
Chỉ còn mình em mênh mông nỗi nhớ <br/>
Trên bến sông buồn nhớ thương anh tím cả hoàng hôn