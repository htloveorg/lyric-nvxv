<i>(nhạc: Trọng Khương - thơ: Nguyễn Bính)</i><br/>
<br/>
<br/>
<br/>
Hỡi cô nhân tình bé của tôi ơi <br/>
tôi muốn môi cô chỉ mỉm cười <br/>
những lúc có tôi và mắt chỉ <br/>
nhìn tôi trong lúc tôi xa xôi <br/>
<br/>
Tôi muốn cô đừng nghĩ đến ai <br/>
đừng hôn dù thấy bó hoa tươi <br/>
đừng ôm gối chiếc đêm nay ngủ <br/>
đừng tắm chiều nay bể lắm người <br/>
<br/>
Tôi muốn mùi thơm của nước hoa <br/>
mà cô thường xức chẳng bay xa <br/>
chẳng làm xao xuyến người qua lại <br/>
dẫu chỉ qua rồi khách lại qua <br/>
<br/>
Nghĩa là yêu quá đấy mà thôi <br/>
thế nghĩa là ghen quá đi mất rồi <br/>
và nghĩa là cô là tất cả <br/>
cô là tất cả của mình tôi <br/>
và nghĩa là cô là tất cả <br/>
cô là tất cả của đời tôi.