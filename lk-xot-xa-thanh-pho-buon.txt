Mình về thành phố đây rồi, <br/>
chốn ăn, chốn vui lạ mặt người. <br/>
Cho bỏ gian lao ngần này phép rong chơi, <br/>
rủ phong sương đầy áo mà nghe lòng ước muốn lên cao. <br/>
<br/>
Đi lính xa đánh giặc từng giờ <br/>
viết trăm lá thư để hẹn hò, <br/>
cho buổi hôm nay đời chỉ có ta thôi, <br/>
tiếng yêu chưa lần nói mà đường yêu chân bước vào rồi! <br/>
<br/>
Kín vai sương tóc lệch đường ngôi <br/>
đã cho nhau, vì nhau mà tới đợi đêm này <br/>
đợi đã bao đêm từ khi chiến đấu mọi miền, <br/>
anh mơ yên lành lần hiện diện trên mắt môi em. <br/>
<br/>
Mình trở lại với đơn vị <br/>
bén hơi chuyến đi hẹn ngày về. <br/>
Nhưng kẻ phương xa đời còn thú vui xa, <br/>
những đêm ngang tầm súng vào nửa khuya nhung nhớ ngập lòng