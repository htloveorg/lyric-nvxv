Xin giã biệt bạn lòng ơi <br/>
Trao trả môi người cười <br/>
Vì hai lối mộng hai hướng trông <br/>
Mình thương nhau chưa trót <br/>
Thì chớ mang nỗi buồn theo bước đời <br/>
Cho dù chưa lần nói... <br/>
<br/>
Nhưng nếu còn đẹp vì nhau <br/>
Xin nhẹ đi vào sầu <br/>
Gợi thương tiếc nhiều đau bấy nhiêu <br/>
Mình chia tay đi nhé <br/>
Để chốn nao với chiều mưa gió lộng <br/>
Ta dừng vui bến mộng <br/>
<br/>
Bao lần đi gối mỏi chân mòn <br/>
Tâm tư nặng vai gánh <br/>
Đường trần cho đến nay <br/>
Chỉ còn bờ mi khép kín <br/>
Giấc ngủ nào quên, <br/>
Giấc ngủ nào gọi tên .... <br/>
<br/>
Thôi nhắc nhở để mà chi <br/>
Quay về xưa làm gì <br/>
Giờ hai lối mộng hai hướng đi <br/>
Niềm ưu tư tôi đếm <br/>
Từng bước trên phố nhỏ đau gót mềm <br/>
Sao rụng giữa đường đêm ...