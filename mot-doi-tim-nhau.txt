Ngày ấy êm đềm bên nhau <br/>
sáng chiều chung lối đón đưa nhau về <br/>
Tình thơ thơm như mùi giấy <br/>
những chiều hẹn hò, giận hờn vu vơ <br/>
Ngày đó anh thường vì em <br/>
thức nhiều đêm trắng chép thơ làm quà <br/>
Tình yêu ôi thơ mộng quá <br/>
ngỡ trọn một đời đôi mình không cách xa <br/>
Nào ngờ đâu giông bão cuốn xô đi <br/>
Bóng anh mây chìm khuất, <br/>
bước chân em lạc loài <br/>
Cuộc tình thơ năm xưa nay đã khuất xa xăm <br/>
Nhớ nhau xin hãy tìm trong kỷ niệm <br/>
Theo gió chim trời giặt trôi <br/>
cuối trời mưa gió, biết đâu mà tìm? <br/>
Tình ơi! sao nghe buồn quá, <br/>
lẽ nào một đời mình xa cách nhau.