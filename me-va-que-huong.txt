Xin hiểu tình yêu, trong thời chiến chinh này mấy người mơ ước cho tròn.<br/>
Càng khổ càng đau, thì tình yêu càng sâu khi dắt đưa nhau về bến.<br/>
Ngăn cách bây giờ, cho mai mốt sum vầy không thấy thẹn cùng sông núi.<br/>
Vì đời khổ đau anh góp một phần xương máu.<br/>
Đôi cánh tay này anh hiến trọn cho tình quê.<br/>
<br/>
Anh đâu hay rằng, mẹ quê hôm sớm còn hắt hiu cùng nương sắn khoai.<br/>
Anh đâu hay rằng đàn trẻ thơ vắng cha cày bừa thay cho người đi.<br/>
<br/>
Đành lòng sao em nỡ nào nhìn bạn bè.<br/>
Nhọc nhằn gian lao, mắt quay đi cho đành.<br/>
Đành lòng sao em, em đành lòng sao em, ấm êm gì chỉ mình ta.<br/>
<br/>
Xin hiểu lòng nhau, cho dù cách ngăn này có dài lâu đến bao giờ.<br/>
Tình vẫn vàng son, tình này vẫn đẹp tươi như đóa hoa không tàn úa.<br/>
Ta đón nhau về, khi non nước yên bề sông núi vào hội yêu thương.<br/>
Mình cùng dìu nhau đi khắp vùng trời quê hương.<br/>
Con bướm đa tình kia sẽ dừng lại ở đây