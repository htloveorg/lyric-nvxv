Anh đi rồi, ngậm ngùi tôi ở lại <br/>
Đêm bóng dài, nghe lạnh đầy bờ vai <br/>
Người đi vương nỗi nhớ <br/>
Người ở lại mang nồi chờ <br/>
Ngõ hồn cô quạnh bơ vơ <br/>
Chỉ còn lại một vần thơ <br/>
<br/>
Anh đi rồi, nửa hồn tôi khờ dại <br/>
Chuyên chở hoài những kỷ niệm chưa phai <br/>
Ngày tôi anh gặp gỡ <br/>
Ngày tạ từ tay vẫy chào, <br/>
Mềm lòng chấp nhận xa nhau <br/>
Bởi giòng đời tựa sóng dâng cao <br/>
<br/>
Anh ra đi, nơi trời xa xứ lạ <br/>
Nhắn gửi gì về miền đất quê ta <br/>
Anh vô tư hay là anh còn nhớ <br/>
Những con đường tình tự ngày qua <br/>
<br/>
Anh đi rồi, có nghĩa là xa xôi <br/>
Tôi ngóng chờ cũng chỉ là chờ thôi <br/>
Làm sao ngăn được bước <br/>
Người ở lại chung hướng đời <br/>
Tình mình đành chia phôi <br/>
Mắt lệ buồn, tuôn mặn bờ môi