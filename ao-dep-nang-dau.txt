Còn gì đẹp hơn những ngày bên nhau <br/>
Hai đứa chung vui ý hợp tâm đầu <br/>
Em nhớ chăng em mỗi độ hoa Đào <br/>
Mình thầm ước đến mai sau <br/>
Nhặt hoa kết áo nàng dâu. <br/>
<br/>
Mình thường chờ nhau những ngày mưa ngâu <br/>
Chung nón che mưa nói chuyện ban đầu <br/>
Anh viết câu thơ ý đẹp muôn màu <br/>
Mình nào nghĩ đến thương đau <br/>
Nào hay yêu mến con tàu. <br/>
<br/>
Em ơi giờ đây em còn phiêu bạt nơi đâu em ơi <br/>
Đời em bao ngày nắng mưa dãi dầu <br/>
Về đây em có bàn tay nhỏ đêm thâu <br/>
Đốt đèn châm lửa cho nhau <br/>
Quên chuyện đắng cay cơ cầu. <br/>
<br/>
Còn gì buồn hơn những ngày xa nhau <br/>
Hai đứa hai nơi cũng một tâm sầu <br/>
Em hỡi nơi đây vẫn nhiều hoa đào <br/>
Mà người cũ mãi nơi đâu <br/>
Để cho duyên lỡ nhịp cầu