Tôi đã yêu anh từ muôn kiếp nào <br/>
Cho dẫu mai sau đời nhiều bể dâu <br/>
Biết rằng chẳng được gần nhau <br/>
Đừng đem cay đắng cho nhau <br/>
Cho cung đàn lỡ nhịp thương đau <br/>
<br/>
Tôi đã yêu anh tình yêu ban đầu <br/>
Tôi biết duyên tôi gặp nhiều khổ đau <br/>
Mắt buồn thức đã quầng sâu <br/>
Để thương để nhớ cho nhau <br/>
Nước mắt nào rơi vào đêm thâu <br/>
<br/>
Thôi xin anh đừng buồn, xin anh đừng buồn <br/>
Tình chỉ đẹp khi còn dang dở <br/>
Đời mất vui khi đã vẹn câu thề <br/>
Tôi đã yêu anh tình yêu ban đầu <br/>
<br/>
Tôi biết duyên tôi gặp nhiều khổ đau <br/>
Cũng đành chấp nhận hợp tan <br/>
Bèo mây trọn kiếp lang thang <br/>
Cho cung buồn rơi vào đêm đen