Mưa khuya hắt hiu xuyên qua mành, tình ngăn cách rồi. <br/>
Đêm qua trắng đêm mơ thương hình bóng cũ xa xôi <br/>
Em ơi! bước đi xa nhau rồi ngày vui đâu còn, <br/>
đèn vàng nhòa sương chưa tắt, <br/>
khu phố xưa lạnh buồn tênh. <br/>
Hôm em bước lên xe hoa thềm nhà tươi pháo hồng <br/>
Em ơi! pháo vui như vô tình xé nát tim anh. <br/>
Bao nhiêu ước mơ nay phai tàn, <br/>
Tình ôi phũ phàng, <br/>
một ngày dù duyên chưa thắm, <br/>
chuyến đò xưa sao nỡ quên! <br/>
Thương còn thương những chiều đời chưa biết nhiều, <br/>
nghẹn ngào nhìn nhau không nói. <br/>
Yêu còn yêu tiếng cười, ngày mới quen nhau, <br/>
ngỡ ngàng tình trong mắt sâu. <br/>
<br/>
Mưa khuya hắt hiu xuyên qua mảnh hồn đơn giá lạnh <br/>
Em ơi phố khuya bâng khuâng sầu buốt giá tim anh. <br/>
Hương xưa ái ân theo êm đềm vàng trăng giãi thềm. <br/>
Từng giờ buồn trông phố vắng, thắm hình em qua bóng đêm