Có tiếng hát nào về ru trên đỉnh trời, <br/>
Lúc em về lạnh lùng con phố dài. <br/>
Chiều còn mưa hàng phố âm thầm, <br/>
Từng cây cao chia cắt dòng sông, <br/>
Chân em xa rồi, Ngày giông bão tới, <br/>
Chân em xa rồi, Ngỡ vừa chiêm bao, <br/>
Mưa thầm đầu mùa, ướt áo em dài, <br/>
Gió vội vàng gội mưa ngỡ ngàng <br/>
Mưa còn ru người trong cách xa, <br/>
Chờ em đến đã quen mưa chiều, <br/>
Bước phong trần nghe lãng quên đã gần, <br/>
Ngoài sông xa, gió cũng chồn chân. <br/>
<br/>
Mưa dìu em đầy vơi đã quen, <br/>
Bày tay gió cá lá ngả nghiêng, <br/>
Anh đưa em về đường vui ướt lá, <br/>
sau cơn mưa rộng, Mẹ chờ bên bếp hồng. <br/>
<br/>
Tiếng hát nào vừa ru trên phố đầy, <br/>
Dắt em về một ngày vui đã dài, <br/>
Tiếng hát nào gọi mưa trên phố hồng, <br/>
Dắt em về ngọt bùi nghe đã nồng.