Đầu xuân năm đó anh ra đi <br/>
Mùa xuân này đến anh chưa về <br/>
Những hôm vừa xong phiên gác chiều <br/>
Ven rừng kín hoa mai vàng <br/>
Chợt nhớ tới sắc áo năm nào em đến thăm gác nhỏ <br/>
<br/>
Mùa hoa năm đó ta chung đôi <br/>
Mùa hoa này nữa xa nhau rồi <br/>
Nhớ đêm hành quân thân ướt mềm <br/>
Băng giòng sông loang trăng gầy <br/>
Lòng muốn vớt ánh sáng trăng thề viết tên em <br/>
<br/>
Đồn anh đóng ven rừng mai <br/>
Nếu mai không nở, anh đâu biết xuân về hay chưa <br/>
Chờ em một cánh thư xuân, nhớ thương gom đầy <br/>
Cho chiến sĩ vui miền xa xôi... <br/>
<br/>
Hẹn em khi khắp nơi yên vui <br/>
Mùa xuân ngày đó riêng đôi mình <br/>
Phút giây mộng mơ nâng cánh hoa mai <br/>
Nhẹ rớt trên vai đầy, hồn chơi vơi <br/>
Ngỡ giữa xuân vàng, dáng em sang