<i>(nhạc: Vũ Thư Nguyên - thơ: Phạm Ngọc)</i><br/>
<br/>
<br/>
<br/>
Bài thơ này tôi không gửi cho ai <br/>
trong biệt ly bóng người xa xôi lắm <br/>
như áng mây cuối trời xa vạn dặm <br/>
hương phấn xưa bay theo gió trăm miền <br/>
<br/>
Nơi tôi về chiều lất phất mưa bay <br/>
mưa trăm ngã mang theo lời gió gọi <br/>
mưa trắng quá che tầm nhìn mắt đợi <br/>
dãy phố buồn còn ai đứng trông theo? <br/>
<br/>
Em có nhớ, những con đường của một thuở thân yêu <br/>
không còn em, không còn em bỗng nhìn sao rất lạ <br/>
hối tiếc gì, chỉ còn mưa giăng trên từng nhánh lá <br/>
thành sông dài chảy suốt nửa đời sau <br/>
<br/>
tình bây giờ là những bước không nhau <br/>
dấu chân ai dẫm lên vùng ký ức <br/>
tôi soi gương nhìn lại mình rất thực <br/>
em xa rồi - tình một kiếp thương vay