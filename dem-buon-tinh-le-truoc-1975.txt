[Nhạc và lời: Tú Nhi - Bằng Giang]<br/>
<br/>
<br/>
Đã lâu rồi đôi lứa cách đôi nơi tơ duyên xưa còn hay mất <br/>
Mái trường ơi em tôi còn học nữa hay ra đi từ độ nào <br/>
Ngày xưa đó ta hay đón dìu nhau đi trên con đường lẻ loi <br/>
Mấy năm qua rồi em anh không gặp nữa <br/>
Bao yêu thương và nhớ anh xin chép nên thơ <br/>
vào những đêm buồn <br/>
<br/>
Mưa mưa rơi từng đêm <br/>
Mưa triền miên trên đồn khuya <br/>
Lòng ai thương nhớ vô biên <br/>
Anh ra đi ngày đó <br/>
Ta nhìn nhau mắt hoen sầu <br/>
Không nói nên câu giã từ <br/>
<br/>
Mong anh mong làm sao <br/>
Cho tình duyên không nhạt phai <br/>
Theo năm tháng thoáng qua mau <br/>
Yêu, yêu em nhiều lắm <br/>
Nhưng tình ta vẫn không thành <br/>
Khi núi sông còn điêu linh <br/>
<br/>
Ở phương này vui kiếp sống chinh nhân nhưng không quên dệt mơ ước <br/>
Ước ngày nao quê hương tàn chinh chiến cho tơ duyên đượm thắm màu <br/>
Và phương đó em ơi có gì vui xin biên thư về cho anh <br/>
Nhớ thương vơi đầy, đêm nay trên đồn vắng <br/>
Thương em anh thương nhiều lắm, em ơi biết cho chăng ... <br/>
Tỉnh lẻ đêm buồn