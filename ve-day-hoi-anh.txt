Anh hỡi em về đây <br/>
Lòng vẫn mong ta <br/>
Sẽ có nhau như ngày xưa. <br/>
Anh hỡi em về đây <br/>
Nhớ nhung đong đầy. <br/>
Ôi tình em cô đơn <br/>
Không có anh đâu còn vui chi. <br/>
Bao giòng lệ chứa chan, <br/>
Ôi mắt môi đã chìm sâu. <br/>
<br/>
Rồi em ca lên <br/>
Dù sáng hay trong đêm thâu <br/>
Giọng ca ngân vang. <br/>
Người có hay chăng hỡi anh <br/>
Tình em trao anh. <br/>
Tươi thắm như muôn đóa hoa <br/>
Mãi không phai nhòa. <br/>
<br/>
Anh hỡi em về đây <br/>
Lòng vẫn mong ta <br/>
Sẽ có nhau như ngày xưa. <br/>
Anh hỡi em về đây <br/>
Héo hon tim gầy. <br/>
Ôi tình em xanh xao <br/>
Không có anh nên đời phai mau <br/>
Sao kỷ niệm đớn đau <br/>
Bao tin yêu đã về đâu ? <br/>
<br/>
Rồi em ca lên <br/>
Lời hát ru cơn đam mê <br/>
Rồi em ca lên <br/>
Tình lỡ em mong kiếp sau. <br/>
Đời em bơ vơ <br/>
Giông tố ơi xin hãy cho <br/>
Phút giây mong chờ. <br/>
<br/>
Anh hỡi em về đây <br/>
Tình vẫn như trong giấc mơ ôi tình say <br/>
Anh hỡi em về đây <br/>
Xót xa bao ngày. <br/>
Đâu vòng tay nâng niu <br/>
Môi cười cho tình ta phiêu diêu. <br/>
Ân tình ta đã trao <br/>
Anh hỡi xin nhớ đừng quên ngày nào. <br/>
<br/>
Rồi em ca lên <br/>
Dù sáng hay trong đêm thâu. <br/>
Giọng ca ngân vang <br/>
Người có hay chăng hỡi anh? <br/>
Tình em trao anh <br/>
Tươi thắm như muôn đoá hoa. <br/>
Rồi em yêu ạnh <br/>
Rồi em yêu anh <br/>
Tình vẫn bên anh ngàn năm. <br/>
Rồi em yêu anh <br/>
Người hỡi thiên thu không phai. <br/>
Và đôi uyên ương <br/>
Sẽ yêu mãi nhau muôn đời nhé anh.