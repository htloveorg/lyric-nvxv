Đời từ muôn thuở tiếng mưa có vui bao giờ <br/>
Chuyện lòng tui kể cách đây đã ba mùa mưa <br/>
Tôi đem tất cả tim lòng trao hết cho người <br/>
Nguyện tròn thương tròn nhớ... <br/>
<br/>
Nàng là trinh nữ tóc buông kín đôi vai gầy <br/>
Một làn môi đỏ nhớ nhung vấn vương vì ai <br/>
Chân son gót nhỏ đi tìm hương phấn cho đời <br/>
Trời xanh đã an bài... <br/>
<br/>
Yêu nhau như bướm say hoa <br/>
Đẹp như giấc mộng vượt qua những năm đầu <br/>
Năm sau mưa gió nhìn nhau <br/>
Người đã quen dần xa rồi năm thứ ba... <br/>
<br/>
Nhìn trời mưa đổ thấy đau buốt thêm trong lòng <br/>
Tình là hoa nở thắm tươi đó nhưng rồi phai <br/>
Khi xưa nếu chẳng đem tình dâng hết cho người <br/>
Thì nay có đâu buồn !