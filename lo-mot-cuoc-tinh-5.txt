Ai biểu anh làm thinh? <br/>
Em ngỡ anh vô tình <br/>
Ngày xưa đôi bên thật thân <br/>
Mẹ khuyên anh sang học chung <br/>
<br/>
Anh viết thư thật hay <br/>
Anh viết văn rất tài <br/>
Ðọc nghe rưng rưng trời mây <br/>
Ngẩn ngơ em quên học bài <br/>
<br/>
Anh thấu chuyện tình ngâu <br/>
Anh biết thương hoa sầu <br/>
Lời yêu em, anh để đâu? <br/>
Mười trang, không ghi một câu <br/>
<br/>
Em nghe tiếng khung trời <br/>
Lỡ rồi duyên một đời <br/>
Mộng xưa tan như thành khói <br/>
Vì người trót đã im hơi <br/>
<br/>
Em giờ này, sống kiếp chợt lòng <br/>
Anh giờ này, sống kiếp bềnh bồng <br/>
Hai cuộc đời như hai bầu trời <br/>
Lỡ cuộc tình, hối tiếc trọn đời <br/>
<br/>
Ai biểu anh làm thinh <br/>
Em ngỡ anh vô tình <br/>
Thành như cơn mơ mỏng manh <br/>
Ngày xuân trôi đi thật nhanh <br/>
<br/>
Năm đó sau mùa thi <br/>
Binh lửa sôi biên thùy <br/>
Ngày anh xa cô bạn cũ <br/>
Thời gian ba năm còn gì? <br/>
<br/>
Cây lớn, cây trổ bông <br/>
Em lớn, em theo chồng <br/>
Gặp chi hương trinh sầu úa <br/>
Chào nhau, xa nhau là sao? <br/>
<br/>
Ba năm, kiếm cơn mộng <br/>
Khi người xa mịt mù <br/>
Lần đi như quên từ giã <br/>
Mà chờ bám víu hư không <br/>
<br/>
Em giờ này, như mây chiều tàn <br/>
Anh giờ này, như mưa ngoài ngàn <br/>
Hai cuộc đời, không chung bầu trời <br/>
Hỡi tình nào, trót lỡ từ đầu... <br/>
<br/>
Ai Biểu Anh Làm Thinh Ai Biểu Anh Làm Thinh ?!?