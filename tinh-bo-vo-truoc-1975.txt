Càng nhìn em yêu em hơn và yêu em mãi <br/>
Dù phút êm đềm xa xưa nay đã đi vào quên lãng <br/>
Trời vào thu việt Nam buồn lắm em ơi <br/>
Mây tím đang dâng cao vời <br/>
Mà tình yêu chưa lên ngôi <br/>
<br/>
Ngày mình yêu <br/>
Anh đâu hay tình ta gian dối <br/>
Để bước phong trần tha hương <br/>
Em khóc cho đời viễn xứ <br/>
Về làm chi rồi anh lặng lẽ ra đi <br/>
Gom góp yêu thương quê nhà <br/>
Dâng hết cho người tình xa <br/>
<br/>
Anh đâu ngờ <br/>
Có ngày đàn đứt dây tơ <br/>
Một phút tim anh ơ hờ <br/>
Trọn kiếp anh vương sầu nhớ <br/>
<br/>
Nói đi em cả đời mình mãi đi tìm <br/>
Cả đời mình xây ước mơ <br/>
Cho ngày mộng được nên thơ <br/>
Cuối cùng là tình bơ vơ <br/>
<br/>
Cho anh xin một đêm trăn trối <br/>
Gởi đống tro tàn năm xưa <br/>
Dâng hết cho lần yêu cuối <br/>
Rồi từng đêm <br/>
Từng đêm nhịp bước cô đơn <br/>
Em khóc cho duyên hững hờ <br/>
Anh chết trong mộng ngày thơ